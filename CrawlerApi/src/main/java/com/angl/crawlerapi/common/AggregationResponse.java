/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.crawlerapi.common;

import com.angl.aggregation.engine.AggregationStarter;
import java.util.Map;
import lombok.Value;
@Value
public class AggregationResponse {
    private Map<String, LogMessage> logMap;
    private Map<String,AggregationStarter> aggregationStarter;
}
