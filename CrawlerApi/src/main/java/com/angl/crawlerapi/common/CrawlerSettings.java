
package com.angl.crawlerapi.common;

import lombok.Getter;
import lombok.Setter;

public class CrawlerSettings {
    private @Getter @Setter Long borderOfLifetime, twitterLong,periscopePlusHours,plusPerEcho;
    private @Getter @Setter Integer minElementCountToCreate;
    private @Getter @Setter String apiKey;

    public CrawlerSettings() {
    }
    
}
