package com.angl.crawlerapi.common;

import lombok.Getter;
import lombok.Setter;

public class ApiResponse {
    private @Getter @Setter String message;

    public ApiResponse(String message) {
        this.message = message;
    }
    
}
