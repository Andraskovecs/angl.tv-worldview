/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.crawlerapi.common;

import java.util.Date;
import lombok.Value;

@Value
public class LogMessage {
    private String id, message;
    private Date timestamp;
}
