package com.angl.crawlerapi.common;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import crawler.utils.ServiceInfo;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class ServiceInfoFileHandler {

    private static class SingletonHelper {private static final ServiceInfoFileHandler INSTANCE = new ServiceInfoFileHandler();}
    public static ServiceInfoFileHandler getInstance() {return SingletonHelper.INSTANCE;}
    public static String path;
    public void save(ServiceInfo info) {
        ObjectMapper mapper = new ObjectMapper();
        try {            
            File file = new File(path);            
            List<ServiceInfo> list = this.read();
            list.add(info); 
            mapper.writeValue(file, new ServiceInfoList(list));
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public List<ServiceInfo> read(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            File file = new File(path);
            if(!file.exists()){
                file.getParentFile().mkdirs();            
                file.createNewFile();
                mapper.writeValue(file, new ServiceInfoList(new ArrayList<>()));
            }
            ServiceInfoList infoList = mapper.readValue(file , ServiceInfoList.class);
            return infoList.getInfoList();
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }    
}
