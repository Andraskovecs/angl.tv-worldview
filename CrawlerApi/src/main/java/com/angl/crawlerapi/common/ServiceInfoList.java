package com.angl.crawlerapi.common;

import crawler.utils.ServiceInfo;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class ServiceInfoList{
        private @Getter @Setter List<ServiceInfo> infoList;

        public ServiceInfoList() {
        }

        public ServiceInfoList(List<ServiceInfo> infoList) {
            this.infoList = infoList;
        }

        @Override
        public String toString() {
            return "ServiceInfoList{" + "infoList=" + infoList + '}';
        }
                
    }
