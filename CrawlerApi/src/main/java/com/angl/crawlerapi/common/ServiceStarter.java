
package com.angl.crawlerapi.common;

import lombok.Getter;
import lombok.Setter;

public class ServiceStarter {
    private @Getter @Setter String mode, type, name;
    private @Getter @Setter Integer videoCount, maxViewer;
    private @Getter @Setter String longitude, latitude, radius;
    private @Getter @Setter String east, west, south, north;
    private @Getter @Setter String listUrl, detailsUrl, providerName;

    public ServiceStarter() {
    }
    

    @Override
    public String toString() {
        return "ServiceStarter{" + "mode=" + mode + ", type=" + type + ", videoCount=" + videoCount + ", maxViewer=" + maxViewer + ", longitude=" + longitude + ", latitude=" + latitude + ", radius=" + radius + ", east=" + east + ", west=" + west + ", south=" + south + ", north=" + north + '}';
    }

    public String getCooirdinate() {
        if(type.equals("facebook")) return "East: "+this.east+" West: "+this.west+" South: "+this.south+" North: "+this.north;
        else if(type.equals("twitter"))return "Longitude: "+this.longitude+" Latitude: "+this.latitude+" Radius: "+this.radius+" km";
        else if(type.equals("custom")) return "List url:"+this.listUrl+" details url: "+this.detailsUrl+" provider name: "+this.providerName;
        return "";
    }
        
}
