
package com.angl.crawlerapi.common;

import java.util.ArrayList;
import java.util.List;

public class User {
    private List<String> emails = new ArrayList<String>() {{
        add("hans@medialab.bme.hu");
        add("mpalfy@medialab.bme.hu");
        add("andras.kovecs@atrklikk.hu");
    }};
     private List<String> passwords = new ArrayList<String>() {{
        add("1q2w3e4r");
        add("1q2w3e4r");
        add("1q2w3e4r");
    }};
    private String email, password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User() {
    }
    
    public Boolean isValid() {
       if(emails.contains(this.email)){
           int index = email.indexOf(this.email);
           String pass = passwords.get(index);
           return pass.equals(this.password);
       }
       return false;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
