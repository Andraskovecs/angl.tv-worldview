package com.angl.crawlerapi.common;

import crawler.utils.ServiceInfo;
import java.util.Map;
import lombok.Value;
@Value
public class ServiceResponse {
    private Map<String, LogMessage> logMap;
    private Map<String,ServiceInfo> services;

    
}
