/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.crawlerapi.controllers;

import com.angl.aggregation.engine.AggregationModul;
import com.angl.aggregation.engine.AggregationStarter;
import com.angl.crawlerapi.common.AggregationResponse;
import com.angl.crawlerapi.common.ApiResponse;
import com.angl.crawlerapi.common.LogMessage;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aggregation")
@CrossOrigin(origins = "*")
@EnableScheduling
public class AggregationController {
    @Autowired
    private SimpMessagingTemplate template;
    
    private Map<String, AggregationStarter> aggregationInfoList = new HashMap<>();
    private Map<String, AggregationModul> aggregationModulList = new HashMap<>();
    private Map<String, Thread> aggregationThreadList = new HashMap<>();
    
    
    @Scheduled(fixedRate = 1000)
    public void sendLog() {
        Map<String, LogMessage> logMap = new HashMap<>();
        aggregationInfoList.entrySet().forEach((entry) -> {
            try {
                logMap.put(entry.getKey(), new LogMessage(entry.getKey(), entry.getValue().takeMessage(), new Date()));
            } catch (InterruptedException ex) {
                logMap.put(entry.getKey(), new LogMessage(entry.getKey(),"Interrupted exception", new Date()));
            }
        });
        this.template.convertAndSend("/topic/aggregation",new AggregationResponse(logMap, aggregationInfoList));
    }
    
    @RequestMapping(value="/start", method=RequestMethod.POST)
    public ResponseEntity<ApiResponse> start(@RequestBody AggregationStarter starter) {
        try{
            if(starter.getAggregationType().equals(AggregationStarter.BASIC)){
                if(starter.getClusterNumber()<=0){starter.setClusterNumber(10);}
                if(starter.getMinItem()<=0){starter.setMinItem(2);}
            }else if(starter.getAggregationType().equals(AggregationStarter.POPULARITY)){
                if(starter.getDateToPopularity() == null){starter.setDateToPopularity(new Date());}
            }else if(starter.getAggregationType().equals(AggregationStarter.SPACE)){
                if(starter.getPointList().isEmpty()){throw new Exception("You did not set valid point list");}
                if(starter.getEps()<= 0){starter.setEps(0.5);}
                if(starter.getMinItem()<=0){starter.setMinItem(2);}
            }
            aggregationInfoList.put(starter.getId(), starter);
            AggregationModul modul = new AggregationModul(starter);
            aggregationModulList.put(starter.getId(), modul);
            Thread modulThread = new Thread(modul);
            modulThread.setName(starter.getName());
            modulThread.start();
            aggregationThreadList.put(starter.getId(), modulThread);
            return new ResponseEntity(new ApiResponse("Ok"),HttpStatus.OK);
        }catch(Exception ex){
            ex.printStackTrace();            
            return new ResponseEntity(new ApiResponse("Ok"),HttpStatus.OK);
        }
    }
    @RequestMapping(value="/stop", method=RequestMethod.POST)
    public ResponseEntity<List<AggregationStarter>> stop(@RequestBody AggregationStarter info) {
        if(aggregationInfoList.containsKey(info.getId())){
            System.out.println(info);
            aggregationThreadList.get(info.getId()).interrupt();
            aggregationInfoList.remove(info.getId());
            aggregationModulList.remove(info.getId());
            aggregationThreadList.remove(info.getId());
        }else{
            System.out.println("Nincs ilyen");
        }
        return new ResponseEntity(this.aggregationInfoList,HttpStatus.OK);
    }
    @RequestMapping(value="/pause", method=RequestMethod.POST)
    public ResponseEntity<List<AggregationStarter>> pause(@RequestBody AggregationStarter info) {
        System.out.println(info);
        if(aggregationInfoList.containsKey(info.getId())){
            System.out.println(info);
            AggregationModul modul = aggregationModulList.get(info.getId());
            System.out.println("Pause form controller");
            modul.pause();
        }else{
            System.out.println("Nincs ilyen");
        }
        return new ResponseEntity(this.aggregationInfoList,HttpStatus.OK);
    }
}
