package com.angl.crawlerapi.controllers;

import com.angl.angldao.aggregation.SpaceAggregation;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.main.VideoStatistic;
import com.angl.angldao.models.Video;
import com.angl.crawlerapi.common.ApiResponse;
import com.angl.crawlerapi.common.CrawlerSettings;
import com.angl.crawlerapi.common.LogMessage;
import com.angl.crawlerapi.common.ServiceInfoFileHandler;
import com.angl.crawlerapi.common.ServiceResponse;
import com.angl.crawlerapi.common.ServiceStarter;
import crawler.CrawlService;
import crawler.utils.ServiceInfo;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service")
@CrossOrigin(origins = "*")
@EnableScheduling
public class CrawlController {
    
    @Value("${crawler.servicePath}")
    private String servicePath;
    
    @Autowired
    private SimpMessagingTemplate template;
    private Map<String,ServiceInfo> serviceInfoList = new HashMap<>();
    private Map<String,ServiceInfo> killedServiceInfoList = new HashMap<>();
    private Map<String,CrawlService> serviceList = new HashMap<>();
    private Map<String,Thread> serviceThreadList = new HashMap<>();
    
    @Scheduled(fixedRate = 1000)
    public void sendLog() {
        Map<String, LogMessage> logMap = new HashMap<>();
        serviceInfoList.entrySet().forEach((entry) -> {
            try {
                logMap.put(entry.getKey(), new LogMessage(entry.getKey(), entry.getValue().takeMessage(), new Date()));
            } catch (InterruptedException ex) {
                logMap.put(entry.getKey(), new LogMessage(entry.getKey(),"Interrupted exception", new Date()));
            }
        });
        this.template.convertAndSend("/topic/log",new ServiceResponse(logMap, serviceInfoList));
    }
    
    @RequestMapping("/list")
    public ResponseEntity<List<ServiceInfo>> list() {
        Video.setBorderTime(new Long(24));
        System.out.println("Border time: "+ Video.getBorderTime());
        return new ResponseEntity(this.serviceInfoList, HttpStatus.OK);
    }
    
    @RequestMapping("/killed_list")
    public ResponseEntity<List<ServiceInfo>> killedList() {
        if(this.killedServiceInfoList.isEmpty()){
            ServiceInfoFileHandler.path = this.servicePath;
            ServiceInfoFileHandler.getInstance().read().forEach((info) -> {
                info.setId(ServiceInfo.createID());
                info.setStartDate(new Date());
                this.killedServiceInfoList.put(info.getId(), info);
            });
        }
        return new ResponseEntity(this.killedServiceInfoList, HttpStatus.OK);
    }
    @RequestMapping(value="/start", method=RequestMethod.POST)
    public ResponseEntity<ApiResponse> start(@RequestBody ServiceStarter starter) {
        try{
        ServiceInfo info;
        if(starter.getType().equals("facebook")){
            info = new ServiceInfo(new Date(), starter.getName(), starter.getType(), starter.getMode(), 
                    starter.getEast(),starter.getWest(), starter.getSouth(), starter.getNorth(), starter.getVideoCount().toString(), starter.getMaxViewer().toString());
            info.setCoordinates(starter.getCooirdinate());
        }else if(starter.getType().equals("twitter")){
            info = new ServiceInfo(new Date(), starter.getName(), starter.getType(), starter.getMode(), 
                    Double.parseDouble(starter.getLongitude()), Double.parseDouble(starter.getLatitude()), Double.parseDouble(starter.getRadius()), starter.getVideoCount().toString());
            info.setCoordinates(starter.getCooirdinate());
        }else if(starter.getType().equals("custom")){
            info = new ServiceInfo(new Date(), starter.getName(), starter.getType(), starter.getMode(), 
                    starter.getProviderName(), starter.getListUrl(),starter.getDetailsUrl());
            info.setCoordinates(starter.getCooirdinate());
        }else{
            info = new ServiceInfo(new Date(), starter.getName(), starter.getType(), starter.getMode(), starter.getCooirdinate());
        }
        CrawlService service = new CrawlService(info);
        Thread crawlThread = new Thread(service);
        crawlThread.setName("Crawl_"+info.getInfoName());
        crawlThread.start();
        serviceInfoList.put(info.getId(),info);
        serviceList.put(info.getId(),service);
        serviceThreadList.put(info.getId(),crawlThread);
        return new ResponseEntity(new ApiResponse("Ok"),HttpStatus.OK);
        }catch(Exception ex){
            ex.printStackTrace();            
            return new ResponseEntity(new ApiResponse("Fail"),HttpStatus.OK);
        }
    }
    
    @RequestMapping(value="/restart", method=RequestMethod.POST)
    public ResponseEntity<ApiResponse> restart(@RequestBody ServiceInfo info) {
        try{
            killedServiceInfoList.remove(info.getId());
            CrawlService service = new CrawlService(info);
            Thread crawlThread = new Thread(service);
            crawlThread.setName("Crawl_"+info.getInfoName());
            crawlThread.start();
            serviceInfoList.put(info.getId(),info);
            serviceList.put(info.getId(),service);
            serviceThreadList.put(info.getId(),crawlThread);
        return new ResponseEntity(new ApiResponse("Ok"),HttpStatus.OK);
        }catch(Exception ex){
            ex.printStackTrace();            
            return new ResponseEntity(new ApiResponse("Fail"),HttpStatus.OK);
        }
    }

    @RequestMapping(value="/stop", method=RequestMethod.POST)
    public ResponseEntity<List<ServiceInfo>> stop(@RequestBody ServiceInfo info) {
        if(serviceInfoList.containsKey(info.getId())){
            System.out.println(info);
            serviceList.get(info.getId()).stopCrawl();
            serviceThreadList.get(info.getId()).interrupt();
            killedServiceInfoList.put(info.getId(), info);
            serviceInfoList.remove(info.getId());
            serviceList.remove(info.getId());
            serviceThreadList.remove(info.getId());            
            ServiceInfoFileHandler.path = this.servicePath;
            ServiceInfoFileHandler.getInstance().save(info);
        }else{
            System.out.println("Nincs ilyen");
        }
        return new ResponseEntity(this.serviceInfoList,HttpStatus.OK);
    }
    @RequestMapping(value="/pause", method=RequestMethod.POST)
    public ResponseEntity<List<ServiceInfo>> pause(@RequestBody ServiceInfo info) {
        if(serviceInfoList.containsKey(info.getId())){
            System.out.println(info);
            CrawlService service = serviceList.get(info.getId());
            System.out.println("Pause form controller");
            service.pauseCrawl();
        }else{
            System.out.println("Nincs ilyen");
        }
        return new ResponseEntity(this.serviceInfoList,HttpStatus.OK);
    }
    @RequestMapping(value="/settings", method=RequestMethod.POST)
    public ResponseEntity<CrawlerSettings> settings(@RequestBody CrawlerSettings settings) {
        try{
            if(settings.getBorderOfLifetime() != null && settings.getBorderOfLifetime() >1){Video.setBorderTime(settings.getBorderOfLifetime());}
            if(settings.getTwitterLong() != null && settings.getTwitterLong() > 0){crawler.provider.implementations.Twitter.setTwitterLong(settings.getTwitterLong());}
            if(settings.getMinElementCountToCreate()!= null && settings.getMinElementCountToCreate() > 0){SpaceAggregation.setMinElementCountToCreate(settings.getMinElementCountToCreate());}
            if(settings.getPeriscopePlusHours()!= null && settings.getPeriscopePlusHours() > 0){crawler.provider.implementations.Periscope.setPlusHours(settings.getPeriscopePlusHours());}
            if(settings.getApiKey()!= null && !settings.getApiKey().isEmpty()){
                crawler.utils.LocationHelper.setAPI_KEY(settings.getApiKey()); 
                com.angl.angldao.main.LocationHelper.setAPI_KEY(settings.getApiKey());
            }
            if(settings.getPlusPerEcho() != null && settings.getPlusPerEcho()>0){Video.setPlusPerEcho(settings.getPlusPerEcho());}
        return new ResponseEntity(settings,HttpStatus.OK);
        }catch(Exception ex){
            ex.printStackTrace();
            settings.setBorderOfLifetime(Video.getBorderTime());
            settings.setTwitterLong(crawler.provider.implementations.Twitter.getTwitterLong());
            settings.setMinElementCountToCreate(SpaceAggregation.getMinElementCountToCreate());
            settings.setPeriscopePlusHours(crawler.provider.implementations.Periscope.getPlusHours());
            settings.setApiKey(crawler.utils.LocationHelper.getAPI_KEY());
            settings.setPlusPerEcho(Video.getPlusPerEcho());
            return new ResponseEntity(settings,HttpStatus.OK);
        }
    }
    @RequestMapping(value="/settings", method=RequestMethod.GET)
    public ResponseEntity<CrawlerSettings> getSettings() {
       CrawlerSettings cs = new CrawlerSettings();
       cs.setBorderOfLifetime(Video.getBorderTime());
       cs.setTwitterLong(crawler.provider.implementations.Twitter.getTwitterLong());
       cs.setMinElementCountToCreate(SpaceAggregation.getMinElementCountToCreate());
       cs.setPeriscopePlusHours(crawler.provider.implementations.Periscope.getPlusHours());
       cs.setApiKey(crawler.utils.LocationHelper.getAPI_KEY());
       cs.setPlusPerEcho(Video.getPlusPerEcho());
       return new ResponseEntity(cs,HttpStatus.OK);
    }
    @RequestMapping(value="/stats", method=RequestMethod.GET)
    public ResponseEntity<List<VideoStatistic>> getStats() {
        List<VideoStatistic> result = new ArrayList<>();
        try{
            DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
            dao.openSession();
            result = ((VideoDAO)dao).getStatisticByDb();
            dao.closeSession();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ResponseEntity<ApiResponse> checkRun() {      
       return new ResponseEntity(new ApiResponse("Run"),HttpStatus.OK);
    }
    
}
