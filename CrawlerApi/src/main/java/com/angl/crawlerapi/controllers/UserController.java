/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.crawlerapi.controllers;

import com.angl.crawlerapi.common.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*")
public class UserController {
    @RequestMapping(value="/login", method=RequestMethod.POST)
    public ResponseEntity<User> login(@RequestBody User user){
       Boolean validUser = user.isValid();
       user.setPassword("");
       if(validUser)return new ResponseEntity(user, HttpStatus.OK);
       return new ResponseEntity(null, HttpStatus.FORBIDDEN);
    }
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ResponseEntity<String> test(){       
       
       return new ResponseEntity("Bulivan!", HttpStatus.OK);
    }
    @RequestMapping(value="/test", method=RequestMethod.POST)
    public ResponseEntity<String> test(@RequestBody String user){
        System.out.println(user);
        return new ResponseEntity("OK", HttpStatus.OK);
    }
}
