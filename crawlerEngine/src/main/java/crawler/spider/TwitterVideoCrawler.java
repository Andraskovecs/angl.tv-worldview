package crawler.spider;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import crawler.engine.JSONPReader;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.utils.ServiceInfo;
import crawler.utils.VideoCrawlerData;
import twitter4j.GeoLocation;
import twitter4j.MediaEntity;
import twitter4j.Query;
import twitter4j.Query.Unit;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.URLEntity;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterVideoCrawler extends VideoCrawler {

    protected Twitter twitter;

    protected double geo_lat = 47.497912;//37.781157;//40.2677882;//
    protected double geo_lon = 19.040234;//-122.398720;//-75.9737522;//
    protected double radius = 100.0;
    protected int video_count = 30;

    protected String consumerKey = "jcZBiZL3yrSZCpVG9Pd1C2UbA";
    protected String consumerSecret = "bUOZsn4dbhymxlDnGEb4rzv4IhcpUVdMfQiyF2knTIY1vilWgg";
    protected String accessToken = "907569096165924865-R4EtqYsoqyKOBXmc2zf06hN2zVszJbr";
    protected String accessTokenSecret = "tzFAkSUk1LIKFUtTdyfMmznNDEU8Ko4holA0EKZTnKzdX";

    private static class SingletonHelper {
        private static final TwitterVideoCrawler INSTANCE = new TwitterVideoCrawler();
    }

    public static TwitterVideoCrawler getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public TwitterVideoCrawler() {
        super();
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true).setOAuthConsumerKey(consumerKey).setOAuthConsumerSecret(consumerSecret).setOAuthAccessToken(accessToken).setOAuthAccessTokenSecret(accessTokenSecret);

        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter = tf.getInstance();
    }

    public Boolean login() {
        Boolean success = false;
        loggedIn = success;
        return loggedIn;
    }
    protected void buildSearchEngineAndRequest() {
    }
    protected static String getVideoLink(Status status) {
        String result = null;
        try{
            MediaEntity[] mediaEntites = status.getMediaEntities();
            URLEntity[] urlEntites = status.getURLEntities();
            // TODO zsolturo handle multiple links
            if (mediaEntites.length != 0) {
                result = mediaEntites[0].getExpandedURL();
            } else if (urlEntites.length != 0) {
                result = urlEntites[0].getExpandedURL();
            }
        }catch(Exception e){
            CrawlLogger.getInstance().sendError(e);
        }
        return result;
    }

    @Override
    public JsonObject search() {
        JsonObject searchResult = null;
        try{
            Query query = new Query("filter:native_video");
            query.setGeoCode(new GeoLocation(geo_lat, geo_lon), radius, Unit.km);
            query.setCount(video_count);
            JsonArray videoLinkList = new JsonArray();
            QueryResult result;
                result = twitter.search(query);
                for (Status status : result.getTweets()) {
                    String videoLink = TwitterVideoCrawler.getVideoLink(status);
                    if(videoLink != null){
                        videoLinkList.add(videoLink);
                    }
                } 
            searchResult = new JsonObject();
            searchResult.add("results", videoLinkList);
        }catch (TwitterException e) {
            CrawlLogger.getInstance().sendError(e);
        }catch(Exception e){
            CrawlLogger.getInstance().sendError(e);
        }
        return searchResult;
    }

    @Override
    synchronized public List<Object> extractVideoList(ServiceInfo info) {
        if (info.isValidTwitterData()) {
            this.geo_lat = info.getLatitude();
            this.geo_lon = info.getLongitude();
            this.radius = info.getRadius();
            this.video_count = Integer.parseInt(info.getVideoCount());
        }
        CrawlLogger.getInstance().sendCommon("aLat: " + this.geo_lat + " Longi: " + this.geo_lon + " radius:" + this.radius);
        List<Object> fullVideoIdLinks = new ArrayList<>();
        try{
            List<Object> result = new ArrayList<>();
            JsonObject jsonObject = search();
            if (jsonObject != null) {
                JSONPReader engine = new JSONPReader(jsonObject.toString());
                result.addAll(engine.execList("$.results"));
            }
            for (Object videoLink : result) {
                if (videoLink instanceof String) {
                    fullVideoIdLinks.add(new VideoCrawlerData((String) videoLink, new JsonObject()));
                }
            }
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }
        return fullVideoIdLinks;
    }

    @Override
    public String getType() {
        return CrawlService.TYPETWITTER;
    }

    public double getGeo_lat() {
        return geo_lat;
    }

    public double getGeo_lon() {
        return geo_lon;
    }
    
}
