package crawler.spider;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import crawler.engine.HttpSessionEngine;
import crawler.engine.JSONPReader;
import crawler.engine.RegexpEngine;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.utils.ServiceInfo;
import crawler.utils.VideoCrawlerData;

public class FacebookVideoCrawler extends VideoCrawler {

    private static final Logger logger = LogManager.getLogger();

    protected static final String START_URL = "https://www.facebook.com/livemap/";
    protected static final String SEARCH_URL = "https://www.facebook.com/api/graphql/";

    protected HttpSessionEngine searchEngine;
    protected HttpUriRequest searchRequest;

    protected String lsd;
    protected String doc_id;
    protected String single_doc_id;

    protected String west = "-78.0103567";//"13.42529296875";
    protected String east = "38.7203342";//"24.10400390625";
    protected String south = "-72.6301777";//"43.96119063892024";
    protected String north = "41.8887628";//"48.748945343432936";

    protected String video_count = "10";
    protected String max_viewers = "10000";

    protected static final String VARIABLES_FMT_STRING = "{\"queryArgs\":{\"level\":\"LOKI\",\"video_count\":%s,\"bounds\":{\"west\":%s,\"east\":%s,\"south\":%s,\"north\":%s},\"max_viewers\":%s,\"force_location_check\":true}}";
    protected String variables = String.format(VARIABLES_FMT_STRING, video_count, west, east, south, north, max_viewers);

    private static class SingletonHelper {
        private static final FacebookVideoCrawler INSTANCE = new FacebookVideoCrawler();
    }

    public static FacebookVideoCrawler getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public FacebookVideoCrawler() {
        super();
    }

    @Override
    public Boolean login() {
        Boolean success = false;
        RegexpEngine engine = null, jsregengine = null, docIdEngine = null;
        try{
            HttpSessionEngine startEngine = new HttpSessionEngine.HttpSessionEngineBuilder().build();
            RequestBuilder startRequestBuilder = RequestBuilder.get(START_URL);
            setupHeaders(startRequestBuilder, COMMON_HEADERS);

            startRequestBuilder.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            startEngine.call(startRequestBuilder.build());
            String startContent = startEngine.getContent();
            startEngine.closeClient();
            engine = new RegexpEngine(startContent, true);
            lsd = engine.exec("\"LSD\",\\[\\],\\{\"token\":\"(\\w+)\"\\}");
            String id = engine.exec("Bootloader\\.setResourceMap\\((\\{.*\\})\\)");

            JSONPReader jsonpreader = new JSONPReader(id);
            List<String> sources = jsonpreader.execList("$..src");
            List<String> unescapedSources = sources.stream().map(StringEscapeUtils::unescapeJson).collect(Collectors.toList());
            List<String> filteredSources = unescapedSources.stream().filter(s -> s.matches(".*\\.js$")).collect(Collectors.toList());
            filteredSources = filteredSources.stream().filter(s -> !s.matches(".*v3/.*\\.js")).collect(Collectors.toList());
            Collections.reverse(filteredSources);

            String match = null;
            for (String jsUrl : filteredSources) {
                jsregengine = new RegexpEngine(jsUrl);
                if (jsregengine.getContent().contains("LiveMapVideoCacheActionsBasicPublicCurrentLiveVideosQuery")) {
                    match = jsUrl;
                    break;
                }
            }

            if (match != null) {
                docIdEngine = new RegexpEngine(match);
                logger.trace("docIdEngine: {}", docIdEngine.getContent());

                doc_id = docIdEngine.exec("\"LiveMapVideoCacheActionsBasicPublicCurrentLiveVideosQuery\",id:\"(\\d+)\"");
                logger.debug("doc_id: {}", doc_id);

                single_doc_id = docIdEngine.exec("\"LiveMapSidebarItem_VideoQuery\",id:\"(\\d+)\"");
                logger.debug("single_doc_id: {}", single_doc_id);

            }
            success = true;
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }
        finally{
            if(docIdEngine != null){docIdEngine.close();}
            if(jsregengine != null){jsregengine.close();}
            if(engine != null){engine.close();}
        }
        loggedIn = success;
        return loggedIn;
    }

    protected void buildSearchEngineAndRequest() throws UnsupportedEncodingException {

//		POST /api/graphql/ HTTP/1.1
//		Host: www.facebook.com
//		Connection: close
//		Content-Length: 372
//		Origin: https://www.facebook.com
//		User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.89 Chrome/62.0.3202.89 Safari/537.36
//		Content-Type: application/x-www-form-urlencoded
//		Accept: */*
//		Referer: https://www.facebook.com/livemap/
//		Accept-Encoding: gzip, deflate
//		Accept-Language: en-US,en;q=0.9
//		
//		lsd: AVoLWrRW
//		fb_api_caller_class: RelayModern
//		variables: urlencode() {"queryArgs":{"level":"LOKI","video_count":100,
//				"bounds":{"west":13.42529296875,"east":24.10400390625,
//				"south":43.96119063892024,"north":48.748945343432936},
//				"max_viewers":1000,"force_location_check":true}}
//		doc_id: 1526722354036851
        searchEngine = (searchEngine == null? new HttpSessionEngine.HttpSessionEngineBuilder().build():searchEngine);
        RequestBuilder searchRequestBuilder = RequestBuilder.post(SEARCH_URL);
        setupHeaders(searchRequestBuilder, COMMON_HEADERS);

        List<NameValuePair> params = new ArrayList<NameValuePair>(2);
        params.add(new BasicNameValuePair("lsd", lsd));
        params.add(new BasicNameValuePair("fb_api_caller_class", "RelayModern"));
        params.add(new BasicNameValuePair("variables", variables));
        params.add(new BasicNameValuePair("doc_id", doc_id));
        UrlEncodedFormEntity entity;
        entity = new UrlEncodedFormEntity(params, "UTF-8");
        searchRequestBuilder.setEntity(entity);
        searchRequest = searchRequestBuilder.build();
    }

    protected JsonObject transformVideoIdToFullURL(String videoId) {
        try{
            HttpSessionEngine videoResolveEngine = new HttpSessionEngine.HttpSessionEngineBuilder().build();
            RequestBuilder videoResolveRequestBuilder = RequestBuilder.post(SEARCH_URL);
            setupHeaders(videoResolveRequestBuilder, COMMON_HEADERS);

            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            params.add(new BasicNameValuePair("lsd", lsd));
            params.add(new BasicNameValuePair("fb_api_caller_class", "RelayModern"));
            params.add(new BasicNameValuePair("variables", String.format("{ \"video_id\": \"%s\" }", videoId)));
            params.add(new BasicNameValuePair("doc_id", single_doc_id));
            UrlEncodedFormEntity entity;
            try {
                entity = new UrlEncodedFormEntity(params, "UTF-8");
                videoResolveRequestBuilder.setEntity(entity);
            } catch (UnsupportedEncodingException e) {
                CrawlLogger.getInstance().sendError(e);
            }

            searchRequest = videoResolveRequestBuilder.build();
            videoResolveEngine.call(searchRequest);
            JsonObject result = videoResolveEngine.getJson();
            videoResolveEngine.closeClient();
            return result;
        }catch(Exception e){
            CrawlLogger.getInstance().sendError(e);
            return null;
        }
    }

    public JsonObject getPrivateVideoData(String videoId) {
        if(this.loggedIn){
            return this.transformVideoIdToFullURL(videoId);
        }
        CrawlLogger.getInstance().sendCommon("Nem volt j� a login a facebook crawlern�l");
        return null;
    }
    

    public JsonObject search() {
        JsonObject result = null;
        if(this.loggedIn){
            try {
                buildSearchEngineAndRequest();
                searchEngine.call(searchRequest);
                result = searchEngine.getJson();
            } catch (UnsupportedEncodingException ex) {
                CrawlLogger.getInstance().sendError(ex);
            }
        }else{
            CrawlLogger.getInstance().sendCommon("Nem volt j� a login a facebook crawlern�l");
        }
        return result;
    }

    @Override
    public String getType() {
        return CrawlService.TYPEFB;
    }
    @Override
    synchronized public List<Object> extractVideoList(ServiceInfo info) {
        if(info.isValidFacebookData()){
            this.east = info.getEast();
            this.west = info.getWest();
            this.south = info.getSouth();
            this.north = info.getNorth();
            this.max_viewers = info.getMaxViewer();
            this.video_count = info.getVideoCount();
            this.variables = String.format(VARIABLES_FMT_STRING, video_count, west, east, south, north, max_viewers);
        }
        CrawlLogger.getInstance().sendCommon(this.variables);        
        List<Object> fullVideoIdLinks = new ArrayList<>();
        try{
            List<String> videoIdLinks = new ArrayList<>();
            List<String> videoLocation = new ArrayList<>();
            JsonObject jsonObject = search();
            if (jsonObject != null) {
                JSONPReader engine = new JSONPReader(jsonObject.toString());
                videoIdLinks.addAll(engine.execList("$..videos..id"));
                videoLocation.addAll(engine.execList("$..videos..location"));
            }
            int i = 0;
            for (String videoId : videoIdLinks) {
                JsonObject result = transformVideoIdToFullURL(videoId);
                JSONPReader reader = new JSONPReader(result.toString());
                String url = reader.<String>exec(String.class, "$..url");
                if(url != null && !url.trim().isEmpty()){
                    JsonParser parser = new JsonParser();
                    JsonObject o = parser.parse(videoLocation.get(i)).getAsJsonObject();
                    result.add("location", o);
                    fullVideoIdLinks.add(new VideoCrawlerData(url, result));
                    i++;
                }
            }
        }catch(JsonSyntaxException ex){
             CrawlLogger.getInstance().sendError(ex);
        }catch(Exception ex){
             CrawlLogger.getInstance().sendError(ex);
        }
        return fullVideoIdLinks;
    }
}
