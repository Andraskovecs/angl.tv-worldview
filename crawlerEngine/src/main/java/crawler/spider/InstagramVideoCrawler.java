package crawler.spider;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.JsonObject;

import crawler.engine.HttpSessionEngine;
import crawler.engine.JSONPReader;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.utils.ServiceInfo;
import crawler.utils.VideoCrawlerData;

public class InstagramVideoCrawler extends VideoCrawler {

    protected static final String LOGIN_URL = "https://www.instagram.com/accounts/login/ajax/";
    protected static final String SEARCH_URL = "http://i.instagram.com/api/v1/discover/top_live/";
    protected static final String VIDEOID_URL = "http://i.instagram.com/api/v1/live/%s/info/";
    protected static final String VIDEOID_URL_COMMENT = "http://i.instagram.com/api/v1/live/%s/get_comment/";

    protected static final String SEARCH_USER_AGENT = "Instagram 10.26.0 (iPhone7,2; iOS 10_1_1; en_US; en-US; scale=2.00; gamut=normal; 750x1334) AppleWebKit/420+";

    public String username = "anglmartklikk";
    public String password = "GripgiOvVot4";

    protected String sessionid;
    protected String csrftoken;

    protected HttpSessionEngine searchEngine;
    protected HttpUriRequest searchRequest = null;

    private static class SingletonHelper {
        private static final InstagramVideoCrawler INSTANCE = new InstagramVideoCrawler();
    }

    public static InstagramVideoCrawler getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public InstagramVideoCrawler() {
        super();
        login();
    }
    
    public InstagramVideoCrawler(String username, String password) {
        super();

        this.username = username;
        this.password = password;

        login();
    }

    public void init() {
    }

    protected static final String DEFAULT_CSRF_TOKEN = "00000000000000000000000000000000";
    protected String csrfToken = DEFAULT_CSRF_TOKEN;

    protected static final Map<String, String> LOGIN_HEADERS;

    static {
        Map<String, String> header = new HashMap<String, String>();
        header.put("Origin", "https://www.instagram.com");
        header.put("X-Instagram-AJAX", "1");
        header.put("Referer", "https://www.instagram.com/");
        header.put("Accept", "*/*");
        header.put("X-CSRFToken", DEFAULT_CSRF_TOKEN);
        header.put("Cookie", String.format("csrftoken=%s;", DEFAULT_CSRF_TOKEN));
        LOGIN_HEADERS = Collections.unmodifiableMap(header);
    }

    protected static final Map<String, String> SEARCH_HEADERS;

    static {
        Map<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        header.put("Accept", "application/json");
        SEARCH_HEADERS = Collections.unmodifiableMap(header);
    }

    @Override
    public Boolean login() {
        Boolean success = false;
        try{
            HttpSessionEngine loginEngine = new HttpSessionEngine.HttpSessionEngineBuilder().build();
            RequestBuilder loginRequestBuilder = RequestBuilder.post(LOGIN_URL);
            setupHeaders(loginRequestBuilder, COMMON_HEADERS, LOGIN_HEADERS);

            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));
            UrlEncodedFormEntity entity;
            entity = new UrlEncodedFormEntity(params);
            loginRequestBuilder.setEntity(entity);
            HttpUriRequest request = loginRequestBuilder.build();
            loginEngine.call(request);
            JsonObject loginResponse = loginEngine.getJson();
            csrftoken = loginEngine.getCookie("csrftoken").getValue();
            sessionid = loginEngine.getCookie("sessionid").getValue();
            loginEngine.closeClient();
            success = "true".equals(loginResponse.get("authenticated").toString());
        }catch(UnsupportedEncodingException ex){
            CrawlLogger.getInstance().sendError(ex);
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }finally{
            
        }
        loggedIn = success;
        return loggedIn;
    }
    protected void buildSearchEngineAndRequest() {
        try{
            searchEngine = (searchEngine== null? new HttpSessionEngine.HttpSessionEngineBuilder()
                    .setUserAgent(SEARCH_USER_AGENT)
                    .build():searchEngine);
            RequestBuilder searchRequestBuilder = RequestBuilder.get(SEARCH_URL);
            setupHeaders(searchRequestBuilder, COMMON_HEADERS, SEARCH_HEADERS);

            searchRequestBuilder.removeHeaders("Cookie");
            searchRequestBuilder.addHeader("Cookie", String.format("sessionid=%s; csrftoken=%s;", sessionid, csrftoken));

            searchRequest = searchRequestBuilder.build();
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }
    }

    @Override
    public JsonObject search() {
        JsonObject searchResult = null;
        buildSearchEngineAndRequest();
        searchEngine.call(searchRequest);
        searchResult = searchEngine.getJson(); 
        return searchResult;
    }   

    public String getVideoDataFromId(String videoId) {
        String result = null;
        try{
            HttpSessionEngine getVideoDataEngine = new HttpSessionEngine.HttpSessionEngineBuilder()
                    .setUserAgent(SEARCH_USER_AGENT)
                    .build();
            RequestBuilder getVideoDataRequestBuilder = RequestBuilder.get(String.format(VIDEOID_URL, videoId));
            setupHeaders(getVideoDataRequestBuilder, COMMON_HEADERS, SEARCH_HEADERS);

            getVideoDataRequestBuilder.removeHeaders("Cookie");
            getVideoDataRequestBuilder.addHeader("Cookie", String.format("sessionid=%s; csrftoken=%s;", sessionid, csrftoken));

            HttpUriRequest getVideoDataRequest = getVideoDataRequestBuilder.build();
            getVideoDataEngine.call(getVideoDataRequest);

            result = getVideoDataEngine.getContent(); 
            getVideoDataEngine.closeClient();
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }
        return result;
    }
    public String getVideoCommentFromId(String videoId) {
        String result = null;
        try{
            HttpSessionEngine getVideoDataEngine = new HttpSessionEngine.HttpSessionEngineBuilder()
                    .setUserAgent(SEARCH_USER_AGENT)
                    .build();
            RequestBuilder getVideoDataRequestBuilder = RequestBuilder.get(String.format(VIDEOID_URL_COMMENT, videoId));
            setupHeaders(getVideoDataRequestBuilder, COMMON_HEADERS, SEARCH_HEADERS);

            getVideoDataRequestBuilder.removeHeaders("Cookie");
            getVideoDataRequestBuilder.addHeader("Cookie", String.format("sessionid=%s; csrftoken=%s;", sessionid, csrftoken));

            HttpUriRequest getVideoDataRequest = getVideoDataRequestBuilder.build();
            getVideoDataEngine.call(getVideoDataRequest);

            result = getVideoDataEngine.getContent();
            getVideoDataEngine.closeClient();
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }
        return result;
    }

    
    @Override
    synchronized public List<Object> extractVideoList(ServiceInfo info) {
        List<Object> result = new ArrayList<>();
        
        try{
            JsonObject jsonObject = search();
            if (jsonObject != null) {
                JSONPReader engine = new JSONPReader(jsonObject.toString());
                engine.execList("$..id").forEach((s) -> {
                    result.add(new VideoCrawlerData(s, new JsonObject()));
                });
            }
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }
        return result;
    }
    @Override
    public String getType() {
        return CrawlService.TYPEINSTA;
    }

}
