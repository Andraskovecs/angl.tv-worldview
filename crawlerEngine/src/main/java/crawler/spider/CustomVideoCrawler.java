/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.spider;

import com.google.gson.JsonObject;
import crawler.engine.JSONPEngine;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.utils.ServiceInfo;
import crawler.utils.VideoCrawlerData;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hans
 */
public class CustomVideoCrawler extends VideoCrawler{

    private String listLink = "http://www.fakeprovider.lh/?menu=list";
    private JSONPEngine engine;

    private static class SingletonHelper {
        private static final CustomVideoCrawler INSTANCE = new CustomVideoCrawler();
    }

    public static CustomVideoCrawler getInstance() {
        return SingletonHelper.INSTANCE;
    }
    
    @Override
    public Boolean login() {
        return true;
    }    

    @Override
    public JsonObject search() {
        return new JsonObject();
    }
    @Override
    synchronized public List<Object> extractVideoList(ServiceInfo info) {
        List<Object> fullVideoIdLinks = new ArrayList<>();
        try{
            List<String> ids = new ArrayList<>();
            if (info.isValidCustomData()) {
                this.listLink = info.getListLink();            
                engine = new JSONPEngine(this.listLink);
            }else{return fullVideoIdLinks;}
            String status = engine.exec("$.status");
            if(!status.equals("ok")){return fullVideoIdLinks;}
            ids = engine.execList("$.videoList..videoId");
            ids.forEach((id) -> {
                fullVideoIdLinks.add(new VideoCrawlerData((String)id, new JsonObject()));
            });
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }finally{
            if(engine != null){engine.close();}
        }
        return fullVideoIdLinks;
    }

    @Override
    public String getType() {
        return CrawlService.TYPECUSTOM;
    }
    
}
