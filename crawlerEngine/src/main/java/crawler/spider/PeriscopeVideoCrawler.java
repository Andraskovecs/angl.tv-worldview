package crawler.spider;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;

import crawler.engine.CSSPathEngine;
import crawler.engine.HttpSessionEngine;
import crawler.engine.JSONPReader;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.utils.ServiceInfo;
import crawler.utils.VideoCrawlerData;


public class PeriscopeVideoCrawler extends VideoCrawler {
	
	private static final Logger logger = LogManager.getLogger();
	
	protected static final String START_URL = "https://www.pscp.tv";
	protected static final String AUTHORIZE_URL = "https://proxsee.pscp.tv/api/v2/authorizeTokenPublic";
	protected static final String TOP_CHANNELS_URL = "https://channels.pscp.tv/v1/top/channels/broadcasts?languages=en&languages=en-gb";
	protected static final String GETBROADCASTSPUBLIC_URL = "https://proxsee.pscp.tv/api/v2/getBroadcastsPublic";
	
	protected static final String X_PERISCOPE_USER_AGENT = "X-Periscope-User-Agent: PeriscopeWeb/App (864bae45945f6a4e09758c6e89c5d45a6fb25be8) Chromium/62.0.3202.89 (Ubuntu;Chromium)";
	
	protected String sessionId;
	protected String csrf;
	protected String user_id;
	protected String authorizationToken;
	
	public PeriscopeVideoCrawler() {
		super();
	}
	
	private static class SingletonHelper {
		private static final PeriscopeVideoCrawler INSTANCE = new PeriscopeVideoCrawler();
	}

	public static PeriscopeVideoCrawler getInstance() {
		return SingletonHelper.INSTANCE;
	}
	
	protected static final String DEFAULT_CSRF_TOKEN = "00000000000000000000000000000000";
	protected String csrfToken = DEFAULT_CSRF_TOKEN;
	
	@Override
	public Boolean login() {
		Boolean success = false;
                JSONPReader loginResponse = null, authorizationEng = null;
                CSSPathEngine htmlDoc = null;
                try{
                    // page data
                    htmlDoc = new CSSPathEngine(START_URL);
                    String query = htmlDoc.exec("div[id='page-container']", "data-store");                   
                    //logger.debug("data-store: {}", query);

                    // csrf
                    loginResponse = new JSONPReader(query);
                    this.csrf = loginResponse.<String>exec(String.class,"$..csrf");;
                    //logger.debug("csrf: {}", csrf);
                    // sessionId
                    sessionId = loginResponse.<String>exec(String.class,"$..SessionToken.public.serviceToken.token.session_id");
                    //logger.debug("sessionId: {}", sessionId);        

                    // authorizationToken
                    HttpSessionEngine authorizeSessionCookieEngine = new HttpSessionEngine.HttpSessionEngineBuilder().build();
                    RequestBuilder authorizeRequestBuilder = RequestBuilder.get(AUTHORIZE_URL);
                    setupHeaders(authorizeRequestBuilder, COMMON_HEADERS);
                    authorizeRequestBuilder.addParameter("service", "channels");
                    authorizeRequestBuilder.addParameter("session_id", sessionId);
                    authorizeRequestBuilder.setHeader("Cookie", String.format("pscp-csrf=%s; mp_mixpanel__c=0;", csrf));

                    authorizeSessionCookieEngine.call((authorizeRequestBuilder.build()));
                    user_id = authorizeSessionCookieEngine.getCookie("user_id").getValue();

                    String authRespString = authorizeSessionCookieEngine.getContent();
                    authorizeSessionCookieEngine.closeClient();
                    authorizationEng = new JSONPReader(authRespString);
                    authorizationToken = authorizationEng.<String>exec(String.class, "$.authorization_token");

                    //logger.debug("authorizationToken: {}", authorizationToken);

                    success = true;
                }catch(Exception e){
                    CrawlLogger.getInstance().sendError(e);
                }finally{
                    if(htmlDoc != null){htmlDoc.close();}
                }
		loggedIn = success;
		return loggedIn;
	}

	@Override
	public JsonObject search() {
            JsonObject result = null;            
            if(this.loggedIn){
                JSONPReader topChannelJSONEngine = null;
                try{
                    // top channels response
                    HttpSessionEngine topChannelsEngine = new HttpSessionEngine.HttpSessionEngineBuilder().build();
                    RequestBuilder topChannelRequestBuilder = RequestBuilder.get(TOP_CHANNELS_URL);
                    setupHeaders(topChannelRequestBuilder, COMMON_HEADERS);
                    topChannelRequestBuilder.addHeader("Authorization", authorizationToken);
                    topChannelRequestBuilder.addHeader("X-Periscope-User-Agent", X_PERISCOPE_USER_AGENT);
                    topChannelRequestBuilder.setHeader("Cookie", String.format("pscp-csrf=%s; mp_mixpanel__c=0;", csrf));

                    topChannelsEngine.call((topChannelRequestBuilder.build()));
                    String topChannelsResponse = topChannelsEngine.getContent();
                    topChannelsEngine.closeClient();
                    //logger.debug("topChannelsResponse: {}", topChannelsResponse);

                    topChannelJSONEngine = new JSONPReader(topChannelsResponse);
                    List<String> topChannelsBIDS = topChannelJSONEngine.execList("$..BID");
                    //logger.debug("topChannelsBIDS: {}", topChannelsBIDS);

                    // get broadcasts public
                    HttpSessionEngine getBroadCastsEngine = new HttpSessionEngine.HttpSessionEngineBuilder().build();
                    RequestBuilder getBroadCastRequestBuilder = RequestBuilder.get(GETBROADCASTSPUBLIC_URL);
                    setupHeaders(getBroadCastRequestBuilder, COMMON_HEADERS);
                    getBroadCastRequestBuilder.setHeader("Cookie", String.format("pscp-csrf=%s; mp_mixpanel__c=0; user_id=%s", csrf, user_id));

                    String broadcast_ids = StringUtils.join(topChannelsBIDS, ',');

                    getBroadCastRequestBuilder.addParameter("broadcast_ids", broadcast_ids);
                    getBroadCastRequestBuilder.addParameter("only_public_publish", "true");

                    getBroadCastsEngine.call((getBroadCastRequestBuilder.build()));
                    result = getBroadCastsEngine.getJson();
                    getBroadCastsEngine.closeClient();
                }catch(Exception ex){
                    CrawlLogger.getInstance().sendError(ex);
                }
            }else{
                CrawlLogger.getInstance().sendCommon("Nem volt j� a login a periscope crawlern�l");
            }
            return result;
	}
	
	@Override
	public List<Object> extractVideoList(ServiceInfo info) {
            List<Object> result = new ArrayList<>();
            JsonObject searchResult = search();
            if(searchResult != null){
                JSONPReader resultEngine = null;
                try{
                    resultEngine = new JSONPReader(searchResult.toString());
                    List<String> videoIdList = resultEngine.execList("$..id");
                    for (String string : videoIdList) {
                        result.add(new VideoCrawlerData(string, new JsonObject()));
                    }
                }catch(Exception ex){
                    CrawlLogger.getInstance().sendError(ex);
                }
            }else{
                System.out.println("logedin: "+this.loggedIn);
                System.out.println("result: "+searchResult);
            }
            return result;
	}
	
	@Override
	public String getType() {
		return CrawlService.TYPEPERISCOPE;
	}

}
