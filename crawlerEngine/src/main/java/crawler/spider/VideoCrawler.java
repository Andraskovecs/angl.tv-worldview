package crawler.spider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.client.methods.RequestBuilder;

import com.google.gson.JsonObject;
import crawler.utils.ServiceInfo;

abstract public class VideoCrawler {

    protected Boolean loggedIn = false;

    public VideoCrawler() {
        init();
    }

    public Boolean isLoggedIn() {
        return loggedIn;
    }

    public void init() {
        login();
    }

    protected static final Map<String, String> COMMON_HEADERS;

    static {
        Map<String, String> header = new HashMap<String, String>();
        header.put("Accept-Encoding", "identity");
        header.put("Accept-Language", "en-US,en;q=0.9");
        COMMON_HEADERS = Collections.unmodifiableMap(header);
    }

    @SafeVarargs
    protected static void setupHeaders(RequestBuilder builder, Map<String, String>... headerSets) {
        for (Map<String, String> headers : headerSets) {
            for (Entry<String, String> header : headers.entrySet()) {
                builder.setHeader(header.getKey(), header.getValue());
            }
        }
    }

    abstract public Boolean login();

    abstract public JsonObject search();

    abstract public String getType();

    public List<Object> extractVideoList(ServiceInfo info) {
        return new ArrayList<Object>();
    }

}
