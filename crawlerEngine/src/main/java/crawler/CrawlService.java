package crawler;

import crawler.queue.Consumer;
import crawler.queue.Producer;
import crawler.utils.Message;
import crawler.utils.ServiceInfo;
import crawler.spider.CustomVideoCrawler;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import crawler.spider.FacebookVideoCrawler;
import crawler.spider.InstagramVideoCrawler;
import crawler.spider.PeriscopeVideoCrawler;
import crawler.spider.TwitterVideoCrawler;
import crawler.spider.VideoCrawler;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

public class CrawlService implements Runnable {
    private final AtomicBoolean running = new AtomicBoolean(true);
    public static final String MODEUPDATE = "update", MODEINSERT = "insert", TYPEFB = "facebook", TYPETWITTER = "twitter", TYPEINSTA = "instagram", TYPEPERISCOPE = "periscope", TYPECUSTOM = "custom";
    private static int queueSize = 50, turnCount = 5;
    private static Long sleepTime = 600000l;
    private Producer producer;
    private Consumer consumer;
    private Thread producerThread, consumerThread;
    private VideoCrawler videoCrawler;
    private BlockingQueue<Message> queue;
    private String mode, type;
    private Boolean run = true;
    private ServiceInfo info;

    public CrawlService(String mode, String type) {
        this.info = new ServiceInfo(new Date(), "defaultka", type, mode, "default");
    }

    public CrawlService(ServiceInfo info) {
        this.info = info;
    }

    @Override
    public void run() {
        CrawlLogger.getInstance().sendAnalytic(info, "CrawlService starting", "CrawlService", "run", 39);
        try {
            work();
        } catch (InterruptedException ex) {
            CrawlLogger.getInstance().sendAnalytic(info, "Stoped by interrupted", "CrawlService", "run", 43);
            return;
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
        }
    }
    private void work() throws InterruptedException{
        if (Thread.currentThread().isInterrupted()) {
            System.out.println(info.getInfoName()+" - Interrupted Producer:= " + Thread.currentThread().isInterrupted());
        }else{
            CrawlLogger.getInstance().sendAnalytic(info, "Producer and Consumer has been started " + this.info.getMode() + " mode with " + this.info.getType() + " type", "CrawlService", "work", 53);            
            if (this.info.getMode().equals(CrawlService.MODEINSERT)) {
                queue = new ArrayBlockingQueue<>(CrawlService.queueSize);
                videoCrawler = this.setVideoCrawler();
                if (videoCrawler != null) {
                    producer = new Producer(queue, videoCrawler);                    
                    producer.setInfo(this.info);
                    producerThread = new Thread(producer);
                    producerThread.setName("Producer_" + info.getInfoName());
                    producerThread.start();
                    consumer = new Consumer(queue, this.info);
                    consumerThread = new Thread(consumer);
                    consumerThread.setName("Consumer_" + info.getInfoName());
                    consumerThread.start();
                    while (running.get()) {
                        if(Thread.interrupted()){
                            throw new InterruptedException();
                        }
                        if (producer.getTurn() <= 0) {
                            producer.setTurn(turnCount);
                            queue = new ArrayBlockingQueue<>(CrawlService.queueSize);
                            CrawlLogger.getInstance().sendAnalytic(info, "Start new " + turnCount + " turn Queue size: "+queue.size(), "CrawlService", "work", 75);                            
                            if(!info.getType().equals(CrawlService.TYPEINSTA)){Thread.sleep(sleepTime);}
                        }
                    }
                } else {
                    CrawlLogger.getInstance().sendAnalytic(info, "The type (" + this.info.getType() + ") does not exist!", "CrawlService", "work", 80);                    
                }
            } else {//update mode            
                consumer = new Consumer(this.info);
                consumerThread = new Thread(consumer);
                consumerThread.setName("Consumer_" + info.getInfoName());
                consumerThread.start();
            }
        }
    }
    public static int getQueueSize() {
        return queueSize;
    }

    public static void setQueueSize(int queueSize) {
        CrawlService.queueSize = queueSize;
    }

    private VideoCrawler setVideoCrawler() {
        if (this.info.getType().equals(CrawlService.TYPEFB)) {
            return FacebookVideoCrawler.getInstance();
        }
        if (this.info.getType().equals(CrawlService.TYPEINSTA)) {
            return InstagramVideoCrawler.getInstance();
        }
        if (this.info.getType().equals(CrawlService.TYPETWITTER)) {
            return TwitterVideoCrawler.getInstance();
        }
        if (this.info.getType().equals(CrawlService.TYPECUSTOM)) {
            return CustomVideoCrawler.getInstance();
        }
        if (this.info.getType().equals(CrawlService.TYPEPERISCOPE)) {
            return PeriscopeVideoCrawler.getInstance();
        }
        return null;
    }

    public String getMode() {
        return mode;
    }

    public String getType() {
        return type;
    }

    synchronized public void stopCrawl() {
        System.out.println(info.getInfoName()+" - Call Consumer stop");
        if(consumer != null){consumer.stopUpdater();}
        if(consumerThread != null)consumerThread.interrupt();
        System.out.println(info.getInfoName()+" - Call Producer stop");      
        if(producerThread != null)producerThread.interrupt();
    }
    synchronized public void pauseCrawl(){
        if(info.getStatus().equals(ServiceInfo.PAUSED))info.setStatus(ServiceInfo.RUNNING);
        else info.setStatus(ServiceInfo.PAUSED);
        System.out.println(info.getInfoName()+" - Pause Producer");
        if(this.producer != null)this.producer.pauseProducer();        
        System.out.println(info.getInfoName()+" - Pause Consumer");
        if(this.consumer != null)this.consumer.pauseConsumer();        
    }
    
    public int actualQueueSize(){
        return this.queue.size();
    }
}
    
