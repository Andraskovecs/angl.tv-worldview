package crawler.queue;

import crawler.CrawlService;
import crawler.CrawlLogger;
import crawler.utils.Message;
import crawler.utils.ServiceInfo;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.models.Interest;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videodetails;
import com.angl.angldao.models.Videolabels;
import crawler.engine.ProxyWriter;
import java.util.concurrent.BlockingQueue;
import crawler.provider.Provider;
import crawler.provider.ProviderFactory;
import crawler.provider.implementations.Custom;
import crawler.provider.implementations.Periscope;
import crawler.utils.DataTransfer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.HibernateException;

public class Consumer implements Runnable {
    private final AtomicBoolean running = new AtomicBoolean(true);
    private BlockingQueue<Message> queue;
    private BlockingQueue<Video> updateQueue;
    private String mode, proveider;
    private volatile Boolean run = true;
    private ServiceInfo info;
    private long updateSleep = 60000L;
    private int round = 1, queueSize = 100;
    private int threadNumber = 3;
    private List<Updater> updaterList = new ArrayList<>();
    private List<Thread> updaterThreadList = new ArrayList<>();
    private @Getter @Setter boolean notified;
    

    public Consumer(BlockingQueue<Message> q, ServiceInfo info) {
        this.queue = q;
        this.info = info;
        this.mode = info.getMode();
    }

    public Consumer(ServiceInfo info) {
        this.mode = info.getMode();
        this.proveider = info.getType();
        this.info = info;
        this.updateQueue = new ArrayBlockingQueue<>(queueSize);
    }

    @Override
    public void run() {
        CrawlLogger.getInstance().sendAnalytic(info, "Consumer starting", "Consumer", "run", 36);
        try {
            this.info.setStatus(ServiceInfo.RUNNING);
            work();
        } catch (InterruptedException e) {
            CrawlLogger.getInstance().sendAnalytic(info, "Stoped by interrupted", "Consumer", "run", 41);           
            return;
        } catch (Exception e) {
            CrawlLogger.getInstance().sendError(e);
        }
    }

    public void work() throws Exception {
        ProviderFactory providerFactory = new ProviderFactory();
        while (running.get()) {
            if (Thread.currentThread().isInterrupted()) {
                System.out.println(info.getInfoName() + " - Interrupted Consumer:= " + Thread.currentThread().isInterrupted());
                throw new InterruptedException();
            } else {
                if (this.mode.equals(CrawlService.MODEUPDATE)) {
                    if (updateQueue != null && updateQueue.isEmpty()) {
                        info.setIsStopable(true);
                    } else if (updateQueue != null && !updateQueue.isEmpty() && info.getIsStopable() == true) {
                        info.setIsStopable(false);
                    }
                    this.updateDataFromDb();
                } else {
                    this.hasToPause();
                    if (queue.isEmpty()) {
                        info.setIsStopable(true);
                    } else if (!queue.isEmpty() && info.getIsStopable() == true) {
                        info.setIsStopable(false);
                    }
                    this.putDataFromMessage(providerFactory);
                }
            }
        }
    }

    private void putDataFromMessage(ProviderFactory providerFactory) throws InterruptedException, HibernateException {
        Message msg;
        msg = queue.take();
        Provider provider = providerFactory.getProvider(msg.getType(), msg.getUrl(), msg.getData(), info);        
        try {
            if(provider != null){
                provider.updateProperties();
            }
        } catch (Exception ex) {
            provider = null;
            CrawlLogger.getInstance().sendError(ex);
        }
        if (provider != null && provider.getVideoId() != null) {
            DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
            dao.openSession();
            Video videoData = ((VideoDAO) dao).getOneByVideoId(provider.getVideoId());
            if (videoData == null) {                
                try {
                    videoData = provider.saveVideo(new Video());
                    if(videoData.getProvidertype().equals(CrawlService.TYPEPERISCOPE)){
                        ProxyWriter.getInstance().pushDataToServer(new DataTransfer(DataTransfer.INSERT, videoData.getVideoid(), videoData.getDefaultstreamurl(), videoData.getDefaultthumbnailurl()));
                    }
                    ((VideoDAO) dao).saveEntity(videoData);
                    Videodetails details = provider.getDetails();
                    if(details != null){
                        details.setVideo(videoData);
                        ((VideoDAO) dao).saveEntity(details);
                    }
                    if(info.getType().equals(CrawlService.TYPECUSTOM)){
                        for(Interest keyword:((Custom)provider).getKeywordsList()){
                            Interest result = ((VideoDAO)dao).saveInterest(keyword);
                            dao.saveEntity(new Videolabels(result, videoData));
                        }
                    }
                    if(info.getType().equals(CrawlService.TYPEPERISCOPE)){                        
                        for(Interest keyword:((Periscope)provider).getKeywords()){
                            Interest result = ((VideoDAO)dao).saveInterest(keyword);
                            dao.saveEntity(new Videolabels(result, videoData));
                        }
                    }
                    info.increaseNewVideoCount();
                    CrawlLogger.getInstance().sendAnalytic(info, "New video: " + provider.getVideoId(), "Consumer", "putDataFromMessage", 75);
                } catch (Exception ex) {
                    CrawlLogger.getInstance().sendError(ex);
                    CrawlLogger.getInstance().sendAnalytic(info, "Error in new video insert videoId: " + provider.getVideoId()+" message: "+ex.getMessage(), "Consumer", "putDataFromMessage", 86);
                }
            } else {
                try {                    
                    videoData = provider.saveVideo(videoData);
                    if(videoData.getProvidertype().equals(CrawlService.TYPEPERISCOPE)){
                        ProxyWriter.getInstance().pushDataToServer(new DataTransfer(DataTransfer.UPDATE, videoData.getVideoid(), videoData.getDefaultstreamurl(), videoData.getDefaultthumbnailurl()));
                    }
                    ((VideoDAO) dao).updateEntity(videoData); 
                    info.increaseUpdateVideoCount();                    
                    CrawlLogger.getInstance().sendAnalytic(info, "Update  existing video: " + videoData.getVideoid(), "Consumer", "putDataFromMessage", 102);
                } catch (Exception ex) {             
                    if(videoData.getProvidertype().equals(CrawlService.TYPEPERISCOPE)){
                        ProxyWriter.getInstance().pushDataToServer(new DataTransfer(DataTransfer.DELETE, videoData.getVideoid(), videoData.getDefaultstreamurl(), videoData.getDefaultthumbnailurl()));
                    }
                    ((VideoDAO) dao).deleteVideo(videoData);
                    info.increaseDeletedVideoCount();
                    CrawlLogger.getInstance().sendError(ex);
                    CrawlLogger.getInstance().sendAnalytic(info, "Delete video because not supported: " + videoData.getVideoid(), "Consumer", "putDataFromMessage", 107);
                }
            }
            dao.closeSession();
        }
    }
    private void updateDataFromDb() throws InterruptedException, HibernateException {
            DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
            dao.openSession();
            List<Video> allLiveVideo;
            if(this.proveider.equals(CrawlService.TYPEFB)){
                allLiveVideo = ((VideoDAO) dao).getVideosToFacebook(Double.parseDouble(info.getWest()),
                                                                    Double.parseDouble(info.getEast()),
                                                                    Double.parseDouble(info.getSouth()),
                                                                    Double.parseDouble(info.getNorth()));
            }else{
                allLiveVideo = ((VideoDAO) dao).getOneToProvider(this.proveider);
            }
            dao.closeSession();
            CrawlLogger.getInstance().sendAnalytic(info, "Update Round: "+round, "Consumer", "putDataFromMessage", 102);             
            
            if(round == 1){
                for (int i = 0; i < threadNumber; i++) {
                    Updater updater = new Updater(updateQueue, info,"Updater "+i);
                    Thread updaterThread = new Thread(updater);
                    updaterThread.setName(info.getInfoName()+updater.getFromTo());
                    updaterThread.start();
                    this.updaterList.add(i, updater);
                    this.updaterThreadList.add(i, updaterThread);
                }
            }
            System.out.println(info.getInfoName()+" ---- All size: "+allLiveVideo.size() + " ----- VideoQueue size: "+this.updateQueue.size());
            if(this.updateQueue.isEmpty()){
                System.out.println("Is Empty");
                allLiveVideo.forEach((video) -> {
                    try {
                        this.updateQueue.put(video);
                    } catch (InterruptedException ex) {
                        System.out.println("Nem siker�lt felrakni a vide�t");
                    }
                });
            }else{System.out.println("Sleep");Thread.sleep(updateSleep);}
            this.round++;
    }

    public void pauseConsumer() {
        System.out.println(info.getInfoName() + " - Consumer called pause");
        this.run = !this.run;
        if (this.run == true) {
            System.out.println(info.getInfoName() + " - Consumer restart");
            synchronized (this) {
                this.notify();
            }
        }
        this.updaterList.forEach((updater)-> {
            updater.pauseUpdater();
        });
    }

    private void hasToPause() throws InterruptedException {
        if (this.run == false) {
            System.out.println(info.getInfoName() + " - Consumer wait");
            synchronized (this) {
                this.wait();
            }
        } 
    }
    
    synchronized public void stopUpdater() {
        System.out.println(info.getInfoName() + " - Call Updater stop");
        if(!updaterList.isEmpty()){
            updaterThreadList.forEach((thread) -> {
                thread.interrupt();
            });
            updaterList.clear();
            updaterThreadList.clear();
        }
    }
}
