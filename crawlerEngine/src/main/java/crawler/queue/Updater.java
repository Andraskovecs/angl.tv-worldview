package crawler.queue;

import crawler.CrawlLogger;
import crawler.utils.ServiceInfo;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.models.Video;
import com.google.gson.JsonObject;
import crawler.CrawlService;
import crawler.engine.ProxyWriter;
import crawler.provider.Provider;
import crawler.provider.ProviderFactory;
import crawler.spider.FacebookVideoCrawler;
import crawler.utils.DataTransfer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import lombok.Getter;
import lombok.Setter;

public class Updater implements Runnable {
    private final AtomicBoolean running = new AtomicBoolean(true);
    private static AtomicLong  idCounter = new AtomicLong();
    public static final String RUN="Run",END="End";
    private @Getter @Setter BlockingQueue<Video> videoQueue;
    private ServiceInfo info;
    private ProviderFactory providerFactory;
    private volatile Boolean run = true;
    private @Getter String fromTo;
    private @Getter
	@Setter String state = crawler.queue.Updater.RUN;

    public Updater(BlockingQueue<Video> videoQueue, ServiceInfo info, String fromTo) {
        this.videoQueue = videoQueue;
        this.info = info;
        providerFactory = new ProviderFactory();
        this.fromTo = fromTo+"_"+ idCounter.incrementAndGet();
    }

    @Override
    public void run() {
        CrawlLogger.getInstance().sendAnalytic(info, "Updater-"+fromTo+" starting", "Updater-"+fromTo, "run", 35);
        try {
            
            work();
        } catch (InterruptedException ex) {
            System.out.println("interrupt"+this.fromTo);
            CrawlLogger.getInstance().sendAnalytic(info, "Stoped by interrupted Updater-"+fromTo, "Updater-"+fromTo, "run", 39);
        } catch (Exception ex) {
            ex.printStackTrace();
            CrawlLogger.getInstance().sendError(ex);
        }
    }

    private void work() throws InterruptedException {
        while (running.get()) {
            if (Thread.currentThread().isInterrupted()) {
                System.out.println(info.getInfoName() + " - Interrupted Updater-"+fromTo+":= " + Thread.currentThread().isInterrupted());
                throw new InterruptedException();
            } else {
                this.hasToPause();
                this.updateDatabase();
            }
        }
    }    

    public void pauseUpdater() {
        System.out.println(info.getInfoName() + " - Updater-"+fromTo+" called pause");
        this.run = !this.run;
        if (this.run == true) {
            System.out.println(info.getInfoName() + " - Updater-"+fromTo+" restart");
            synchronized (this) {
                this.notify();
            }
        }
    }

    private void hasToPause() throws InterruptedException {
        if (this.run == false) {
            System.out.println(info.getInfoName() + " - Updater-"+fromTo+" wait");
            synchronized (this) {
                this.wait();
            }
        }
    }

    private void updateDatabase(){ 
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        dao.openSession();
        Video videoData = null;
        try{
            videoData = videoQueue.take();
        }catch(InterruptedException ex){
            ex.printStackTrace();
            System.out.println("Video take is interrupted!");
        }
        if(videoData != null){
            if (videoData.isNeeded()) {           
                JsonObject data = (!videoData.getProvidertype().equals(CrawlService.TYPEFB) ? null : FacebookVideoCrawler.getInstance().getPrivateVideoData(videoData.getVideoid()));
                Provider provider = providerFactory.getProvider(videoData.getProvidertype(), videoData.getOriginalurl(), data, info);
                try {
                    if (provider != null) {provider.updateProperties();}
                } catch (Exception ex) {
                    provider = null;
                    CrawlLogger.getInstance().sendError(ex);
                }
                if (provider != null && provider.getVideoId() != null && !provider.getVideoId().trim().isEmpty()) {
                    try {
                        videoData = provider.saveVideo(videoData);
                        if(videoData.getProvidertype().equals(CrawlService.TYPEPERISCOPE)){
                            ProxyWriter.getInstance().pushDataToServer(new DataTransfer(DataTransfer.UPDATE, videoData.getVideoid(), videoData.getDefaultstreamurl(), videoData.getDefaultthumbnailurl()));
                        }
                        ((VideoDAO) dao).updateEntity(videoData);
                        info.increaseUpdateVideoCount();
                        CrawlLogger.getInstance().sendAnalytic(info, "Updater-"+fromTo+" Update  existing video: " + videoData.getVideoid(), "Consumer", "putDataFromMessage", 102);
                    } catch (Exception ex) {
                        if(videoData.getProvidertype().equals(CrawlService.TYPEPERISCOPE)){
                            ProxyWriter.getInstance().pushDataToServer(new DataTransfer(DataTransfer.DELETE, videoData.getVideoid(), videoData.getDefaultstreamurl(), videoData.getDefaultthumbnailurl()));
                        }
                        ((VideoDAO) dao).deleteVideo(videoData);
                        info.increaseDeletedVideoCount();
                        CrawlLogger.getInstance().sendError(ex);
                        CrawlLogger.getInstance().sendAnalytic(info, "Updater-"+fromTo+" Delete video because not supported: " + videoData.getVideoid(), "Consumer", "putDataFromMessage", 107);
                    }
                } else {
                    if(videoData.getProvidertype().equals(CrawlService.TYPEPERISCOPE)){
                        ProxyWriter.getInstance().pushDataToServer(new DataTransfer(DataTransfer.DELETE, videoData.getVideoid(), videoData.getDefaultstreamurl(), videoData.getDefaultthumbnailurl()));
                    }
                    ((VideoDAO) dao).deleteVideo(videoData);
                    info.increaseDeletedVideoCount();
                    CrawlLogger.getInstance().sendAnalytic(info, "Updater-"+fromTo+" Delete video because not supported from provider: " + videoData.getVideoid(), "Consumer", "putDataFromMessage", 107);
                }
            } else {
                if(videoData.getProvidertype().equals(CrawlService.TYPEPERISCOPE)){
                    ProxyWriter.getInstance().pushDataToServer(new DataTransfer(DataTransfer.DELETE, videoData.getVideoid(), videoData.getDefaultstreamurl(), videoData.getDefaultthumbnailurl()));
                }
                ((VideoDAO) dao).deleteVideo(videoData);
                info.increaseDeletedVideoCount();
                CrawlLogger.getInstance().sendAnalytic(info, "Updater-"+fromTo+" Delete video because not need: " + videoData.getVideoid() + " publishDate: " + videoData.getPublishdate(), "Consumer", "updateDataFromDb", 135);            
            }
        }else{
            System.out.println("A video NULL");
        }
        dao.closeSession(); 
    }

}
