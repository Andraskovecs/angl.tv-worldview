package crawler.queue;

import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.utils.Message;
import crawler.utils.ServiceInfo;
import crawler.utils.VideoCrawlerData;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import crawler.spider.VideoCrawler;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

public class Producer implements Runnable {
    private final AtomicBoolean running = new AtomicBoolean(true);
    private static final Long sleepTime = 1000l*30;
    private int turn;

    private BlockingQueue<Message> queue;
    private VideoCrawler videoCrawler;
    private List<VideoCrawlerData> allVideo = new ArrayList<>();
    protected String type;
    private volatile Boolean run = true;
    private ServiceInfo info;
    private boolean periscopeRelogin = false;
    private Long referenceDate;

    public Producer(BlockingQueue<Message> q, VideoCrawler videoCrawler) {
        this.queue = q;
        this.videoCrawler = videoCrawler;
        type = videoCrawler.getType();
        this.turn = 5;
        this.referenceDate = new Date().getTime();
    }

    @Override
    public void run() {
        CrawlLogger.getInstance().sendAnalytic(info, "Producer starting", "Producer", "run", 35);
        try {
            work();
        } catch (InterruptedException ex) {
            CrawlLogger.getInstance().sendAnalytic(info, "Stoped by interrupted", "Producer", "run", 39);
            return;
        } catch (Exception ex) {
            CrawlLogger.getInstance().sendError(ex);
        }
    }

    synchronized private void waitForNewTurn() throws InterruptedException {
        this.turn--;
        if (this.turn <= 0) {
            this.allVideo.clear();
            CrawlLogger.getInstance().sendAnalytic(info, "Wait for turn count", "Producer", "waitForNewTurn", 50);
            this.wait();
        } else {
            CrawlLogger.getInstance().sendAnalytic(info, "Start " + this.turn + ". turn", "Producer", "waitForNewTurn", 54);
        }
    }

    public int getTurn() {
        return turn;
    }

    synchronized public void setTurn(int turn) {
        if (turn > 0) {
            this.notify();
        }
        this.turn = turn;
    }

    public void pauseProducer() {
        System.out.println(info.getInfoName() + " - Producer called pause");
        this.run = !this.run;
        if (this.run == true) {
            info.setStatus(ServiceInfo.RUNNING);
            System.out.println(info.getInfoName() + " - Producer restart");
            synchronized (this) {
                this.notify();
            }
        }
    }

    private void hasToPause() throws InterruptedException {
        if (this.run == false) {
            System.out.println(info.getInfoName() + " - Producer wait");
            synchronized (this) {
                this.wait();
            }
        }
    }

    private void work() throws InterruptedException {
        this.info.setStatus(ServiceInfo.RUNNING);
        while (running.get()) {
            if (Thread.currentThread().isInterrupted()) {
                System.out.println(info.getInfoName() + " - Interrupted Producer:= " + Thread.currentThread().isInterrupted());                
                throw new InterruptedException();
            } else {
                this.hasToPause();
                this.waitForNewTurn();
                this.calcRunningTime();
                if(videoCrawler.getType().equals(CrawlService.TYPEPERISCOPE) && periscopeRelogin){
                    CrawlLogger.getInstance().sendAnalytic(info, "RELOGIN", "Producer", "work", 99);
                    videoCrawler.login();
                }
                List<Object> result = videoCrawler.extractVideoList(this.info);
                CrawlLogger.getInstance().sendAnalytic(info, "Result count: " + result.size(), "Producer", "work", 99);
                for (Object s : result) {
                    if (s instanceof VideoCrawlerData) {
                        VideoCrawlerData vcd = (VideoCrawlerData) s;
                        if (!allVideo.contains(vcd) && vcd.getUrl() != null) {
                            allVideo.add(vcd);
                            Message msg = new Message(type, vcd.getUrl(), vcd.getData());
                            queue.put(msg);
                        }
                    }
                }
                CrawlLogger.getInstance().sendAnalytic(info, "All videos count: " + allVideo.size(), "Producer", "work", 110);
                Thread.sleep(Producer.sleepTime);
            }
        }
    }

    public void setInfo(ServiceInfo info) {
        this.info = info;
    }

    private void calcRunningTime() {
        final long halfAnHoursInMillis = 30L * 60L * 1000L;
        long now = new Date().getTime();
        long reloginPeriod = new Date(referenceDate+halfAnHoursInMillis).getTime();
        if(now>= reloginPeriod){
            this.periscopeRelogin = true;
            this.referenceDate = new Date().getTime();
        }else{this.periscopeRelogin = false;}
        
    }

}
