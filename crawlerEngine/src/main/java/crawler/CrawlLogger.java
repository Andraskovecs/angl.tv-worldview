package crawler;

import crawler.utils.ServiceInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CrawlLogger {
    private static final Logger logCommon = LogManager.getLogger("commons-log");
    private static final Logger logAnalytics = LogManager.getLogger("analytics-log");
    private static final Logger logError = LogManager.getLogger("error-log");
    private static final Logger logCrawler = LogManager.getLogger("crawler-log");

    private static class SingletonHelper {
        private static final CrawlLogger INSTANCE = new CrawlLogger();
    }

    public static CrawlLogger getInstance() {
        return SingletonHelper.INSTANCE;
    }
    public void sendError(Exception ex){
        if(ex instanceof CrawlerProviderException){
            logCrawler.error("Crawler error message: {}",ex.getMessage());
            logCrawler.error("{}","-------------------------------------------------------------------------------------------------------------------------");
        }else{
            logError.error("Exception class: {}",ex.getClass().toGenericString());
            logError.error("Exception message: {}",ex.getMessage());
            int i = 1;
            String trace = "Exception trace";
            for(StackTraceElement ste: ex.getStackTrace()){
                trace += "\r\n\t"+i+". "+ste.getFileName()+", Class:  "+ste.getClassName()+", Method: "+ste.getMethodName()+"(), Line: "+ste.getLineNumber();
                i++;
            }
            logError.error("{}",trace);
            logError.error("{}","-------------------------------------------------------------------------------------------------------------------------");     
        }
    }
    public void sendCommon(String message){
        logCommon.debug("CrawlCommonMessage: {}", message);
    }
    public void sendAnalytic(ServiceInfo info, String message, String clazz, String method, Integer line){       
        String infoLine = message+"\r\n\t"+info.toString()+"\r\n\tClass: "+clazz+", Method: "+method+", Line: "+line;
        logAnalytics.info("CrawlInfo {}", infoLine);
         try {
            info.putMessage(message);
        } catch (InterruptedException ex) {
            
        }
    }   
}
