package crawler.utils;

import crawler.engine.JSONPEngine;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class LocationHelper {    
    private static @Getter @Setter String API_KEY = "AIzaSyBT9HFeqVUMt4exDI_mWvMMeeDUtCU7el4";
    private static final String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=false&key=%s";
    private final String callable;
    private JSONPEngine apiDoc;
    public LocationHelper(String lat, String lon) {
        this.callable = String.format(URL, lat,lon, API_KEY);       
    }
    public String getFormattedAddress(){
        try{
        apiDoc = new JSONPEngine(this.callable);  
        String status = apiDoc.exec("$..status");
        if(status.equals("OK")){
            List<String> list = apiDoc.execList("$..formatted_address");
            if(list.size()>2){
                apiDoc.close();
                return list.get(1);
            }
        }
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if(apiDoc != null){apiDoc.close();}
        }
        return "unknown";
    }    
}
