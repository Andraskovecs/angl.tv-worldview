package crawler.utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import lombok.Getter;
import lombok.Setter;
@JsonIgnoreProperties({"validFacebookData", "validTwitterData", "validCustomData", "infoName"})
public class ServiceInfo {
    public static final String STARTING = "Starting",RUNNING="Running",PAUSED="Paused";
    private static AtomicLong  idCounter = new AtomicLong();
    private @Getter @Setter String id, name;
    private @Getter @Setter Date startDate;
    private @Getter @Setter String type, mode;
    private @Getter @Setter String coordinates;
    private @Getter @Setter Integer newVideoByThis;
    private @Getter @Setter Integer updateVideoByThis;
    private @Getter @Setter Integer deletedVideoByThis;
    private @Getter @Setter String status;
    private @Getter @Setter Boolean isStopable;
    private @Getter @Setter String maxViewer, videoCount;
    
    private @Getter @Setter Double longitude, latitude, radius;
    private @Getter @Setter String east, west, south, north;
    private @Getter @Setter String providerName, listLink, detailsLink;
    
    private BlockingQueue<String> messageQueue = new ArrayBlockingQueue<>(30);

    public ServiceInfo(Date startDate, String name, String type, String mode, String coordinates) {
        this.id = ServiceInfo.createID();
        this.name = name;
        this.startDate = startDate;
        this.type = type;
        this.mode = mode;
        this.coordinates = coordinates;
        this.newVideoByThis = 0;
        this.deletedVideoByThis = 0;
        this.updateVideoByThis = 0;
        this.status = ServiceInfo.STARTING;
        this.isStopable = false;
    }

    public ServiceInfo(Date startDate,String name, String type, String mode, String providerName, String listLink, String detailsLink) {
        this.id = ServiceInfo.createID();
        this.name = name;
        this.startDate = startDate;
        this.type = type;
        this.mode = mode;
        this.providerName = providerName;
        this.listLink = listLink;
        this.detailsLink = detailsLink;
        this.newVideoByThis = 0;
        this.deletedVideoByThis = 0;
        this.updateVideoByThis = 0;
        this.status = ServiceInfo.STARTING;
        this.isStopable = false;
    }
    
    public ServiceInfo(Date startDate, String name, String type, String mode, Double longitude, Double latitude, Double radius, String videoCount) {
        this.id = ServiceInfo.createID();
        this.name = name;
        this.startDate = startDate;
        this.type = type;
        this.mode = mode;
        this.longitude = longitude;
        this.latitude = latitude;
        this.radius = radius;
        this.videoCount = videoCount;
        this.newVideoByThis = 0;
        this.deletedVideoByThis = 0;
        this.updateVideoByThis = 0;
        this.status = ServiceInfo.STARTING;
        this.isStopable = false;
    }    
    public ServiceInfo(Date startDate, String name, String type, String mode, String east, String west, String south, String north, String videoCount, String maxViewer) {
        this.id = ServiceInfo.createID();
        this.name = name;
        this.startDate = startDate;
        this.type = type;
        this.mode = mode;
        this.east = east;
        this.west = west;
        this.south = south;
        this.north = north;
        this.maxViewer = maxViewer;
        this.videoCount = videoCount;
        this.newVideoByThis = 0;
        this.deletedVideoByThis = 0;
        this.updateVideoByThis = 0;
        this.status = ServiceInfo.STARTING;
        this.isStopable = false;
    }

    public ServiceInfo() {        
    }
        
    synchronized public void increaseNewVideoCount(){
        this.newVideoByThis++;
    }
    synchronized public void increaseUpdateVideoCount(){
        this.updateVideoByThis++;
    }
    synchronized public void increaseDeletedVideoCount(){
        this.deletedVideoByThis++;
    }
    public static String createID(){
        return String.valueOf(idCounter.incrementAndGet());
    }
    
    public String getInfoName(){
        return this.id+"-"+type+"-"+mode;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.id);
        hash = 47 * hash + Objects.hashCode(this.startDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServiceInfo other = (ServiceInfo) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ServiceInfo - "+ id + ", "
                + "startDate: " + startDate + ", "
                + "type: " + type + ", "
                + "mode:" + mode + ", "
                + "coordinates: " + coordinates + ", "
                + "newVideoByThis: " + newVideoByThis + ", "
                + "updateVideoByThis: " + updateVideoByThis + ", "
                + "deletedVideoByThis: " + deletedVideoByThis + ", "
                + "status: " + status + ", "
                + "isStopable: " + isStopable;
    }

    public boolean isValidFacebookData() {
        if(this.east == null || this.east.equals(""))return false;
        if(this.west == null || this.west.equals(""))return false;
        if(this.north == null || this.north.equals(""))return false;
        if(this.south == null || this.south.equals(""))return false;
        if(this.maxViewer == null || this.maxViewer.equals(""))return false;
        if(this.videoCount == null || this.videoCount.equals(""))return false;
        return true;
    }
    public boolean isValidTwitterData(){
        if(this.latitude == null)return false;
        if(this.longitude == null)return false;
        if(this.radius == null)return false;
        if(this.videoCount == null || this.videoCount.equals(""))return false;
        return true;
    }
    public boolean isValidCustomData(){
        if(this.providerName == null || this.providerName.trim().isEmpty())return false;
        if(this.listLink == null || this.listLink.trim().isEmpty())return false;
        if(this.detailsLink == null || this.detailsLink.trim().isEmpty())return false;
        return true;
    }

    public void putMessage(String message) throws InterruptedException {
        this.messageQueue.put(message);
    }
    public String takeMessage() throws InterruptedException{
        if(this.messageQueue.size()>0){
        return this.messageQueue.take();
        }
        return "";
    }
}
