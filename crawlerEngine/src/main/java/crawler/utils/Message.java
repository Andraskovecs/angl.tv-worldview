package crawler.utils;

import com.google.gson.JsonObject;

public class Message {

	private String type;
	private String url;
        private JsonObject data;

	public Message(String type, String url, JsonObject data) {
		this.type = type;
		this.url = url;
                this.data = data;
	}

	public String getType() {
		return type;
	}

	public String getUrl() {
		return url;
	}

        public JsonObject getData() {
            return data;
        }
        
	
	@Override
	public String toString() {
		return String.format("Message[ type=%s, url=%s, data=%s]", type, url, data.toString());
	}

}
