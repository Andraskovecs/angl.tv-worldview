package crawler.utils;

import com.google.gson.JsonObject;
import java.util.Objects;

public class VideoCrawlerData {
    private String url;
    private JsonObject data;

    public VideoCrawlerData(String url, JsonObject data) {
        this.url = url;
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public JsonObject getData() {
        return data;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.url);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VideoCrawlerData other = (VideoCrawlerData) obj;
        if (!Objects.equals(this.url, other.url)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "VideoCrawlerData{" + "url=" + url + ", data=" + data.toString() + '}';
    }

    
    
}
