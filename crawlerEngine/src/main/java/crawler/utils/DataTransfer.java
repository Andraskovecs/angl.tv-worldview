package crawler.utils;

import lombok.Getter;
import lombok.Setter;

public class DataTransfer {
    public static final String UPDATE = "upsert", INSERT = "upsert", DELETE = "delete";
    private @Getter @Setter String method, videoid, defaultstreamurl, defaultthumbnailurl;

    public DataTransfer() {
    }

    public DataTransfer(String method, String videoid, String defaultstreamurl, String defaultthumbnailurl) {
        this.method = method;
        this.videoid = videoid;
        this.defaultstreamurl = defaultstreamurl;
        this.defaultthumbnailurl = defaultthumbnailurl;
    }
}
