package crawler;

import lombok.Getter;

public class CrawlerProviderException extends Exception{
    public static final String TYPEVIDEO = "video", TYPEPROVIDER = "provider";
    private @Getter String type;

    public CrawlerProviderException(String type, String message) {
        super(message);
        this.type = type;
    }

    public CrawlerProviderException(String message) {
        super(message);
        this.type = CrawlerProviderException.TYPEPROVIDER;
    }
    
}
