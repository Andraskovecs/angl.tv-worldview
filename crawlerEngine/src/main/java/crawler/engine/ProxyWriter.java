
package crawler.engine;

import com.google.gson.Gson;
import crawler.CrawlLogger;
import crawler.utils.DataTransfer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class ProxyWriter {
    //private static final String URL = "http://152.66.254.131:8899/users/test/";
    private static final String URL = "http://104.237.4.64:8091/";
    private final Gson gson = new Gson();
    
    private static class SingletonHelper {private static final ProxyWriter INSTANCE = new ProxyWriter();}
    public static ProxyWriter getInstance() {return SingletonHelper.INSTANCE;}
    
    public void pushDataToServer(DataTransfer dt) {
        HttpURLConnection http = null;
        try{
            http = (HttpURLConnection) new URL(URL).openConnection();
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            String json = gson.toJson(dt);
            byte[] out = json.getBytes(StandardCharsets.UTF_8);
            int length = out.length;
            http.setFixedLengthStreamingMode(length);
            http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            http.connect();
            try(OutputStream os = http.getOutputStream()) {
                os.write(out);
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                CrawlLogger.getInstance().sendCommon(inputLine);
            }
            in.close();
        } catch (MalformedURLException ex) {
            CrawlLogger.getInstance().sendError(ex);
        } catch (IOException ex) {
            CrawlLogger.getInstance().sendError(ex);
        }finally{
            if(http != null){http.disconnect();}
        }      
    }
}
