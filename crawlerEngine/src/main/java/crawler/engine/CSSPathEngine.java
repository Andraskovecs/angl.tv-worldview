package crawler.engine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class CSSPathEngine extends AbstractHttpSyncDownloaderEngine {

	protected Document doc;

	public CSSPathEngine(String URL) {
		super(URL, false);
	}

	@Override
	public String exec(String... expression) {
		String result = "";
		if (expression.length == 2) {
			if (doc == null) {
				doc = Jsoup.parse(this.content);
			}
			Element videoURLEl = doc.select(expression[0]).first();                        
			result = videoURLEl.attr(expression[1]);
                        if(result.equals("")) result = videoURLEl.html();
		}
		return result;
	}
}
