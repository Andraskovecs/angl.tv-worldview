package crawler.engine;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class HttpSessionEngine {

	protected String userAgent;

	protected CloseableHttpClient httpclient;
	protected HttpClientContext localContext;

	protected String content;

	private HttpSessionEngine(HttpSessionEngineBuilder builder) {
		this.userAgent = builder.userAgent;

		init();
	}

	public JsonObject getJson() {
		JsonParser parser = new JsonParser();

		String formattedContent = null;
		if (content.startsWith("[")) {
			formattedContent = String.format("{ content: %s }", content);
		} else {
			formattedContent = content;
		}

		JsonObject json = parser.parse(formattedContent).getAsJsonObject();
		return json;
	}
	
	public String getContent() {
		return content;
	}

	private void init() {
		httpclient =
			HttpClients
				.custom()
				.setUserAgent(userAgent)
				.setDefaultRequestConfig(RequestConfig.custom().setContentCompressionEnabled(false).setCookieSpec(CookieSpecs.STANDARD).build())
				.build();

		localContext = HttpClientContext.create();
	}

	public HttpResponse call(HttpUriRequest request) {

		HttpResponse response = null;

		try {
			
			response = httpclient.execute(request, localContext);

			StringWriter writer = new StringWriter();
			try {
				IOUtils.copy(response.getEntity().getContent(), writer, StandardCharsets.UTF_8);
			} catch (UnsupportedOperationException | IOException e) {
				e.printStackTrace();
			}
			
			content = writer.toString();
			
			//httpclient.close();
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
		}

		return response;
	}
        public void closeClient() throws IOException{
            if(httpclient != null)httpclient.close();
        }

	public List<Cookie> getCookies() {
		List<Cookie> cookies = localContext.getCookieStore().getCookies();
		return cookies;
	}

	public Cookie getCookie(String name) {
		Cookie match = null;
		List<Cookie> cookies = getCookies();
		for (Cookie cookie : cookies) {
			if (name.equals(cookie.getName())) {
				match = cookie;
			}
		}
		return match;
	}

	public static class HttpSessionEngineBuilder {

		protected static final String DEFAULTUSERAGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36";
		protected String userAgent;

		public HttpSessionEngineBuilder() {
			userAgent = DEFAULTUSERAGENT;
		}

		public HttpSessionEngineBuilder setUserAgent(String userAgent) {
			this.userAgent = userAgent;
			return this;
		}

		public HttpSessionEngine build() {
			return new HttpSessionEngine(this);
		}
	}
}
