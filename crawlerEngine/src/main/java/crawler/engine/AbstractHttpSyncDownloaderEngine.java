package crawler.engine;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;

public abstract class AbstractHttpSyncDownloaderEngine {

	protected String URL;
	protected String content;

	protected List<BasicClientCookie> inputCookies = new ArrayList<BasicClientCookie>();
	protected Map<String, String> headers;

	protected Map<String, BasicClientCookie> outputCookies;
	protected Map<String, String> outputHeaders;

	protected String defaultUserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36";

	protected CookieStore cookieStore;
	protected AbstractHttpEntity entity;
	protected CloseableHttpAsyncClient httpclient;
	protected HttpClientContext localContext;
	
	public AbstractHttpSyncDownloaderEngine(String urlOrContent, AbstractHttpEntity entity) {
		this(urlOrContent, false, entity);
	}

	public AbstractHttpSyncDownloaderEngine(String urlOrContent) {
		this(urlOrContent, false, null);
	}
	
	public AbstractHttpSyncDownloaderEngine(String urlOrContent, Boolean isContent) {
		this(urlOrContent, isContent, null);
	}

	public AbstractHttpSyncDownloaderEngine(String urlOrContent, Boolean isContent, AbstractHttpEntity entity) {
		
		this.entity = entity;
		
		httpclient =
			HttpAsyncClients
				.custom()
				.setUserAgent(defaultUserAgent)
				.setDefaultRequestConfig(
						RequestConfig.custom()
						.setContentCompressionEnabled(false)
						.setCookieSpec(CookieSpecs.STANDARD).build()).build();

		localContext = HttpClientContext.create();
		cookieStore = localContext.getCookieStore();
		
		httpclient.start();
		
		if (!isContent) {
			this.URL = urlOrContent;
			this.load();
		} else {
			content = urlOrContent;
		}
	}
	
	public static String extractHttpEntityContent(HttpResponse response) {
		String content = null;
		
		StringWriter writer = new StringWriter();
		try {
			IOUtils.copy(response.getEntity().getContent(), writer, StandardCharsets.UTF_8);
			content = writer.toString();
		} catch (UnsupportedOperationException | IOException e) {
			e.printStackTrace();
		}
		
		return content;
	}
	
	public void reinit() {
		
		if (httpclient.isRunning()) {
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		httpclient =
			HttpAsyncClients
				.custom()
				.setUserAgent(defaultUserAgent)
				.setDefaultRequestConfig(
						RequestConfig.custom()
						.setContentCompressionEnabled(false)
						.setCookieSpec(CookieSpecs.STANDARD).build()).build();

		localContext = HttpClientContext.create();
		cookieStore = localContext.getCookieStore();
		
		httpclient.start();
	}
	
	public void load() {
		load(this.URL, "GET", null);
	}
	
	public void get(String url) {
		load(url, "GET", null);
	}
	
	public void post(String url, AbstractHttpEntity entity) {
		load(url, "POST", entity);
	}
	
	public void load(String url, String mode, AbstractHttpEntity entity) {
		
		try {
			
			if (!httpclient.isRunning()) {
				httpclient.start();
			}
			
			// default GET
			HttpRequestBase request = new HttpGet(url);

			if (mode == "POST") {
				request = new HttpPost(url);
				if (entity != null) {
					((HttpPost) request).setEntity(entity);
				}
			}
			
			Future<HttpResponse> future = httpclient.execute(request, localContext, null);
			HttpResponse response = future.get();
			
			this.content = extractHttpEntityContent(response);
			
			httpclient.close();

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
	}
	
	public void close() {
		if (httpclient != null && httpclient.isRunning()) {
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<Cookie> getCookies() {
		CookieStore cookieStore = localContext.getCookieStore();
		// get Cookies
		List<Cookie> cookies = cookieStore.getCookies();
		return cookies;
	}

	public String getContent() {
		return content;
	}

	public abstract String exec(String... expression);
	
	public List<BasicClientCookie> getInputCookies() {
		return inputCookies;
	}

	public void setInputCookies(List<BasicClientCookie> inputCookies) {
		this.inputCookies = inputCookies;
	}
	
	public String getDefaultUserAgent() {
		return defaultUserAgent;
	}

	public void setDefaultUserAgent(String defaultUserAgent) {
		this.defaultUserAgent = defaultUserAgent;
	}

	public Map<String, String> getOutputHeaders() {
		return outputHeaders;
	}

	public void setOutputHeaders(Map<String, String> outputHeaders) {
		this.outputHeaders = outputHeaders;
	}

}
