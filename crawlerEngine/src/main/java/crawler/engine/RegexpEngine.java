package crawler.engine;

public class RegexpEngine extends AbstractHttpSyncDownloaderEngine {

	public RegexpEngine(String urlOrContent) {
		super(urlOrContent, false);
	}

	public RegexpEngine(String urlOrContent, boolean isContent) {
		super(urlOrContent, isContent);
	}
	
	@Override
	public String exec(String... expression) {
		String result = "";

		jregex.Pattern p = new jregex.Pattern(expression[0]);
		jregex.Matcher m = p.matcher(content);

		if (m.find() && m.groupCount() == 2) {
			result = m.group(1);
		}

		return result;
	}
}
