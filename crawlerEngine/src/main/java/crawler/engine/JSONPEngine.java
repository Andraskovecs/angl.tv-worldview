package crawler.engine;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.entity.AbstractHttpEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

public class JSONPEngine extends AbstractHttpSyncDownloaderEngine {

    private static final Logger logger = LogManager.getLogger();

    public JSONPEngine(String URL) {
        this(URL, false);
    }

    public JSONPEngine(String URL, Boolean isContent) {
        super(URL, isContent);
    }

    public JSONPEngine(String URL, AbstractHttpEntity entity) {
        super(URL, entity);
    }

    // REVIEW zsolturo varargs and multi list return
    @Override
    public String exec(String... expression) {

        String result = null;

        if (content == null || content.length() == 0) {
            return result;
        }

        try {
            if (expression.length == 1) {
                result = JsonPath.parse(content).read(expression[0], String.class);
                if (result == null) {
                    @SuppressWarnings("unchecked")
                    List<String> listResult = JsonPath.parse(content).read(expression[0], List.class);
                    if (listResult.size() > 0) {
                        result = listResult.get(0);
                    }
                }
            }

        } catch (PathNotFoundException e) {
            //logger.info("path not found: {}", expression[0]);
        }

        return result;
    }

    public Double execDouble(String... expression) {
        Double result = null;

        if (content == null || content.length() == 0) {
            return result;
        }

        try {
            if (expression.length == 1) {
                result = JsonPath.parse(content).read(expression[0], Double.class);
                if (result == null) {
                    @SuppressWarnings("unchecked")
                    List<Double> listResult = JsonPath.parse(content).read(expression[0], List.class);
                    if (listResult.size() > 0) {
                        result = listResult.get(0);
                    }
                }
            }

        } catch (PathNotFoundException e) {
            logger.info("path not found: {}", expression[0]);
        }

        return result;
    }
    public Boolean execBoolean(String... expression) {
        Boolean result = null;

        if (content == null || content.length() == 0) {
            return result;
        }

        try {
            if (expression.length == 1) {
                result = JsonPath.parse(content).read(expression[0], Boolean.class);
                if (result == null) {
                    @SuppressWarnings("unchecked")
                    List<Boolean> listResult = JsonPath.parse(content).read(expression[0], List.class);
                    if (listResult.size() > 0) {
                        result = listResult.get(0);
                    }
                }
            }

        } catch (PathNotFoundException e) {
            logger.info("path not found: {}", expression[0]);
        }

        return result;
    }

    public Integer execInt(String... expression) {
        Integer result = null;

        if (content == null || content.length() == 0) {
            return result;
        }

        try {
            if (expression.length == 1) {
                result = JsonPath.parse(content).read(expression[0], Integer.class);
                if (result == null) {
                    @SuppressWarnings("unchecked")
                    List<Integer> listResult = JsonPath.parse(content).read(expression[0], List.class);
                    if (listResult.size() > 0) {
                        result = listResult.get(0);
                    }
                }
            }

        } catch (PathNotFoundException e) {
            logger.info("path not found: {}", expression[0]);
        }

        return result;
    }
    public Long execLong(String... expression) {
        Long result = null;

        if (content == null || content.length() == 0) {
            return result;
        }

        try {
            if (expression.length == 1) {
                result = JsonPath.parse(content).read(expression[0], Long.class);
                if (result == null) {
                    @SuppressWarnings("unchecked")
                    List<Long> listResult = JsonPath.parse(content).read(expression[0], List.class);
                    if (listResult.size() > 0) {
                        result = listResult.get(0);
                    }
                }
            }

        } catch (PathNotFoundException e) {
            logger.info("path not found: {}", expression[0]);
        }

        return result;
    }

    public List<String> execList(String expression) {
        List<?> listResult = JsonPath.parse(content).read(expression, List.class);

        List<String> stringResult = new ArrayList<String>();
        for (Object o : listResult) {
            stringResult.add(String.valueOf(o));
        }

        return stringResult;
    }
}
