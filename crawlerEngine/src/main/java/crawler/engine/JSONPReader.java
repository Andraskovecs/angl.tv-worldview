package crawler.engine;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import crawler.CrawlLogger;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class JSONPReader {
    private @Getter String jsoncContent;

    public JSONPReader(String jsoncContent) {
        this.jsoncContent = jsoncContent;
    }
    
    public <T> T exec(Class<T> clazz, String expression){
        T result = null;
        if (this.jsoncContent == null || this.jsoncContent.trim().isEmpty()) {return result;}
        try {
            if (expression != null && !expression.trim().isEmpty()) {
                result = JsonPath.parse(this.jsoncContent).read(expression, clazz);
                if (result == null) {
                    List<T> listResult = JsonPath.parse(this.jsoncContent).read(expression, List.class);
                    if (listResult.size() > 0) {result = listResult.get(0);}
                }
            }

        } catch (PathNotFoundException e) {
            CrawlLogger.getInstance().sendError(e);
            CrawlLogger.getInstance().sendCommon("path not found: "+expression);
        }
        return result;
    }
    public List<String> execList(String expression) {
        List<String> stringResult = new ArrayList<>();        
        try{            
            List<?> listResult = JsonPath.parse(this.jsoncContent).read(expression, List.class);
            listResult.forEach((o) -> {
                stringResult.add(String.valueOf(o));
            });
        }catch(Exception e){
            CrawlLogger.getInstance().sendError(e);
            CrawlLogger.getInstance().sendCommon("path not found: "+expression);
        }
        return stringResult;
    }
                
}
