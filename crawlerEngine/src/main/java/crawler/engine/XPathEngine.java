package crawler.engine;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XPathEngine extends AbstractHttpSyncDownloaderEngine {

	protected HashMap<String, String> NameSpaces;

	public XPathEngine(String URL) {
		this(URL, false);
	}

	public XPathEngine(String URL, Boolean isContent) {
		super(URL, isContent);
	}

	public XPathEngine(String URL, Boolean isContent, HashMap<String, String> NS) {
		super(URL, isContent);
	}

	private XPath xPath;
	private Document domDocument;

	protected void process() {

		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder domParser = null;
		try {
			domParser = builderFactory.newDocumentBuilder();
			InputStream stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8.name()));
			domDocument = domParser.parse(stream);
			xPath = XPathFactory.newInstance().newXPath();
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String exec(String... expression) {
		String result = "";
		if (expression.length == 1) {
			if (xPath == null) {
				process();
			}
			String expr = expression[0];
			try {
				Object o = xPath.compile(expr).evaluate(domDocument, XPathConstants.STRING);
				result = (String) o;
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public List<HashMap<String, String>> execList(String... expression) {
		List<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		if (xPath == null) {
			process();
		}
		try {
			Object o = xPath.compile(expression[0]).evaluate(domDocument, XPathConstants.NODESET);

			NodeList nodes = (NodeList) o;
			for (int i = 0, n = nodes.getLength(); i < n; i++) {
				Node node = nodes.item(i);
				HashMap<String, String> nodeMap = new HashMap<String, String>();
				for (int idx = 1; idx < expression.length; idx++) {
					nodeMap.put(expression[idx], node.getAttributes().getNamedItem(expression[idx]).getTextContent());
				}
				result.add(nodeMap);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return result;
	}
}
