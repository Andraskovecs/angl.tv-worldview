package crawler.provider;

import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videodetails;
import com.google.gson.JsonObject;
import crawler.CrawlerProviderException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;

public abstract class Provider implements IProvider{

    protected @Getter String username;
    protected @Getter String title;
    protected @Getter String defaultThumbnailURL;
    protected @Getter String defaultStreamURL;
    protected @Getter Integer watched = 0;    
    protected @Getter String videoId;
    protected @Getter Boolean isLive = true;    
    protected @Getter Double longitude = 0.0;
    protected @Getter Double latitude = 0.0;
    protected @Getter Date publishDate = new Date();    
    protected @Getter Videodetails details;
    
    protected String URL;
    protected String videoID;
    protected static String DEFAULT_REGEXP_EXTRACTOR = ".*";
    protected static String videoIdExtractorPattern;
    protected JsonObject data;

    public Provider(String URL, String regexp) {
        this.URL = URL;
        videoIdExtractorPattern = regexp;
    }
    /**
     * Facebook
     * @param URL
     * @param regexp
     * @param data 
     */
    public Provider(String URL, String regexp, JsonObject data) {
        this.data = data;
        this.URL = URL;
        videoIdExtractorPattern = regexp;
    }

    public Provider(String URL) {
        this(URL, DEFAULT_REGEXP_EXTRACTOR);
    }

    @Override
    public abstract void process() throws CrawlerProviderException;
    @Override
    public abstract Video saveVideo(Video video) throws CrawlerProviderException;   
    @Override
    public abstract void updateProperties() throws CrawlerProviderException;

    protected String declareVideoID() {
        if (videoID == null) {videoID = regexpVideoIDExtractor(videoIdExtractorPattern);}
        return videoID;
    }

    protected String regexpVideoIDExtractor(String regexp) {
        String result = "";
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(URL);
        if (matcher.find()) {result = matcher.group(1);}
        return result;
    }
    
    public String isValidResult(){
        if(this.getVideoId() == null || this.getVideoId().trim().isEmpty())return "VideoId is null or empty!";
        if(this.getUsername()== null || this.getUsername().trim().isEmpty())return "Username is null or empty!";
        if(this.getDefaultStreamURL() == null || this.getDefaultStreamURL().trim().isEmpty())return "DefaultStreamURL is null or empty!";   
        if(this.getDefaultThumbnailURL()== null || this.getDefaultThumbnailURL().trim().isEmpty())return "Thumbnail is null or empty!";           
        if(this.getTitle() == null || this.getTitle().trim().isEmpty())this.title = username+" "+videoId;
        return "OK";
    }
    
    public static String decompose(String s) {
	return java.text.Normalizer.normalize(s, java.text.Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+","");
    }

    @Override
    public String toString() {
        return "Provider{" + "URL=" + URL + ", username=" + username + ", title=" + title + ", defaultThumbnailURL=" + defaultThumbnailURL + ", defaultStreamURL=" + defaultStreamURL + ", watched=" + watched + ", videoId=" + videoId + ", isLive=" + isLive + ", longitude=" + longitude + ", latitude=" + latitude + ", publishDate=" + publishDate + '}';
    }
    
}
