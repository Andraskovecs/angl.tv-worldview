package crawler.provider;

import com.google.gson.JsonObject;
import crawler.provider.implementations.Custom;
import crawler.provider.implementations.Facebook;
import crawler.provider.implementations.Instagram;
import crawler.provider.implementations.Periscope;
import crawler.provider.implementations.Twitter;
import crawler.utils.ServiceInfo;

public class ProviderFactory {

    public Provider getProvider(String providerType, String URL, JsonObject data, ServiceInfo info) {
        if (providerType == null) {return null;}
        if (providerType.equalsIgnoreCase("twitter")) {return new Twitter(URL);}
        else if (providerType.equalsIgnoreCase("facebook")) {return new Facebook(URL, data);}
        else if (providerType.equalsIgnoreCase("periscope")) {return new Periscope(URL);}
        else if (providerType.equalsIgnoreCase("instagram")) {return new Instagram(URL);}        
        else if (providerType.equalsIgnoreCase("custom")) {return new Custom(URL, info.getDetailsLink());}
        return null;
    }
}
