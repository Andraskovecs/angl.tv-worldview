package crawler.provider;

import com.angl.angldao.models.Video;
import crawler.CrawlerProviderException;

public interface IProvider {
    public void process() throws CrawlerProviderException;
    public Video saveVideo(Video video) throws CrawlerProviderException;
    void updateProperties()throws CrawlerProviderException;
}
