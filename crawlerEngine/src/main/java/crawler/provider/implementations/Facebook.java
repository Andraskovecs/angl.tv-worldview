package crawler.provider.implementations;

import com.angl.angldao.enums.Orientationtype;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videodetails;
import com.google.gson.JsonObject;

import crawler.engine.JSONPReader;
import crawler.engine.RegexpEngine;
import crawler.provider.Provider;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.CrawlerProviderException;
import crawler.utils.LocationHelper;
import java.util.Date;

public class Facebook extends Provider {
    private static final String REGEXP_FOR_VIDEOID_EXTRACTOR = ".*\\/videos\\/(\\d+)\\/?";
    protected JSONPReader streamDoc;
    
    public Facebook(String URL, JsonObject data) {
        super(URL, REGEXP_FOR_VIDEOID_EXTRACTOR, data);
    }

    @Override
    public void process() throws CrawlerProviderException {
        if(this.URL == null || this.URL.trim().isEmpty()){throw new CrawlerProviderException("Facebook provider got null or empty to URL: "+this.URL);}
        RegexpEngine htmlDoc = null;
        try{
            htmlDoc = new RegexpEngine(this.URL);
            if (!htmlDoc.getContent().trim().isEmpty()) {
                String streamData = htmlDoc.exec("bigPipe\\.onPageletArrive\\((\\{.+?\\})\\)\\s*;\\s*}\\s*\\)\\s*,\\s*[\"\\']onPageletArrive\\s+(?:pagelet_group_mall|permalink_video_pagelet)");
                streamDoc = new JSONPReader(streamData);
            }
        }catch(Exception ex){
            CrawlLogger.getInstance().sendError(ex);
            throw new CrawlerProviderException("We catched an error its message: "+ex.getMessage());
        }
        finally{
            if(htmlDoc != null){ htmlDoc.close();}
        }
    }
    private int rotation = 0, width = 0, height = 0;
    private String category = "";
    
    @Override
    public void updateProperties() throws CrawlerProviderException{
        this.process();
        if(streamDoc == null){throw new CrawlerProviderException("StreamDoc of facebook provider is null");}
        if(streamDoc.getJsoncContent().trim().isEmpty()){throw new CrawlerProviderException("StreamDoc of facebook provider is empty");}
        if(this.data == null){throw new CrawlerProviderException("Other data of facebook provider is null");}
        JSONPReader dataDoc = null;
        try {            
            dataDoc = new JSONPReader(data.toString());
            defaultStreamURL = streamDoc.<String>exec(String.class,"$..instances..videoData..sd_src");
            isLive = streamDoc.<Boolean>exec(Boolean.class, "$..instances..videoData..is_live_stream");            
            latitude = (data.has("location")? dataDoc.<Double>exec(Double.class, "$..location..latitude"): 0.0);
            longitude = (data.has("location")? dataDoc.<Double>exec(Double.class,"$..location..longitude"): 0.0);
            watched = dataDoc.<Integer>exec(Integer.class,"$..data..video..viewer_count");
            defaultThumbnailURL = dataDoc.<String>exec(String.class, "$..data..video..image..uri");
            videoId = dataDoc.<String>exec(String.class,"$..data..video..id");
            username = dataDoc.<String>exec(String.class,"$..data..video..owner..name");
            title = dataDoc.<String>exec(String.class,"$..data..video..message..text");
            publishDate = new Date();
            try{
                String ownerType = dataDoc.<String>exec(String.class,"$..data..video..owner..__typename");            
                category = (ownerType.equals("User")?"user live":dataDoc.<String>exec(String.class,"$..data..video..publisher_category"));
                rotation = streamDoc.<Integer>exec(Integer.class,"$..instances..videoData..rotation");
                height = dataDoc.<Integer>exec(Integer.class,"$..data..video..height");
                width = dataDoc.<Integer>exec(Integer.class,"$..data..video..width");                
            }catch(Exception ex){
                System.out.println("Facebook not required data error: "+ex.getMessage());     
            }
        } catch (Exception e) {            
            CrawlLogger.getInstance().sendError(e);
            throw new CrawlerProviderException(e.getMessage());
        }
    }

    @Override
    public Video saveVideo(Video video) throws CrawlerProviderException {
        String result = this.isValidResult();
        if(!result.equals("OK")){throw new CrawlerProviderException(CrawlerProviderException.TYPEVIDEO,"Does not have valid rasult: "+result);}
        try{
            video.setDefaultstreamurl(defaultStreamURL);
            video.setDefaultthumbnailurl(defaultThumbnailURL);
            video.setLive(isLive);
            if(video.getLongitude() == null || video.getLongitude()<1){video.setLongitude(longitude);}
            if(video.getLatitude()== null || video.getLatitude()<1){video.setLatitude(latitude);}        
            if(video.getPublishdate() == null){video.setPublishdate(publishDate);}
            video.setOriginalurl(URL);
            if(video.getProvider() == null){video.setProvidertype(CrawlService.TYPEFB);}
            video.setTitle(title);
            video.setUsername(username);
            video.setVideoid(videoId);
            video.setWatched(watched);
            if((video.getLocation() == null || video.getLocation().trim().isEmpty()) && (video.getLongitude()!=0 && video.getLatitude()!=0)){
                LocationHelper l = new LocationHelper(video.getLatitude().toString(), video.getLongitude().toString());
                video.setLocation(l.getFormattedAddress());
            }
            details = new Videodetails();
            details.setVideoheight((short)height);
            details.setVideowidth((short)width);
            details.setVideocategory(category);
            details.setOrientation((rotation == 0?Orientationtype.landscape:Orientationtype.portrait));
        }catch(Exception e){
            CrawlLogger.getInstance().sendError(e);
            throw new CrawlerProviderException("Catched exception in saveVideo(facebook): "+e.getMessage()+" class: "+e.getClass().getTypeName());
        }
        return video;
    }
}
