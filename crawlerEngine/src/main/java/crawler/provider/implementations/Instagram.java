package crawler.provider.implementations;

import com.angl.angldao.models.Video;
import crawler.engine.JSONPReader;
import crawler.engine.RegexpEngine;
import crawler.provider.Provider;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.CrawlerProviderException;
import crawler.spider.InstagramVideoCrawler;
import java.util.Date;
import java.util.List;

public class Instagram extends Provider {

    protected static String REGEXP_FOR_VIDEOID_EXTRACTOR = "^\\d+$";
    private static final String HTML_EXEC_PATTERN = "window\\._sharedData\\s*=\\s*(\\{.+?\\});";

    public Instagram(String URL) {
        super(URL);
    }
    protected JSONPReader streamDoc;
    protected JSONPReader liveStreamDoc;
    protected JSONPReader commentStreamDoc;
    protected Boolean isLiveVideo = false;

    @Override
    public void process() throws CrawlerProviderException {
        if(this.URL == null || this.URL.trim().isEmpty()){throw new CrawlerProviderException("Instagram provider got null or empty to URL: "+this.URL);}
        RegexpEngine htmlDoc = null;
        try {
            isLiveVideo = this.URL != null && this.URL.matches(REGEXP_FOR_VIDEOID_EXTRACTOR);
            if (isLiveVideo) {
                String result = InstagramVideoCrawler.getInstance().getVideoDataFromId(this.URL);
                if(!result.trim().isEmpty()){
                    liveStreamDoc = new JSONPReader(result);
                }
                String commentResult = InstagramVideoCrawler.getInstance().getVideoCommentFromId(this.URL);
                if(!commentResult.trim().isEmpty()){
                    commentStreamDoc = new JSONPReader(commentResult);
                }
            } else {
                htmlDoc = new RegexpEngine(this.URL);
                if (!htmlDoc.getContent().trim().isEmpty()) {
                    String streamData = htmlDoc.exec(HTML_EXEC_PATTERN);
                    if(!streamData.trim().isEmpty()){
                        streamDoc = new JSONPReader(streamData);
                    }
                }
            }
        } catch (Exception e) {            
            CrawlLogger.getInstance().sendError(e);
            throw new CrawlerProviderException("We catched an error its message: "+e.getMessage());
        } finally {
            if (htmlDoc != null) {htmlDoc.close();}
        }
    }
    @Override
    public void updateProperties() throws CrawlerProviderException {
        this.process();
        if(isLiveVideo){            
            if(liveStreamDoc == null){throw new CrawlerProviderException("LiveStreamDoc of instagram provider is null");}
            if(liveStreamDoc.getJsoncContent().trim().isEmpty()){throw new CrawlerProviderException("LiveStreamDoc of instagram provider is empty");}
            if(commentStreamDoc == null){throw new CrawlerProviderException("commentStreamDoc of instagram provider is null");}
            if(commentStreamDoc.getJsoncContent().trim().isEmpty()){throw new CrawlerProviderException("commentStreamDoc of instagram provider is empty");}
            try {                
                String status = liveStreamDoc.<String>exec(String.class,"$..status");
                String broadcastStatus = liveStreamDoc.<String>exec(String.class,"$..broadcast_status");
                if (status.equals("ok") && broadcastStatus.equals("post_live")) {
                    String dashManifest = liveStreamDoc.<String>exec(String.class,"$..dash_manifest");
                    CrawlLogger.getInstance().sendCommon("INSTAGRAM Nem �l� vide� adatok: "+dashManifest);//TODO EZT M�G ki kell fejteni!
                    throw new CrawlerProviderException("DASHMANIFEST -> not live video data was get");
                }else if(status.equals("ok") && !broadcastStatus.equals("post_live")){
                    username = liveStreamDoc.<String>exec(String.class,"$.broadcast_owner.full_name") + " @" + liveStreamDoc.<String>exec(String.class,"$.broadcast_owner.username");
                    defaultStreamURL = decompose(liveStreamDoc.<String>exec(String.class,"$..dash_abr_playback_url"));
                    videoId = liveStreamDoc.<Long>exec(Long.class,"$..id").toString();
                    title = liveStreamDoc.<String>exec(String.class,"$.broadcast_message");
                    title = (title.isEmpty()?"":title);
                    watched = (liveStreamDoc.<Double>exec(Double.class,"$..viewer_count")).intValue();
                    latitude = 0.0;
                    longitude = 0.0;
                    defaultThumbnailURL = liveStreamDoc.<String>exec(String.class,"$..cover_frame_url");
                    isLive = isLiveVideo;
                    long timestamp = liveStreamDoc.<Integer>exec(Integer.class,"$..published_time");
                    publishDate = new Date(timestamp*1000);
                    try{
                        String commentStatus = commentStreamDoc.<String>exec(String.class,"$.status");
                        if(commentStatus.equals("ok")){
                            int commentMuted = commentStreamDoc.<Integer>exec(Integer.class,"$.comment_muted");
                            if(commentMuted == 0){
                                int commentCount = commentStreamDoc.<Integer>exec(Integer.class,"$.comment_count");
                                if(commentCount>0){
                                    List<String> commentList = commentStreamDoc.execList("$.comments..text");
                                    commentList.forEach((s) -> {
                                        title += "\r\n"+s;
                                    });
                                }
                            }
                        }
                    }catch(Exception ex){
                        System.out.println("Instagram not required data error: "+ex.getMessage());                    
                    }
                }
            } catch (Exception e) {
                CrawlLogger.getInstance().sendError(e);
                throw new CrawlerProviderException(e.getMessage());
            }
        }else{
            if(streamDoc == null){throw new CrawlerProviderException("streamDoc of instagram provider is null");}
            CrawlLogger.getInstance().sendCommon("INSTAGRAM Nem �l� vide� adatok: "+streamDoc.getJsoncContent());//TODO EZT M�G ki kell fejteni!
            throw new CrawlerProviderException("not live video data was get with non OK satatus");
        }              
    }
    @Override
    public Video saveVideo(Video video) throws CrawlerProviderException {
        this.updateProperties();
        String result = this.isValidResult();
        if(!result.equals("OK")){throw new CrawlerProviderException(CrawlerProviderException.TYPEVIDEO,"Does not have valid rasult: "+result);}
        video.setDefaultstreamurl(defaultStreamURL);
        video.setDefaultthumbnailurl(defaultThumbnailURL);
        video.setLatitude(latitude);
        video.setLive(isLive);
        video.setLongitude(longitude);
        video.setOriginalurl(URL);
        video.setProvidertype(CrawlService.TYPEINSTA);
        video.setPublishdate(publishDate);
        video.setTitle((video.getTitle() != null?video.getTitle()+"\r\n"+title:(title.trim().isEmpty()?"Instagram LIVE":title)));
        video.setUsername(username);
        video.setVideoid(videoId);
        video.setWatched(watched);
        return video;
    }    
}
