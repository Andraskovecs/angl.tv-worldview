package crawler.provider.implementations;

import com.angl.angldao.models.Interest;
import com.angl.angldao.enums.Orientationtype;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videodetails;
import crawler.engine.JSONPEngine;
import crawler.provider.Provider;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.CrawlerProviderException;
import crawler.utils.LocationHelper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Periscope extends Provider {

	private static final Logger logger = LogManager.getLogger();
        private @Getter @Setter static long plusHours = 2;
	private JSONPEngine broadcastJsonp;
	private JSONPEngine accessJsonp;

	private static String URL_BROADCAST_PATTERN = "https://proxsee.pscp.tv/api/v2/getBroadcastPublic?broadcast_id=%s";
	private static String URL_ACCESS_PATTERN = "https://proxsee.pscp.tv/api/v2/accessVideoPublic?broadcast_id=%s&replay_redirect=false";

	protected static String REGEXP_FOR_VIDEOID_EXTRACTOR = ".*w/(.*)";

	public Periscope(String URL) {
		super(URL, REGEXP_FOR_VIDEOID_EXTRACTOR);
	}
	
	@Override
	public void process() throws CrawlerProviderException {
            if(this.URL == null || this.URL.trim().isEmpty() || this.URL.length()>14){throw new CrawlerProviderException("Facebook provider got null or empty to URL: "+this.URL);}
            try{
                videoId = this.URL;
                this.URL = String.format("https://www.pscp.tv/w/%s", this.URL);
                String urlAccess = String.format(URL_ACCESS_PATTERN, videoId);
                accessJsonp = new JSONPEngine(urlAccess);
                String urlBroadcast = String.format(URL_BROADCAST_PATTERN, videoId);
                broadcastJsonp = new JSONPEngine(urlBroadcast);                  
            }catch(Exception ex){
                CrawlLogger.getInstance().sendError(ex);
                throw new CrawlerProviderException("We catched an error its message: "+ex.getMessage());
            }
            finally{
                if(broadcastJsonp != null){ broadcastJsonp.close();}
                if(accessJsonp != null){ accessJsonp.close();}
            }
	}
        private String location = null;
        private int rotation = 0, width = 0, height = 0;
        private List<String> tags = new ArrayList<>();        
        private @Getter List<Interest> keywords = new ArrayList<>();
	@Override
	public void updateProperties() throws CrawlerProviderException {
            this.process();
            try{
                try{
                    defaultStreamURL = accessJsonp.exec("$.replay_url");
                }catch(Exception e){
                    //System.out.println("Periscope replay_url doesn't exist: "+e.getMessage());
                }
                if(defaultStreamURL == null) {defaultStreamURL = accessJsonp.exec("$.hls_url");}
                if(defaultStreamURL == null) {defaultStreamURL = accessJsonp.exec("$.lhlsweb_url");}
                if(defaultStreamURL == null) {defaultStreamURL = accessJsonp.exec("$.https_hls_url");}
                defaultStreamURL = decompose(defaultStreamURL);
                isLive = accessJsonp.exec(".broadcast.state").equals("RUNNING");
                username = accessJsonp.exec("$.broadcast.user_display_name");		
                defaultThumbnailURL = accessJsonp.exec("$.broadcast.image_url");
                longitude = accessJsonp.execDouble("$.broadcast.ip_lng");
                latitude =  accessJsonp.execDouble("$.broadcast.ip_lat"); 
                location = accessJsonp.exec("$.broadcast.country")+", "+accessJsonp.exec("$.broadcast.city"); 
                watched = broadcastJsonp.execInt("$.n_watched");
                videoId = accessJsonp.exec("$.broadcast.id");
                title = accessJsonp.exec("$.broadcast.status");
                publishDate = parseDate(accessJsonp.exec("$.broadcast.updated_at"));                
                try{
                    rotation = accessJsonp.execInt("$.broadcast.camera_rotation");
                    height = accessJsonp.execInt("$.broadcast.height");
                    width = accessJsonp.execInt("$.broadcast.width");
                    tags = accessJsonp.execList("$.broadcast.tags");
                }catch(Exception e){
                    //System.out.println("Periscope not required data error: "+e.getMessage());
                }                
            }catch(Exception ex){
                CrawlLogger.getInstance().sendError(ex);
                throw new CrawlerProviderException("We catched an error its message: "+ex.getMessage());
            }
            finally{
                if(broadcastJsonp != null){ broadcastJsonp.close();}
                if(accessJsonp != null){ accessJsonp.close();}
            }
	}
    @Override
    public Video saveVideo(Video video) throws CrawlerProviderException {
        String result = this.isValidResult();
        if(!result.equals("OK")){throw new CrawlerProviderException(CrawlerProviderException.TYPEVIDEO,"Does not have valid rasult: "+result);}
        try{
            video.setDefaultstreamurl(defaultStreamURL);
            video.setDefaultthumbnailurl(defaultThumbnailURL);
            video.setLive(isLive);
            if(video.getLongitude() == null || video.getLongitude()<1){video.setLongitude(longitude);}
            if(video.getLatitude()== null || video.getLatitude()<1){video.setLatitude(latitude);}        
            if(video.getPublishdate() == null){video.setPublishdate(publishDate);};
            video.setOriginalurl(videoId);
            video.setProvidertype(CrawlService.TYPEPERISCOPE);
            video.setTitle(title);
            video.setUsername(username);
            video.setVideoid(videoId);
            video.setWatched(watched);
            if((video.getLocation() == null || video.getLocation().trim().isEmpty()) && (video.getLongitude()!=0 && video.getLatitude()!=0)){
                if(location != null && !location.trim().isEmpty()){
                    video.setLocation(location);
                }else{
                    LocationHelper l = new LocationHelper(video.getLatitude().toString(), video.getLongitude().toString());
                    video.setLocation(l.getFormattedAddress());
                }
            }
            details = new Videodetails();
            details.setVideoheight((short)height);
            details.setVideowidth((short)width);
            details.setOrientation((rotation == 90?Orientationtype.landscape:Orientationtype.portrait));
            tags.forEach((tag)->{
                keywords.add(new Interest(tag));
            });
        }catch(Exception e){
            CrawlLogger.getInstance().sendError(e);
            throw new CrawlerProviderException("Catched exception in saveVideo(periscope): "+e.getMessage()+" class: "+e.getClass().getTypeName());
        }
        return video;
    }
    
    private Date parseDate(String exec) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", new Locale("hu"));
                Date parsedDate = dateFormat.parse(exec);
                final long hoursInMillis = 60L * 60L * 1000L;
                Date newDate = new Date(parsedDate.getTime() + (Periscope.plusHours * hoursInMillis));
                return newDate;
            } catch (ParseException ex) {
                CrawlLogger.getInstance().sendError(ex);
                return new Date();
            }
    }
}
