package crawler.provider.implementations;

import com.angl.angldao.models.Video;
import crawler.engine.CSSPathEngine;
import crawler.engine.JSONPReader;
import crawler.provider.Provider;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.CrawlerProviderException;
import crawler.utils.LocationHelper;
import crawler.spider.TwitterVideoCrawler;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import lombok.Getter;
import lombok.Setter;

public class Twitter extends Provider {

    private static @Getter @Setter Long twitterLong = 3L;

    private JSONPReader videoDataDoc;
    private String periscopeId = null;
    private Periscope periscopeProvider = null;
    protected static String REGEXP_FOR_VIDEOID_EXTRACTOR = ".*/(.*)\\?";

    public Twitter(String URL) {
        super(URL, REGEXP_FOR_VIDEOID_EXTRACTOR);
    }

    @Override
    public void process() throws CrawlerProviderException {
        if(this.URL == null || this.URL.trim().isEmpty()){throw new CrawlerProviderException("Twitter provider got null or empty to URL: "+this.URL);}
        CSSPathEngine videoPlayerDoc = null;
        CSSPathEngine htmlDoc = null;
        try {
            if (this.URL.contains("twitter")) {                
                htmlDoc = new CSSPathEngine(this.URL);
                if(!htmlDoc.getContent().trim().isEmpty()){
                    String videoPlayerUrl = htmlDoc.exec("meta[property='og:video:url']", "content");
                    videoPlayerDoc = new CSSPathEngine(videoPlayerUrl);
                    if(!videoPlayerDoc.getContent().trim().isEmpty()){
                        String jsonData = videoPlayerDoc.exec("div#playerContainer", "data-config");
                        videoDataDoc = new JSONPReader(jsonData);
                    }else{ throw new CrawlerProviderException("The videoPlayerDoc in Twitter provider is empty");}
                }else{ throw new CrawlerProviderException("The htmlDoc in Twitter provider is empty");}
            }else if(this.URL.contains("www.pscp.tv")){
                htmlDoc = new CSSPathEngine(this.URL);
                if(!htmlDoc.getContent().trim().isEmpty()){
                    String twittercarduri = htmlDoc.exec("meta[property='twitter:player']", "content");
                    String [] elementOfUri = twittercarduri.split("/");
                    this.periscopeId = elementOfUri[elementOfUri.length-2];
                    this.periscopeProvider = new Periscope(this.periscopeId);
                }else{ throw new CrawlerProviderException("The htmlDoc in Twitter provider is empty");}
            }else{throw new CrawlerProviderException("The URL is not a twitter url: "+this.URL);}
        } catch (CrawlerProviderException e) {
           CrawlLogger.getInstance().sendError(e);
           throw new CrawlerProviderException("We catched an error its message: "+e.getMessage());
        } catch (Exception e) {
           CrawlLogger.getInstance().sendError(e);
           throw new CrawlerProviderException("We catched an error its message: "+e.getMessage());
        }finally {
            if (videoPlayerDoc != null) {videoPlayerDoc.close();}
            if (htmlDoc != null) {htmlDoc.close();}            
        }
    }    
    
    @Override
    public synchronized void updateProperties() throws CrawlerProviderException {
        this.process();
        if(this.periscopeId != null && this.periscopeProvider != null){
            
            System.out.println("PERISCOPE:"+ this.periscopeId);
            this.periscopeProvider.updateProperties();
        }else{
            if(videoDataDoc == null){throw new CrawlerProviderException("The videoDataDoc in Twitter provider is null");}
            if (videoDataDoc.getJsoncContent().trim().isEmpty()) {throw new CrawlerProviderException("The videoDataDoc in Twitter provider is empty");}
            try {
                videoId = videoDataDoc.<String>exec(String.class,"$..status..id_str");
                username = videoDataDoc.<String>exec(String.class,"$..videoInfo..publisher..name") + " @" + videoDataDoc.<String>exec(String.class,"$..videoInfo..publisher..screen_name");
                title = videoDataDoc.<String>exec(String.class,"$..status..text");
                watched = this.parseWatch(videoDataDoc.<String>exec(String.class,"$..view_counts"));
                latitude = this.generateLatLong(TwitterVideoCrawler.getInstance().getGeo_lat());
                longitude = this.generateLatLong(TwitterVideoCrawler.getInstance().getGeo_lon());
                defaultThumbnailURL = videoDataDoc.<String>exec(String.class,"$..image_src");
                isLive = this.parseIsLive(videoDataDoc.<String>exec(String.class,"$..status..created_at"));
                if(isLive == null){
                    throw new Exception("This video is older then the border!");
                }
                String simpleVideoUrl = videoDataDoc.<String>exec(String.class,"$.video_url");
                if (simpleVideoUrl != null) {defaultStreamURL = simpleVideoUrl;}
            } catch (Exception e) {
                CrawlLogger.getInstance().sendError(e);
                throw new CrawlerProviderException(e.getMessage());
            } 
        }
    }
    
    @Override
    public Video saveVideo(Video video) throws CrawlerProviderException {
        if(this.periscopeId != null && this.periscopeProvider != null){
            return this.periscopeProvider.saveVideo(video);
        }else{
            String result = this.isValidResult();
            if(!result.equals("OK")){throw new CrawlerProviderException(CrawlerProviderException.TYPEVIDEO,"Does not have valid rasult: "+result);}
            try{
                video.setDefaultstreamurl(defaultStreamURL);
                video.setDefaultthumbnailurl(defaultThumbnailURL);
                video.setLatitude(latitude);
                video.setLive(isLive);
                video.setLongitude(longitude);
                video.setOriginalurl(URL);
                video.setProvidertype(CrawlService.TYPETWITTER);
                video.setPublishdate(publishDate);
                video.setTitle(title);
                video.setUsername(username);
                video.setVideoid(videoId);
                video.setWatched(watched);
                if((video.getLocation() == null || video.getLocation().trim().isEmpty()) && (video.getLongitude()!=0 && video.getLatitude()!=0)){
                    LocationHelper l = new LocationHelper(video.getLatitude().toString(), video.getLongitude().toString());
                    video.setLocation(l.getFormattedAddress());
                }
            }catch(Exception e){            
                CrawlLogger.getInstance().sendError(e);
                throw new CrawlerProviderException("Catched exception in saveVideo(twitter): "+e.getMessage()+" class: "+e.getClass().getTypeName());
            }
            return video;
        }
    }


    private Integer parseWatch(String exec) throws Exception{
        if (exec == null) {return 0;}
        Boolean hasDot = exec.contains(".");
        String watchedWithoutComma = exec.replaceAll(",", "").replaceAll("\\.", "");
        String theWatched = "0";
        if (watchedWithoutComma.contains("K")) {
            theWatched = watchedWithoutComma.replaceAll("K", (hasDot ? "00" : "000"));
        } else if (watchedWithoutComma.contains("M")) {
            theWatched = watchedWithoutComma.replaceAll("M", (hasDot ? "00000" : "000000"));
        } else {
            theWatched = watchedWithoutComma;
        }
        return Integer.valueOf(theWatched);
    }

    private Boolean parseIsLive(String exec) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
            Date parsedDate = dateFormat.parse(exec);
            this.publishDate = parsedDate;
            Date oldDate = new Date(); 
            final long hoursInMillis = 60L * 60L * 1000L;
            Date newDate = new Date(oldDate.getTime() - (Twitter.twitterLong * hoursInMillis));
            Date isNeed = new Date(oldDate.getTime() - (Video.getBorderTime() * hoursInMillis));
            if((isNeed.getTime()>parsedDate.getTime())){return null;}
            return (newDate.getTime()<parsedDate.getTime());
        } catch (ParseException e) {
            CrawlLogger.getInstance().sendError(e);
            if(this.publishDate == null){this.publishDate = new Date();}
            return false;
        }
    }

    private Double generateLatLong(double coordinate) throws Exception{
        double direction = Math.random();
        double value = (double)((int)((Math.random()*1000)+500))/100000;    
        coordinate = (direction>0.5?(coordinate+value):(coordinate-value));
        coordinate = BigDecimal.valueOf(coordinate).setScale(6, RoundingMode.HALF_UP).doubleValue();
        return coordinate;
    }
}
