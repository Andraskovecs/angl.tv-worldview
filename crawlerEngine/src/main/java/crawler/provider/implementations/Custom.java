/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.provider.implementations;

import com.angl.angldao.models.Interest;
import com.angl.angldao.models.Keyword;
import com.angl.angldao.enums.Orientationtype;
import com.angl.angldao.enums.Sextype;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videodetails;
import crawler.engine.JSONPEngine;
import crawler.provider.Provider;
import crawler.CrawlLogger;
import crawler.CrawlService;
import crawler.CrawlerProviderException;
import crawler.utils.LocationHelper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import lombok.Getter;

/**
 *
 * @author hans
 */
public class Custom extends Provider{
    private String detailsUrl = "http://www.fakeprovider.lh/?menu=video&id=%s";
    private JSONPEngine engine;

    public Custom(String URL, String detailsUrl) {        
        super(URL);
        this.detailsUrl = detailsUrl;
        this.URL = String.format(this.detailsUrl, URL);
    }

    @Override
    public void process() throws CrawlerProviderException {
        if(this.URL != null && !this.URL.trim().isEmpty()){
            engine = new JSONPEngine(this.URL);
        }
    }

    @Override
    public Video saveVideo(Video video) throws CrawlerProviderException {
        String result = this.isValidResult();
        if(!result.equals("OK")){throw new CrawlerProviderException(CrawlerProviderException.TYPEVIDEO,"Does not have valid rasult: "+result);}        
        result = this.isValidCustomResult();        
        if(!result.equals("OK")){throw new CrawlerProviderException(CrawlerProviderException.TYPEVIDEO,"Does not have valid rasult: "+result);}        
        video.setDefaultstreamurl(defaultStreamURL);
        video.setDefaultthumbnailurl(defaultThumbnailURL);
        video.setLive(isLive);
        video.setLongitude(longitude);        
        video.setLatitude(latitude);
        video.setOriginalurl(URL);
        video.setProvidertype(CrawlService.TYPECUSTOM);
        video.setPublishdate(publishDate);
        video.setTitle(title);
        video.setUsername(username);
        video.setVideoid(videoId);
        video.setWatched(watched);
        if((video.getLocation() == null || video.getLocation().trim().isEmpty()) && (video.getLongitude()!=0 && video.getLatitude()!=0)){
                LocationHelper l = new LocationHelper(video.getLatitude().toString(), video.getLongitude().toString());
                video.setLocation(l.getFormattedAddress());
            }
        this.saveDetails();
        this.saveKeywords();
        this.saveStreamaer();
        return video;
    }
    private String videoFormat, category, eventName, width, height, orientation, age, sex, qualifacitain, profession;
    private List<String> keywords;
    @Override
    public void updateProperties() throws CrawlerProviderException {
        this.process();
        try{
            String status = engine.exec("$..status");
            if(status.equals("ok")){
                defaultStreamURL = engine.exec("$..videoData..streamUrl");
                isLive = engine.exec("$..videoData..isLive").equals("true");            
                latitude = Double.parseDouble(engine.exec("$..videoData..latitude"));
                longitude = Double.parseDouble(engine.exec("$..videoData..longitude"));
                watched = Integer.parseInt(engine.exec("$..videoData..watched"));
                defaultThumbnailURL = engine.exec("$..videoData..thumbnailUrl");
                videoId = engine.exec("$..videoData..videoId");
                username = engine.exec("$..videoData..username");
                title = engine.exec("$..videoData..title");
                publishDate = this.getDate(engine.exec("$..videoData..publishDate"));
                videoFormat = engine.exec("$..videoDetails..videoFormat");
                category = engine.exec("$..videoDetails..category");
                eventName = engine.exec("$..videoDetails..eventName");
                width = engine.exec("$..videoDetails..width");
                height = engine.exec("$..videoDetails..height");
                orientation = engine.exec("$..videoDetails..orientation");
                age = engine.exec("$..streamerData..age");
                sex = engine.exec("$..streamerData..sex");
                qualifacitain = engine.exec("$..streamerData..qualifacitain");
                profession = engine.exec("$..streamerData..profession");
                keywords = engine.execList("$..keyword");
            }
        }catch (NumberFormatException e) {
            CrawlLogger.getInstance().sendError(e);
            throw new CrawlerProviderException("NumberFormatException: "+e.getMessage());
        }catch (Exception e) {
            CrawlLogger.getInstance().sendError(e);
            throw new CrawlerProviderException("Exception: "+e.getMessage());
        }finally{
            if(engine != null) {engine.close();}
        }
    }

    private Date getDate(String exec) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date parsedDate = dateFormat.parse(exec);
            return parsedDate;
        } catch (ParseException ex) {
            return new Date();
        }
    }
    private @Getter Videodetails details;
    private void saveDetails() {
        details = new Videodetails();
        if(height != null)details.setVideoheight(Short.parseShort(height));
        if(width != null)details.setVideowidth(Short.parseShort(width));
        details.setVideocategory(category);
        if(orientation != null)details.setOrientation((orientation.equals("landscape")?Orientationtype.landscape:Orientationtype.portrait));
        if(eventName != null)details.setEvent(eventName);
        if(videoFormat != null)details.setVideoformat(videoFormat);
    }
    private void saveStreamaer() {
        if(age != null)details.setStreamerage(Short.parseShort(age));
        if(profession != null)details.setStreamerprofession(profession);
        if(qualifacitain != null)details.setStreamerqualification(qualifacitain);
        if(sex != null)details.setStreamersex((sex.equals("male")?Sextype.male:Sextype.female));
    }
    private @Getter List<Interest> keywordsList = new ArrayList<>();
    private void saveKeywords() {
        keywords.forEach((k)->{
            keywordsList.add(new Interest(k));
        });
    }

    private String isValidCustomResult() {
        if(this.category == null || this.category.trim().isEmpty())return "Category is null or empty!";
        if(this.keywords == null || this.keywords.isEmpty())return "Keywords is null or empty!";
        if(this.longitude == null || this.longitude<1)return "Longitude is null or empty!";
        if(this.latitude == null || this.latitude<1)return "Latitude is null or empty!";
        return "OK";
    }

    @Override
    public String toString() {
        return "Custom{" + "videoFormat=" + videoFormat + ", category=" + category + ", eventName=" + eventName + ", width=" + width + ", height=" + height + ", orientation=" + orientation + ", age=" + age + ", sex=" + sex + ", qualifacitain=" + qualifacitain + ", profession=" + profession + ", keywords=" + keywords + '}';
    }
    
}
