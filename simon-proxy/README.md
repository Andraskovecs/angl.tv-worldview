
# proxy

ports: all configurable via the docker-compose.yml

- 8090: proxy
- 8091: proxy setup, config
- 6379 redis instance (only accessible from localhost)

## proxy config

```json
{
  "method": "insert",
  "videoid": "1dRKZepnklaxB",
  "defaultstreamurl": "https://prod-video-us-east-1.pscp.tv/...ay-direct-prod-us-east-1-public/playlist_16927099745026366727.m3u8",
  "defaultthumbnailurl": "https://prod-video-us-east-1.pscp.tv/D16i...CA/replay_thumbnail/us-east-1/periscope-replay-direct-prod-us-east-1-public/...-tdm27uXvqLc&service=proxsee&digest=Q109bXU1KgkzaTOVf-0gboUlELvAqx6p4nfwsxMfV7Y"
}

```


## how to use

### backup and restore
- the load methods works without the tar.gz extension! keep in mind!


### dependencies

- make jq
- docker & docker-compose
- user with docker and sudo group permission

### install

- clone the git repository


```bash
# PRE

# OPT A - start with already built image `nginx-simon-proxy.tar` in the actual directory
$ make load-image

# OPT B - build the image yourself, BEWARE the version mismatching... TODO: lock version file
$ make build # build the image

# START
$ make run-deploy # then run

# stop
$ make stop

# dump the db to JSON format
$ make dump # this will create a timestamped json dump into the `dump` directory

$ make dump DUMPFILE=example_new.json # this will create a example_new.json.tar.gz `dump` into the dump directory

# load the JSON dump into the db
$ make load LOADFILE=example.json # this will load the example.json.tar.gz into the db from the `dump` directory
$ # like: make load LOADFILE=dump_1528720915.json

# clean all the data and stuff! WARNING this will delete all your db entries and logs
$ make clean-redis clean-logs

```
