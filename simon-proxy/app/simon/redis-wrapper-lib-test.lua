require "luarocks.loader"


local F = require("F")
local cjson = require 'cjson'

local utils = require 'simpleutils'
local redis_lib = require "redis-wrapper-lib"

local u = utils
local i = utils.log_info



local redis = redis_lib:new('127.0.0.1', 6379)

redis:set('hello', 'world')
value = redis:get('hello')

i{F"value: {value}"}
