require "luarocks.loader"


local F = require 'F'
local cjson = require 'cjson'
local class = require 'middleclass'
require('table')

local utils = require 'simpleutils'
local redis_lib = require "redis-wrapper-lib"

-- import aliases
local i = utils.log_info
local u = utils
local sha1 = u.sha1


local DbUpdate = class('DbUpdate')


function DbUpdate:initialize(pushContent)

	self.success = false

	-- constant baseHost namespace
	local baseHost = "simon:/proxy/";
	i{F"baseHost: {baseHost}"}

	-- constant absolute base namespace
	local redisPathNS = "simon:paths:"


	-- redis key
	local proxyBasePath = baseHost .. pushContent.videoid;
	i{F"proxyBasePath: {proxyBasePath}"}


	-- strip protocoll
	local urlWithoutProt = pushContent.defaultstreamurl:gsub("http://", ""):gsub("https://", "")
	i{F"urlWithoutProt: {urlWithoutProt}\n\n"}

	local thumbnailWithoutProt = pushContent.defaultthumbnailurl:gsub("http://", ""):gsub("https://", "")
	i{F"thumbnailWithoutProt: {thumbnailWithoutProt}\n\n"}


	-- explode url
	local explodedURI = utils.explodeURI(urlWithoutProt)
	local pathPart = explodedURI.pathPart
	local fnamePart = explodedURI.fnamePart
	local hostPart = explodedURI.hostPart

	if not pathPart or not fnamePart then
		return
	end

	i{F"hostPart: {hostPart}"}
	i{F"pathPart: {pathPart}"}
	i{F"fnamePart: {fnamePart}"}
	i{F"hash(pathPart): {sha1(pathPart)}"}


	-- playlist file constant
	local playlistName = "/playlist.m3u8";
	local thumbnailName = "/thumbnail";


	-- TODO make parameters
	local client = redis_lib:new('127.0.0.1', 6379)


	-- forge base redis keys and values
	local basePlaylistKey = proxyBasePath .. playlistName
	local thumbnailKey = proxyBasePath .. thumbnailName
	local basePlaylistValue = urlWithoutProt
	local thumbnailValue = thumbnailWithoutProt
	i{F"basePlaylist: '{basePlaylistKey}' -> '{basePlaylistValue}'"}
	i{F"thumbnail: '{thumbnailKey}' -> '{thumbnailValue}'"}


	-- dl playlistFile
	local playlistRawContent = utils.get_url('https://' .. basePlaylistValue)
	if not playlistRawContent then
		i{F"link seems dead: https://{basePlaylistValue}"}
		return
	end

	local truncLimit = 100
	i{F"playlistRawContent(truncated for {truncLimit} char): \n{playlistRawContent:sub(0, truncLimit)}\n\n"}

	local absolutePlaylistURLsTable = {}
	local hashes = {}


	-- if playlist file is a dynamicall generated playlist file with absolute urls
	if playlistRawContent:find('#EXT%-X%-DYNAMICALLY%-GENERATED') ~= nil then

		-- get abs playlists
		local absolutePlaylistURLs = {}
		for line in u.magiclines(playlistRawContent) do
			if line:find("^[^#]") ~= nil then
				table.insert(absolutePlaylistURLs, line)
			end
		end
		local tableCon = table.concat(absolutePlaylistURLs, '\n\n')
		i{F"absolutePlaylistURLs: {tableCon}\n\n"}

		-- process playlists
		for idx, line in ipairs(absolutePlaylistURLs) do
			local absPathPart = string.match(line, '^(.+/)[^/]+$')
			local hashedPathPart = sha1(absPathPart)
			table.insert(hashes, hashedPathPart)
			absolutePlaylistURLsTable[hashedPathPart] = hostPart .. absPathPart
			i{F"hashedPathPart:absPathPart -> {hashedPathPart}:{hostPart}{absPathPart}\n"}
		end

	else

		local hashedPathPart = sha1(pathPart)
		table.insert(hashes, hashedPathPart)
		absolutePlaylistURLsTable[hashedPathPart] = hostPart .. pathPart
		i{F"hashedPathPart:pathPart -> {hashedPathPart}:{pathPart}\n"}

	end


	-- register urls in redis
	i{"F register absolute urls for videoId playlist"}
	i{F"hashes: {cjson.encode(hashes)}"}

	client:set(basePlaylistKey .. ":abs", cjson.encode(hashes))


	-- for absolutePlaylist
	i{F"absolutePlaylistURLsTable: \n{utils.dump_table(absolutePlaylistURLsTable)}\n\n"}
	for hash, absUrl in pairs(absolutePlaylistURLsTable) do
		client:set(redisPathNS .. hash, absUrl)
		client:set(redisPathNS .. hash .. ":videoId", pushContent.videoid)
		i{F"redis client set: hash:absUrl -> '{hash}':'{absUrl}'\n"}
	end


	-- for base urls
	client:set(basePlaylistKey, basePlaylistValue)
	i{F"redis client set: basePlaylist:basePlaylistValue -> '{basePlaylistKey}':'{basePlaylistValue}'\n"}

	-- for thumbnails
	client:set(thumbnailKey, thumbnailValue)
	i{F"{thumbnailKey}' -> '{thumbnailValue}'\n"}

	self.success = true
end


return DbUpdate
