require "luarocks.loader"

local F = require 'F'
local redis = require 'redis'

local utils = require 'simpleutils'

-- import aliases
local i = utils.log_info
local u = utils


local class = require 'middleclass'
local redisWrapper = class('redisWrapper')

function redisWrapper:initialize(host, port)
	i{F"redis initalize: {host}:{port}"}

	self.type = nil

	pcall(
		function ()
			local client = redis.connect(host, port)
			local pingres = client:ping()
			if not pingres then
				error({msg="not pingable"})
				i{F"luarocks redis: not pingable"}
				return
			end
			self.client = client
			self.type = 'luarocks'
		end
	)

	pcall(
		function()
            local red = redis:new()
            local ok, err = red:connect(host, port)
            if not ok then
                error({msg="failed to connect: ", err=err})
                i{F"openresty redis: failed to connect"}
                return
            end
            self.client = red
            self.type = 'openresty'
		end
	)
	i{F"redis client choosed: {self.type}"}
end

function redisWrapper:set(key, val)
	return self.client:set(key, val)
end

function redisWrapper:get(key)
	return self.client:get(key)
end


return redisWrapper
