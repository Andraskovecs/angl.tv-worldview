-- imports [[
	package.path = package.path .. 
	    ";/opt/openresty/lualib/simon/?.lua" ..
	    ";/opt/openresty/lualib/?.lua" ..
	    ";/root/.luarocks/share/lua/5.1/" ..
	    ";/opt/openresty/site/lualib/?.lua;;"

	require "luarocks.loader"


	local F = require("F")	
	local cjson = require 'cjson'

	local utils = require 'simpleutils'

	local u = utils
	local i = utils.log_info

	local DbUpdate = require('db-update-lib')
-- ]]


-- local sess = u.sess_start()
-- i{F"sess: {sess.time}, {sess.sessId}"}


-- process/decode ngx post body message
ngx.req.read_body()
local body = ngx.req.get_body_data()
i{F"body data: \n{body}\n\n"}

local playlistPush = cjson.decode(body)


-- then call the dbupdate
local result = DbUpdate:new(playlistPush)

-- sess = u.sess_end()
-- i{F"sess / duration: {sess.sessId}:{sess.duration}"}

if result.success == true then
	ngx.say('OK')
end

ngx.status = ngx.HTTP_BAD_REQUEST
