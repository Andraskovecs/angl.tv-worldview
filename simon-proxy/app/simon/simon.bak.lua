
package.path = package.path .. 
    ";/opt/openresty/lualib/simon/?.lua" ..
    ";/opt/openresty/lualib/?.lua" ..
    ";/opt/openresty/site/lualib/?.lua;;"


local rnd = math.random()
ngx.log(ngx.DEBUG, "\n\n\n\n\n[[rnd:" .. rnd)

local h = ngx.req.get_headers()
for k, v in pairs(h) do
    ngx.log(ngx.DEBUG, "headers " .. k .. ":" .. v)
    if k:find("^cookie") == nil then
        ngx.req.clear_header(k)
    end
end


ngx.log(ngx.DEBUG, "headers after clean [[")
local h = ngx.req.get_headers()
for k, v in pairs(h) do
    ngx.log(ngx.DEBUG, "headers " .. k .. ":" .. v)
end
ngx.log(ngx.DEBUG, "headers after clean ]]")


ngx.header['Access-Control-Allow-Origin'] = '*'


function cr(val)
    return tostring(val) ~= "userdata: NULL"
end

function crd(val, name)
    if not cr(val) then
        val = nil
        ngx.log(ngx.DEBUG, "`", name, "`is empty, defaults to nil")
    end
    return val
end


function b2s(val)
    local res = "false"
    if val == true then
        res = "true"
    end
    return res
end


local resty_sha1 = require "sha1"
local str = require "resty.string"

function doHash(msg)
    msg = ("" or msg)
    local rnd = math.random() .. os.clock()
    local sha1 = resty_sha1:new()
    sha1:update(msg .. rnd)
    local digest = sha1:final()
    local hash = str.to_hex(digest)
    ngx.log(ngx.DEBUG, "for msg `" .. msg .. "` sha1: " .. hash)
    return hash
end


local os = require 'os'
local x = os.clock()

local ck = require "cookie"
local cookie = ck:new()


-- Connect to Redis
local redis = require 'redis'
local red = redis:new()
red:connect('127.0.0.1', 6379)


-- ngx.log(ngx.DEBUG, "prev: x-simon:" .. ("" or ngx.header['x-simon']))
-- ngx.header['x-simon'] = rnd

local xsimon = cookie:get("x-simon")
ngx.log(ngx.DEBUG, "x-simon cookie:" .. (xsimon or ""))


local uri = string.gsub(ngx.var.request_uri, "?.*", "") or ""
ngx.log(ngx.DEBUG, "uri:" .. uri)

local fullp = uri:find("^proxy/") == nil and xsimon ~= nil


-- Choose a random backend
ngx.log(ngx.DEBUG, "search key `simon:" .. uri .. "`")
local backend = red:get('simon:' .. uri)

-- check and defaults to nil
backend = crd(backend, "backend value for: " .. 'simon:' .. uri)

local chosen = backend


local pathPart = string.match(uri, '(.*)/.*')
local fnamePart = string.match(uri, '.*/(.*)')

ngx.log(ngx.DEBUG, "pathPart=" .. (pathPart or ""))
ngx.log(ngx.DEBUG, "fnamePart=" .. (fnamePart or ""))


local path_chosen = nil

local direc = false

-- Choose base path
if chosen == nil then

    ngx.log(ngx.DEBUG, "pathc_key=" .. 'simon:' .. pathPart)
    local pathc = red:get('simon:' .. pathPart)
    path_chosen = crd(pathc, "backend value for: " .. ('simon:' .. pathPart))
    if path_chosen ~= nil then direc = true end
    ngx.log(ngx.DEBUG, "path_chosen="  .. (path_chosen or ""))

end

-- TODO: regexp for playlist
if chosen ~= nil and not xsimon then

    ngx.log(ngx.DEBUG, "search for host key:  host_backends=" .. ('simon:' .. uri .. ':host') )
    local host_back = red:get('simon:' .. uri .. ':host')
    host_back = crd(host_back, "backend value for: `" .. ('simon:' .. uri .. ':host') .. "`")
    ngx.log(ngx.DEBUG, "result for host key:  host_back=" .. (host_back or ""))

    local session = doHash()
    ngx.log(ngx.DEBUG, "for host_back the session=" .. (session or ""))
    cookie:set({ key = "x-simon", value = session, path = "/" })
    ngx.log(ngx.DEBUG, "x-simon cookie set:" .. session)

    red:set("simon:sess:" .. session, host_back)
    ngx.log(ngx.DEBUG, "session set for key: " .. ("simon:sess:" .. session) .. " | with keys:" .. (host_back or ""))

end


ngx.log(ngx.DEBUG, "chosen="  .. (chosen or "") )

-- Set proxy_to variable
if direc == false then
    ngx.var.v_proxy_to = chosen
else
    ngx.var.v_proxy_to = path_chosen .. "/" .. fnamePart
end


-- search for session key
ngx.log( ngx.DEBUG, "search for session key: " ..
    " fullp=" .. b2s(fullp) ..
    " ngx.var.v_proxy_to=" .. (ngx.var.v_proxy_to or "") .. 
    " xsimon=" .. (xsimon or "")
)

if fullp and not ngx.var.v_proxy_to and xsimon ~= nil then
    -- search for host and setup
    local session_host = red:get("simon:sess:" .. xsimon)
    -- ngx.log( ngx.DEBUG, "fullp: session_host=" .. (session_host or ""))
    ngx.var.v_proxy_to = session_host .. (uri or "")
end


-- Result
ngx.log(ngx.DEBUG, "result: v_proxy_to="  .. (ngx.var.v_proxy_to or "nil") )
ngx.log(ngx.DEBUG, string.format("]]:rnd: %s\n elapsed time: %.8f\n\n\n\n\n", rnd, os.clock() - x))
