require "luarocks.loader"

local _ = require ("moses")
local F = require("F")

local rnd = math.random()
local os = require 'os'


-- magiclines(string) | https://stackoverflow.com/a/19329565 [[
	local function magiclines(s)
	  if s:sub(-1)~="\n" then s=s.."\n" end
	  return s:gmatch("(.-)\n")
	end
-- ]]


-- lines_from(file), file_exists(file) | https://stackoverflow.com/a/11204889 [[

	-- see if the file exists
	local function file_exists(file)
	  local f = io.open(file, "rb")
	  if f then f:close() end
	  return f ~= nil
	end

	-- get all lines from a file, returns an empty 
	-- list/table if the file does not exist
	local function lines_from(file)
	  if not file_exists(file) then return {} end
	  lines = {}
	  for line in io.lines(file) do 
	    lines[#lines + 1] = line
	  end
	  return lines
	end
-- ]]


-- strtable_to_str(table):string | https://www.lua.org/pil/2.5.html [[
	local function strtable_to_str(a)
		result = ""
		for i, line in ipairs(a) do
	      result = result .. line .. "\n"
	    end
	    return result
	end
-- ]]


-- sha1(string):hash [[
	local function sha1(msg)
		local sha1 = require "sha1"
		result = nil
		if pcall(sha1, msg) then
			result = sha1(msg)
		else 
			local str = require "resty.string"
			local sha = sha1:new()
    		sha:update(msg)
    		local digest = sha:final()
    		local hash = str.to_hex(digest)
    		result = hash
		end
		return result
	end
--]]


-- randomString(length):string [[ | https://gist.github.com/earthgecko/3089509
	local function randomString(length)
		if not ngx then
			local handle = io.popen("cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w " .. tonumber(length) .. " | head -n 1 | tr '\n' ' '")
			local result = handle:read("*a")
			handle:close()
			return result
		end
		return "somerandomnumber"
	end
-- ]]


-- sess_start(): {time, sessId} | sess_get():{time, sessId} | sess_end():{time} [[
	local function sess_start()		
		___TIMER = os.clock()
		___SESSION = randomString(8)
		return {time=___TIMER, sessId = ___SESSION}
	end

	local function sess_get()
		return {time=___TIMER, sessId = ___SESSION}
	end

	local function sess_end()
		local result = nil
		local oldSess = ___SESSION
		if ___TIMER then
			result = os.clock() - ___TIMER
		end
		___TIMER = nil
		___SESSION = nil
		return {duration=result, sessId = oldSess}
	end
-- ]]


-- log_info(meta):nil | -- https://stackoverflow.com/a/6023372 [[
	local function log_info(t)
	    setmetatable(t, { __index={ p1='', p2='', p3='', p4='' }} )
	    local p1, p2, p3, p4 =
			t[1] or t.p1,
			t[2] or t.p2,
			t[3] or t.p3,
			t[4] or t.p4

		local p0 = ""
		if ___SESSION then
			p0 =  "[" .. ___SESSION .. "] "
		end

		result = p0 .. p1 .. p2 .. p3 .. p4

		if ngx~=nil then
			ngx.log(ngx.DEBUG, result)
		else
			print(result)
		end

	end
-- ]]


-- dump_table(table):string | https://github.com/Yonaba/Moses/blob/master/doc/tutorial.md#table [[
	local function dump_table(t)
		result = ""
		_.each(t,
			function(i,v)
				result = result .. F"'{i}':'{v}'\n\n"
			end
		)
	  	return result
	end
-- ]]


-- get_url(string):string [[
	local function get_url(uri)
		result = nil
		require("socket")
		local https = require("ssl.https")
		local body, code, headers, status = https.request(uri)
		if code == 200 then
			result = body
		end
		return result
	end
-- ]]


-- explodeURI(uri):{pathPart=pathPart, fnamePart=fnamePart, hostPart=hostPart} [[
	local function explodeURI(uri)
		local urlWithoutProt = uri:gsub("http://", ""):gsub("https://", "")

		-- explode url
		local pathPart = string.match(urlWithoutProt, '(/.*/).*')
		local fnamePart = string.match(urlWithoutProt, '.*/(.*)')
		local hostPart = string.match(urlWithoutProt, '([^/]+)/.*')

		local result = {pathPart=pathPart, fnamePart=fnamePart, hostPart=hostPart}
		return result
	end
-- ]]


-- TODO zsolturo Need to rewrite
--[[
function p(cnt)
	ngx.log( ngx.DEBUG, cnt)
end


function cr(val)
    return tostring(val) ~= "userdata: NULL"
end

function crd(val, name)
    if not cr(val) then
        val = nil
        ngx.log(ngx.DEBUG, "`", name, "`is empty, defaults to nil")
    end
    return val
end

function b2s(val)
    local res = "false"
    if val == true then
        res = "true"
    end
    return res
end

--]]


return {
  magiclines = magiclines,
  lines_from = lines_from,
  file_exists = file_exists,
  strtable_to_str = strtable_to_str,
  log_info = log_info,
  dump_table = dump_table,
  get_url = get_url,
  sha1 = sha1,
  explodeURI = explodeURI,
  sess_start = sess_start,
  sess_end = sess_end,
  sess_get = sess_get,
  randomString = randomString,
}
