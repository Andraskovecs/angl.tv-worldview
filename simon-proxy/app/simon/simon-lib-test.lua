require "luarocks.loader"

local F = require 'F'

local utils = require 'simpleutils'
local i = utils.log_info
local u = utils

local simon = require("simon-lib")

local sess = u.sess_start()
i{F"sess: {sess.time}, {sess.sessId}"}


local Simon = simon:new()

local result = nil

-- TODO make dynamic
local abs = false
if abs then
	local abs_playlist = "/proxy/1LyxBrwvgLpxN/playlist.m3u8"
	result = Simon:getRoute(abs_playlist)
	i{F"abs_playlist -> result\n {abs_playlist}:{result}"}
	i{}i{}

	local abs_path = "/Kc7TmgIthd_4R0skw7VvfF5iCCoqkrlRkCfFoTEJyewkVYJbtSU-yGYaRALnCEKyVUPJOYorV6-iL0kwYTX6FQ/replay_transcode/us-east-1/periscope-replay-direct-prod-us-east-1-public/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsInZlcnNpb24iOiIyIn0.eyJIZWlnaHQiOjI3MCwiS2JwcyI6NTAwLCJXaWR0aCI6NDgwfQ.0PLzHwJpzC4wA_jNo8XOma8zOx8uPmEDTD3WKSRLYOA/"
	local abs_incPlaylist = F"{abs_path}playlist_16924415456702778238.m3u8"
	result = Simon:getRoute(abs_incPlaylist)
	i{F"abs_incPlaylist -> result\n {abs_incPlaylist}:{result}"}
	i{}i{}

	local abs_chunk = F"{abs_path}chunk_1522328546383516777_4688_a.ts"
	result = Simon:getRoute(abs_chunk)
	i{F"request_uri -> result\n {abs_chunk}:{result}"}
end

local rel = true
if rel then
	local rel_playlist = "/proxy/1dRKZepnklaxB/playlist.m3u8"
	result = Simon:getRoute(rel_playlist)
	i{F"rel_playlist -> result\n {rel_playlist}:{result}"}
	i{}i{}

	local rel_path = "/proxy/1dRKZepnklaxB/"
	local rel_chunk = F"{rel_path}chunk_1519644319081769464_1586_a.ts"
	result = Simon:getRoute(rel_chunk)
	i{F"rel_chunk -> result\n {rel_chunk}:{result}"}
	i{}i{}

	local rel_path = "/proxy/1dRKZepnklaxB/"
	local rel_thumb = F"{rel_path}thumbnail"
	result = Simon:getRoute(rel_thumb)
	i{F"rel_thumb -> result\n {rel_thumb}:{result}"}
	i{}i{}
end

sess = u.sess_end()
i{F"sess / duration: {sess.sessId}:{sess.duration}"}

local explodedURI = utils.explodeURI("prod-video-us-east-1.pscp.tv/Kc7TmgIthd_4R0skw7VvfF5iCCoqkrlRkCfFoTEJyewkVYJbtSU-yGYaRALnCEKyVUPJOYorV6-iL0kwYTX6FQ/replay_transcode/us-east-1/periscope-replay-direct-prod-us-east-1-public/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsInZlcnNpb24iOiIyIn0.eyJIZWlnaHQiOjI3MCwiS2JwcyI6NTAwLCJXaWR0aCI6NDgwfQ.0PLzHwJpzC4wA_jNo8XOma8zOx8uPmEDTD3WKSRLYOA/playlist_16924415456702778238.m3u8")
local pathPart = explodedURI.pathPart
local fnamePart = explodedURI.fnamePart
local hostPart = explodedURI.hostPart
local hashedPathPart = u.sha1(pathPart)

i{F"hostPart: {hostPart}"}
i{F"pathPart: {pathPart}"}
i{F"fnamePart: {fnamePart}"}
i{F"hash(pathPart): {hashedPathPart}"}

