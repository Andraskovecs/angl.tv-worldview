require "luarocks.loader"

-- classic imports
local F = require 'F'
local cjson = require 'cjson'
require('table')
local class = require 'middleclass'

local utils = require 'simpleutils'
local redis_lib = require "redis-wrapper-lib"

-- import aliases
local i = utils.log_info
local u = utils
local sha1 = u.sha1


local Simon = class('Simon')
function Simon:initialize()
	self.client = redis_lib:new('127.0.0.1', 6379)
end


function Simon:getRoute(request_uri)
	i{F"simon getRoute called: request_uri: {request_uri}"}

	local explodedURI = utils.explodeURI(request_uri)
	local pathPart = explodedURI.pathPart
	local fnamePart = explodedURI.fnamePart
	local hostPart = explodedURI.hostPart
	local hashedPathPart = sha1(pathPart)

	i{F"hostPart: {hostPart}"}
	i{F"pathPart: {pathPart}"}
	i{F"fnamePart: {fnamePart}"}
	i{F"hash(pathPart): {hashedPathPart}"}

	local absHash = hashedPathPart

	-- if playlist file
	if request_uri:find("^/proxy/.*playlist%.m3u8$") then
		-- format: simon:/proxy/1LyxBrwvgLpxN/playlist.m3u8
		local val = self.client:get('simon:' .. request_uri)
		return val
	end

	-- if thumbnail
	if request_uri:find("^/proxy/.*/thumbnail$") then
		-- format: simon:/proxy/1LyxBrwvgLpxN/thumbnail
		local val = self.client:get('simon:' .. request_uri)
		return val
	end

	if request_uri:find("^/proxy/.+%..+$") then
		i{F"relative path found"}
		local val = self.client:get(F"simon:{pathPart}playlist.m3u8:abs")
		i{F"abs val:{val}"}
		if val then
			local enc = cjson.decode(val)
			if enc and #enc == 1 then
				absHash = enc[1]
			end
		end
	end

	local absProxyUrl = self.client:get('simon:paths:' .. absHash)
	i{F"absProxyUrl: {absProxyUrl}"}
	if absProxyUrl then
		return absProxyUrl .. fnamePart
	end

end


return Simon
