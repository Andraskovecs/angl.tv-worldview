package.path = package.path .. 
    ";/opt/openresty/lualib/simon/?.lua" ..
    ";/opt/openresty/lualib/?.lua" ..
    ";/opt/openresty/site/lualib/?.lua;;"

require "luarocks.loader"

-- classic imports
local F = require 'F'

local utils = require 'simpleutils'

-- import aliases
local i = utils.log_info
local u = utils
local sha1 = u.sha1


---


-- local sess = u.sess_start()
-- i{F"sess: {sess.time}, {sess.sessId}"}


-- clear all the headers
local h = ngx.req.get_headers()
for k, v in pairs(h) do
    i{F"headers: {k}:{v}, cleared"}
    ngx.req.clear_header(k)
end

-- set access control allow origin: this made THE trick
ngx.header['Access-Control-Allow-Origin'] = '*'


i{F"request_uri: ", ngx.var.request_uri}

local simon = require("simon-lib")
local Simon = simon:new()

local result = Simon:getRoute(ngx.var.request_uri)
i{F"simonlib: {result}"}
if result then
	ngx.var.v_proxy_to = result
else
	ngx.say("not found")
end


-- sess = u.sess_end()
-- i{F"sess / duration: {sess.sessId}:{sess.duration}"}
