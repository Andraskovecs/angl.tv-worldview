require "luarocks.loader"


local F = require("F")
local cjson = require 'cjson'

local utils = require 'simpleutils'
local u = utils
local i = utils.log_info


local function loadJson(fname)
	local playlistPushContent = utils.strtable_to_str(u.lines_from(fname))
	i{playlistPushContent}
	local playlistPush = cjson.decode(playlistPushContent)
	return playlistPush
end


local DbUpdate = require('db-update-lib')

local res = nil
res = DbUpdate:new(loadJson('../../test/data.json'))
i{F"rel res: {res.success}"}

-- res = DbUpdate:new(loadJson('../../test/absolute-data.json'))
-- i{F"abs res: {res.success}"}

-- res = DbUpdate:new(loadJson('../../test/error-data.json'))
-- i{F"err res: {res.success}"}
