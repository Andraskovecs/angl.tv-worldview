
import subprocess

import time
current_time = lambda: int(round(time.time()))

if True:
	# subprocess.run([
	#	"sudo",
	#	"mv", "../app/redis/data/dump.rdb", "../app/redis/data/dump_backup_" + str(current_time()) +".rdb", 
	#	"-v"])

	subprocess.run([
		"docker-compose", "-f", "../docker-compose.yml", "down"
	])

	subprocess.run([
		"sudo",
		"rm", "../app/redis/data/dump.rdb", "-v"
	])

	subprocess.run([
		"sudo",
		"cp","./jun11/dump.rdb", "../app/redis/data/", "-v"
	])

	subprocess.run([
		"sudo",
		"chown", "999:docker", "../app/redis/data/dump.rdb", "-v"
	])

	subprocess.run([
		"docker-compose", "-f", "../docker-compose.yml", "up", "-d"
	])

	time.sleep(5)


# import argparse
# parser = argparse.ArgumentParser()
# parser.parse_args()

# import redis
# r = redis.StrictRedis(host='localhost', port=6379, db=0)

# r.set('foo','bar')
# print(r.get('foo'))


# import redisdl

# with open("./jsondumps/dump_" + str(current_time()) +  ".json", 'w') as f:
#	# streams data
#	redisdl.dump(f)

# json_text = '...'
# redisdl.loads(json_text)

# with open('path/to/dump.json') as f:
#	# streams data if ijson or jsaone are installed
#	redisdl.load(f)
