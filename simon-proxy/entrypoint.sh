#!/bin/bash

set -e

chown -R redis /data

sv=$(which supervisord)

if [ ! -f /etc/supervisord.conf ]; then
	ln -s /etc/supervisor/supervisord.conf /etc/supervisord.conf
fi



function replace_g {

	REP_SOURCE="$1"
	REP_TARGET="$2"

	(mv -v "${REP_TARGET}" "${REP_TARGET}".`date +%s` 2>/dev/null) || true

	if [ ! -f "${REP_TARGET}" ]; then
		ln -sv "${REP_SOURCE}" "${REP_TARGET}"
	fi
}


REP_1_SOURCE="/app.conf.d/nginx/config/nginx.conf"
REP_1_TARGET="/opt/openresty/nginx/conf/nginx.conf"
replace_g "${REP_1_SOURCE}" "${REP_1_TARGET}"


REP_2_SOURCE="/app.conf.d/redis/config/redis.conf"
REP_2_TARGET="/etc/redis.conf"
replace_g "${REP_2_SOURCE}" "${REP_2_TARGET}"


REP_3_SOURCE="/app.conf.d/bash/.bashrc"
REP_3_TARGET="/root/.bashrc"
replace_g "${REP_3_SOURCE}" "${REP_3_TARGET}"


REP_4_SOURCE="/app.conf.d/bash/.bash_history"
REP_4_TARGET="/root/.bash_history"
replace_g "${REP_4_SOURCE}" "${REP_4_TARGET}"


if [ ! -f /etc/supervisord.conf ]; then
	ln -s /etc/supervisor/supervisord.conf /etc/supervisord.conf
fi


service cron start


if [ -z "$@" ]; then
  exec "$sv" -c /etc/supervisord.conf --nodaemon
else
  exec $@
fi
