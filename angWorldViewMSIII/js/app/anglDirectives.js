anglApp.directive('backgroundImageDirective', function () {
    return function (scope, element, attrs) {
        //console.log("ddd",attrs.backgroundImageDirective);
        var videoObj = JSON.parse(attrs.backgroundImageDirective);

        if (videoObj.defaultthumbnailurl && videoObj.providertype != "periscope") {
            element.css({
                'background-image': 'url(' + videoObj.defaultthumbnailurl + ') ',

            });
        } else if (videoObj.defaultthumbnailurl && videoObj.providertype == "periscope") {
            element.css({
                'background-image': 'url(http://104.237.4.64:8090/proxy/' + videoObj.videoid + '/thumbnail) ',

            });
        } else {
            element.css({
                'background-image': 'url(images/noImage.png)',

            });
        }

    };
});
anglApp.directive('spaceBackgroundImageDirective', function () {
    return function (scope, element, attrs) {
        //console.log("ddd",attrs.backgroundImageDirective);
        var spaceObj = JSON.parse(attrs.spaceBackgroundImageDirective);
      console.log(element);
           element.css({
                'background-image': 'url(https://maps.googleapis.com/maps/api/staticmap?size=350x350&zoom=8&center='+spaceObj.latitude+','+spaceObj.longitude+'&key=AIzaSyDLshjTTA0BVMVW3xVAtcm4Wq5J1nxUcwg&style=feature:all|saturation:-100|lightness:10&style=feature:poi.park|color:#333333&style=road.arterial|color:#ed1c24&style=road.highway|color:#3c3c3c)',

            });
        

   

    };
});
anglApp.directive('ngMouseWheelUp', function () {
    return function (scope, element, attrs) {
        element.bind("DOMMouseScroll mousewheel onmousewheel", function (event) {

            // cross-browser wheel delta
            var event = window.event || event; // old IE support
            var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));

            if (delta > 0) {
                scope.$apply(attrs.ngMouseWheelUp);

//                // for IE
//                event.returnValue = false;
//                // for Chrome and Firefox
//                if (event.preventDefault) {
//                    event.preventDefault();
//                }

            }
        });
    };
});


anglApp.directive('ngMouseWheelDown', function () {
    return function (scope, element, attrs) {
        element.bind("DOMMouseScroll mousewheel onmousewheel", function (event) {

            // cross-browser wheel delta
            var event = window.event || event; // old IE support
            var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));

            if (delta < 0) {
                scope.$apply(attrs.ngMouseWheelDown);

//                // for IE
//                event.returnValue = false;
//                // for Chrome and Firefox
//                if (event.preventDefault) {
//                    event.preventDefault();
//                }

            }
        });
    };
});