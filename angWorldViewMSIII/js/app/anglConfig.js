anglApp.config(function ($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: "sites/list.html",
        controller: "listController"
    }).when("/details/:videoId", {
        templateUrl: "sites/list.html",
        controller: "listController"
    }).when("/confirmError", {
        templateUrl: "sites/confirmError.html"
    }).when("/about", {
                templateUrl: "sites/about.html"
    }).when("/top100live", {
        templateUrl: "sites/topLive.html",
        controller: "topController"
    }).when("/mapview", {
        templateUrl: "sites/maps.html",
        controller: "mapController"
    }).when("/spaces", {
        templateUrl: "sites/spaces.html",
        controller: "spaceController"
    }).when("/spaces/:spaceId", {
        templateUrl: "sites/spacelist.html",
        controller: "spaceDetailsController"
    }).when("/signup", {
        templateUrl: "sites/signup.html",
        controller: "signUpController",
        resolve: {
            "check": function ($cookies, $location) {
                var userToken = $cookies.get('userToken');
                if (userToken) {
                    $location.path('/');    //redirect user to home.
                    alert("Be vagy jelentkezve");
                }
            }
        }
    }).when("/login", {
        templateUrl: "sites/login.html",
        controller: "loginController",
        resolve: {
            "check": function ($cookies, $location) {
                var userToken = $cookies.get('userToken');
                if (userToken) {
                    $location.path('/');    //redirect user to home.
                    alert("Már bejelentkeztél");
                }
            }
        }
    }).when("/profile", {
        templateUrl: "sites/profile.html",
        controller: "profileController",
        resolve: {
            "check": function ($cookies, $location) {
                var userToken = $cookies.get('userToken');
                if (!userToken) {
                    $location.path('/login');
                    alert("Nem vagy bejelentkezve");
                }
            }
        }
    }).otherwise({redirectTo: '/'});
});
anglApp.filter('trusted', ['$sce', function ($sce) {
        return function (url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);
anglApp.directive("otcScripts", function () {
    var updateScripts = function (element) {
        return function (scripts) {
            element.empty();
            angular.forEach(scripts, function (source, key) {
                var scriptTag = angular.element(
                        document.createElement("script"));
                source = "//@ sourceURL=" + key + "\n" + source;
                scriptTag.text(source)
                element.append(scriptTag);
            });
        };
    };
    return {
        restrict: "EA",
        scope: {
            scripts: "="
        },
        link: function (scope, element) {
            scope.$watch("scripts", updateScripts(element));
        }
    };
});

anglApp.directive('whenScrolledUp', function () {
    return function (scope, elm, attr) {
        var raw = elm[0];
        elm.bind('scroll', function () {
            if (raw.scrollTop == 0) {
                scope.$apply(attr.whenScrolledUp);
                raw.scrollTop = 1;
            }
        });
    };
});
anglApp.directive('whenScrolledDown', function () {
    return function (scope, elm, attr) {
        var raw = elm[0];
        elm.bind('scroll', function () {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolledDown);
            }
        });
    };
});