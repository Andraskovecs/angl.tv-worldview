anglApp.factory('UserFactory', function ($http) {
    var url = "http://213.181.192.210:8888/users";
    var UserFactory = {};
    UserFactory.login = function (user) {
        return $http.post(url + '/login', user);
    };
    UserFactory.signup = function (user) {
        return $http.post(url + '/signup', user);
    };
    UserFactory.logout = function (token) {
        return $http.get(url + '/logout/?token=' + token);
    };
    return UserFactory;
});
anglApp.factory('LogFactory', function ($http) {
    var url = "http://213.181.192.210:8888/log";
    var LogFactory = {};
    LogFactory.videoOpenLog = function (logObject) {
        $http.post(url + "/video_open", logObject).then(function (response) {
            console.log(response);
        }).catch(function (response) {
            console.log(response);
        });
    }
    LogFactory.startVideoLog = function (logObject) {
        return $http.post(url + "/start_video", logObject).then(function (response) {
            console.log(response);
        }).catch(function (response) {
            console.log(response);
        });
    }
    LogFactory.stopVideoLog = function (logObject, videoid) {
        logObject.videoid = videoid;
        return $http.post(url + "/stop_video", logObject).then(function (response) {
            console.log(response);
        }).catch(function (response) {
            console.log(response);
        });
    }
    LogFactory.commentLog = function (logObject) {
        return $http.post(url + "/comment", logObject).then(function (response) {
            console.log(response);
        }).catch(function (response) {
            console.log(response);
        });
    }
    LogFactory.echoLog = function (logObject) {
        return $http.post(url + "/echo", logObject).then(function (response) {
            console.log(response);
        }).catch(function (response) {
            console.log(response);
        });
    }
    LogFactory.disEchoLog = function (logObject) {
        return $http.post(url + "/echo_remove", logObject).then(function (response) {
            console.log(response);
        }).catch(function (response) {
            console.log(response);
        });
    }
    LogFactory.recommendedOpenLog = function (logObject) {
        return $http.post(url + "/recommended_open", logObject).then(function (response) {
            console.log(response);
        }).catch(function (response) {
            console.log(response);
        });
    }
    return LogFactory;
});
anglApp.factory('RestFactory', function ($http) {
    var RestFactory = {};
    var url = "http://213.181.192.210:8888/video";
    var ipurl = "//freegeoip.net/json/";
    //videoEvents    
    RestFactory.addHistory = function (historyReq) {
        return $http.post(url + '/add_history', historyReq);
    };
    RestFactory.addComment = function (comment) {
        return $http.post(url + '/add_comment', comment)
    }
    RestFactory.addEcho = function (echo) {
        return $http.post(url + '/echo', echo)
    }
    RestFactory.history = function (userIdOrIp) {
        return $http.get(url + '/history?userid=' + userIdOrIp);
    };
    RestFactory.comments = function (videoid) {
        return $http.get(url + '/comment?videoid=' + videoid)
    };
    RestFactory.recommended = function (videoid) {
        return $http.get(url + '/recommended?videoid=' + videoid);
    };
    RestFactory.popularity = function () {
        return $http.get(url + '/popularity');
    };
    RestFactory.videoSpaces = function () {
        return $http.get(url + '/spaces');
    };
    RestFactory.oneVideoSpaces = function (spaceId) {
        return $http.get(url + '/oneSpace?spaceId=' + spaceId);
    };
    RestFactory.details = function (videoId) {
        return $http.get(url + '/details/?videoId=' + videoId);
    };
    RestFactory.closeVideo = function (videoId) {
        return $http.get(url + '/closeVideo/?videoId=' + videoId);
    };
    RestFactory.list = function (after) {
        return $http.get(url + '/list?from=' + after);
    };
    RestFactory.listnew = function (after) {
        return $http.get(url + '/new_list?from=' + after);
    };
    RestFactory.map = function () {
        return $http.get(url + '/video');
    };
    RestFactory.statistic = function () {
        return $http.get(url + '/statistic');
    };

    //Utils
    RestFactory.getIp = function () {
        return $http.get(ipurl);
    }
    return RestFactory;
});
anglApp.factory('CommentFactory', function (RestFactory) {
    var CommentFactory = function () {
        this.busy = false;
        this.items = [];
    }
    CommentFactory.prototype.loadComments = function (videoId) {
        if (this.busy)
            return;
        this.busy = true;
        RestFactory.comments(videoId).then(function (response) {
            for (var i = 0; i < response.data.length; i++) {
                if (typeof this.items[i] === 'undefined') {
                    this.items.unshift(response.data[i]);
                }
            }
        }.bind(this));
        this.busy = false;
    };
    return CommentFactory;
});
anglApp.factory('ModalFactory', function ($modal, RestFactory, LogFactory, $cookies) {
    var ModalFactory = {};
    ModalFactory.showPlayer = function (videoid) {
        var modalInstance = $modal.open({
            templateUrl: 'sites/details.html',
            controller: "detailsController",
            resolve: {
                videoid: function () {
                    return videoid;
                }
            }
        });
        modalInstance.result.then(function () {
            console.log("egy");
        }, function () {
            RestFactory.closeVideo(videoid).then(function (response) {
                console.log(response.data);
            });
            var userToken = $cookies.getObject('userToken');
            LogFactory.stopVideoLog({user: userToken}, videoid);

        });
    };
    return ModalFactory;
});
anglApp.factory('AnglLoaderFactory', function (RestFactory) {
    var AnglLoaderFactory = function () {
        this.maxItemCount = 30;
        this.sleepTime = 3000;
        this.items = [];
        this.test = [];
        this.busy = false;
        this.new = 0;
        this.old = 0;
        this.after = new Date().getTime();
        this.before = 0;
        this.feed = false;
        this.stat = [];
    };

    AnglLoaderFactory.prototype.nextPage = function () {
        if (this.busy)
            return;
        this.busy = true;
        RestFactory.list(this.after).then(function (response) {
            console.log(response.data);
            for (var i = 0; i < response.data.length; i++) {
                this.items.push(response.data[i]);
                this.test.push(--this.old);
            }
            if (this.items.length > this.maxItemCount) {
                this.items.splice(0, (this.items.length - this.maxItemCount));
                this.test.splice(0, (this.test.length - this.maxItemCount));
                this.old = this.test[this.items.length - 1];
                this.new = this.test[0];
                this.after = this.items[this.items.length - 1].publishdate;
                this.before = this.items[0].publishdate;
            } else {
                this.after = (response.data[response.data.length - 1]?response.data[response.data.length-1].publishdate:this.after);
                this.before = (this.before == 0 ? (response.data[0]?response.data[0].publishdate:this.before) : this.before);
            }
            this.busy = false;
            console.log("after", this.after);
            console.log("before", this.before)
        }.bind(this));
    };
    AnglLoaderFactory.prototype.newPage = function () {
        if (this.busy)
            return;
        this.busy = true;
        this.before = (!this.before ? new Date().getTime() : this.before);
        RestFactory.listnew(this.before).then(function (response) {
            console.log("Data", response.data);
            for (var i = 0; i < response.data.length; i++) {
                this.items.unshift(response.data[i]);
                this.test.unshift(++this.new);
            }
            if (this.items.length > this.maxItemCount) {
                this.items.splice(this.maxItemCount, (this.items.length - this.maxItemCount));
                this.test.splice(this.maxItemCount, (this.test.length - this.maxItemCount));
                this.old = this.test[this.items.length - 1];
                this.new = this.test[0];
                this.after = this.items[this.items.length - 1].publishdate;
                this.before = this.items[0].publishdate;
            } else {
                this.before = (response.data.length > 0 ? response.data[response.data.length - 1].publishdate : this.before);
            }
            this.busy = false;
            console.log("after", this.after);
            console.log("before", this.before);
        }.bind(this));
    };
    AnglLoaderFactory.prototype.getFeed = function (feed) {
        var that = this;
        this.feed = feed;
        if (this.feed) {
            that.newPage();
            setTimeout(function () {
                console.log("FEED ON");
                that.getFeed(that.feed)
            }, that.sleepTime);
        } else {
            console.log("FEED OFF");
        }
    };
   

    return AnglLoaderFactory;
});
