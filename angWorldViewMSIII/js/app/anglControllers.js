/* global anglApp */
anglApp.controller("listController", function ($scope, $routeParams, $cookies, AnglLoaderFactory, ModalFactory,RestFactory) {
    var anglLoader = new AnglLoaderFactory();
    var userToken = $cookies.getObject('userToken');
    var feedData = {"on": {label: "auto feed off", feedValue: true}, "off": {label: "auto feed on", feedValue: false}};
    $scope.feed = (anglLoader.feed ? feedData.on : feedData.off);
    anglLoader.nextPage();
    anglLoader.getFeed($scope.feed.feedValue);
    $scope.videos = anglLoader;
    $scope.autoFeed = function (feed) {
        console.log(feed.feedValue);
        anglLoader.getFeed(!feed.feedValue);
        $scope.feed = (anglLoader.feed ? feedData.on : feedData.off);
    };
    $scope.nextPage = function () {
        if (!anglLoader.feed) {
            console.log("nextPage");
            anglLoader.nextPage();
        }
    };
    $scope.modifyWatchedValue = function (watched, localwatched) {

        var currentWatchNumber = watched + localwatched;

        if (currentWatchNumber >= 1000000) {
            return Math.ceil(currentWatchNumber / 100000) + "M+";
        } else if (currentWatchNumber > 999) {
            return Math.round(currentWatchNumber / 1000) + "K";
        }
        return currentWatchNumber;
    };
    $scope.modifyValue = function (valueNum) {

        if (valueNum >= 1000000) {
            return Math.ceil(valueNum / 100000) + "M+";
        } else if (valueNum > 999) {
            return Math.round(valueNum / 1000) + "K";
        }
        return valueNum;
    };
    $scope.newPage = function () {
        if (!anglLoader.feed) {
            console.log("newPage");
            anglLoader.newPage();
        }
    };
    $scope.stopFeed = function () {
        if (anglLoader.feed) {
            console.log("STOP FEED")
            anglLoader.getFeed(false);
            $scope.feed = (anglLoader.feed ? feedData.on : feedData.off);
        }
    };
    $scope.showPlayer = function (videoId) {
        $scope.stopFeed();
        if (userToken) {
            ModalFactory.showPlayer(videoId);
        } else {
            alert("Please log in or register!");
        }
    };
    if ($routeParams.videoId) {
        $scope.stopFeed();
        if (userToken) {
            ModalFactory.showPlayer($routeParams.videoId);
        } else {
            alert("Please log in or register!");
        }
    }
    
       RestFactory.statistic().then(function (response) {
                $scope.stats = response.data;
                
                console.log($scope.stats);
            });
  
});
anglApp.controller("topController", function ($scope, $routeParams, RestFactory, ModalFactory) {
    $scope.busy = true;
    RestFactory.popularity().then(function (response) {
        $scope.videos = response.data;
        $scope.busy = false;
    });
    $scope.showPlayer = function (videoId) {
        ModalFactory.showPlayer(videoId);
    };
    $scope.modifyWatchedValue = function (watched, localwatched) {

        var currentWatchNumber = watched + localwatched;

        if (currentWatchNumber >= 1000000) {
            return Math.ceil(currentWatchNumber / 100000) + "M+";
        } else if (currentWatchNumber > 999) {
            return Math.round(currentWatchNumber / 1000) + "K";
        }
        return currentWatchNumber;
    };
    $scope.modifyValue = function (valueNum) {

        if (valueNum >= 1000000) {
            return Math.ceil(valueNum / 100000) + "M+";
        } else if (valueNum > 999) {
            return Math.round(valueNum / 1000) + "K";
        }
        return valueNum;
    };
    if ($routeParams.videoId) {
        ModalFactory.showPlayer($routeParams.videoId);
    }

});
anglApp.controller("spaceController", function ($scope, $routeParams, RestFactory, ModalFactory) {
    $scope.json = [];
    $scope.busy = true;
    console.log($routeParams.spaceId);
    RestFactory.videoSpaces().then(function (response) {
        $scope.spaces = response.data;

        console.log(response.data);
        $scope.busy = false;
    });


});

anglApp.controller("spaceDetailsController", function ($scope, $routeParams, RestFactory, ModalFactory) {
    $scope.json = [];
    $scope.busy = true;
    $scope.spaceId = $routeParams.spaceId;
    
    RestFactory.oneVideoSpaces($scope.spaceId) .then(function (response) {
        $scope.spaces = response.data;
        $scope.city =response.data.city;
        $scope.videos = response.data.videoliList;
     
        console.log($scope.city);
        if (!$scope.city) {
            alert("This space is not available anymore");
            var landingUrl ="#!/spaces/";
            window.location.href = landingUrl;
        }
        $scope.busy = false;
    });
    $scope.showPlayer = function (videoId) {
        ModalFactory.showPlayer(videoId);
    };
    $scope.modifyWatchedValue = function (watched, localwatched) {

        var currentWatchNumber = watched + localwatched;

        if (currentWatchNumber >= 1000000) {
            return Math.ceil(currentWatchNumber / 100000) + "M+";
        } else if (currentWatchNumber > 999) {
            return Math.round(currentWatchNumber / 1000) + "K";
        }
        return currentWatchNumber;
    };
    $scope.modifyValue = function (valueNum) {

        if (valueNum >= 1000000) {
            return Math.ceil(valueNum / 100000) + "M+";
        } else if (valueNum > 999) {
            return Math.round(valueNum / 1000) + "K";
        }
        return valueNum;
    };



});
anglApp.controller("mapController", function ($scope, RestFactory, ModalFactory) {
    $scope.json = [];
    $scope.busy = true;
    RestFactory.map().then(function (response) {
        $scope.json = response.data;
        initialize();
        console.log(response.data);
        $scope.busy = false;
    });
    $scope.showPlayer = function (videoId) {
        ModalFactory.showPlayer(videoId);
    };
});
anglApp.controller("detailsController", function ($scope, $cookies, RestFactory, videoid, $rootScope, CommentFactory, LogFactory) {
    var userToken = $cookies.getObject('userToken');
    var cf = new CommentFactory;
    $scope.userToken = userToken;
    $scope.commentFactory = cf;
    $scope.recommendedVideos = [];
    $scope.busy = true;
    RestFactory.details(videoid).then(function (response) {
        loadVideo(response);
        sendHistory(RestFactory, response.data, userToken, $rootScope, LogFactory);
        $scope.busy = false;
    });
    $scope.openVideo = function (video) {
        loadVideo({data: video});
        sendHistory(RestFactory, video, userToken, $rootScope, LogFactory);
    };
    $rootScope.$on("comment-done", function () {
        cf.loadComments($scope.video.videoid);
        $scope.video.commentCount = cf.items.length;
    });
    $rootScope.$on("echo-done", function () {
        $scope.video.echoCount++;
    });
    $rootScope.$on("echo-fail", function () {
        $scope.video.echoCount--;
    });
    var loadVideo = function (response) {
        $scope.scripts = [];
        $scope.video = response.data;
        $scope.video.comments = [];
        $scope.live = (response.data.isLive == true ? 'Élő' : 'Nem élő');
        $scope.videoPlayerId = "video" + response.data.videoid;


        $scope.scripts[0] = 'var player' + response.data.videoid + ' = videojs("video' + response.data.videoid + '");' +
                "player" + response.data.videoid + ".addClass('vjs-matrix');" +
                'player' + response.data.videoid + '.ready(function() {player' + response.data.videoid + '.src({ src: "' + response.data.defaultstreamurl + '",' + 'type: "' + response.data.videoType + '"});player' + response.data.videoid + '.play();});';
        cf.loadComments($scope.video.videoid);
        $scope.video.comments = cf.items;
        if (!$scope.video.location) {
            $scope.video.location = "unknown";
        }
        RestFactory.recommended($scope.video.videoid).then(function (response) {
            $scope.recommendedVideos = response.data;
        });
    }
});
anglApp.controller("echoController", function ($scope, RestFactory, $cookies, $rootScope, LogFactory) {
    $scope.echos = [
        {type: "like", img: "images/Echo_Icon.png"}
    ];
    var userToken = $cookies.getObject('userToken');
    $scope.addEcho = function (video, echo) {
        if (userToken) {
            var echoObj = {
                user: userToken,
                video: video,
                echotype: echo.type
            };
            var logObj = {user: userToken, video: video};
            RestFactory.addEcho(echoObj).then(function (ncresponse) {
                if (ncresponse.data.message == "Ok") {
                    $rootScope.$broadcast("echo-done");
                    LogFactory.echoLog(logObj);
                } else if (ncresponse.data.message == "NotOk") {
                    $rootScope.$broadcast("echo-fail");
                    LogFactory.disEchoLog(logObj);
                }
            });
        }
    };
});
anglApp.controller("commentController", function ($scope, RestFactory, $cookies, $rootScope, LogFactory) {
    var userToken = $cookies.getObject('userToken');
    $scope.addComment = function (video) {
        if (userToken) {
            var commentobj = {
                user: userToken,
                video: video,
                text: $scope.textareaData,
                time: new Date()
            };
            var logObj = {user: userToken, video: video};
            RestFactory.addComment(commentobj).then(function (ncresponse) {
                if (ncresponse.data.message == "Ok") {
                    $scope.textareaData = "";
                    LogFactory.commentLog(logObj);
                    $rootScope.$broadcast("comment-done");
                }
            });
        }
    };
});
anglApp.controller("historyController", function ($scope, RestFactory, $cookies, $rootScope, ModalFactory) {
    var userToken = $cookies.getObject('userToken');
    historyLoad($scope, RestFactory, userToken);
    $rootScope.$on("add-history-done", function () {
        historyLoad($scope, RestFactory, userToken);
    });
    $scope.showPlayer = function (videoId) {
        if (userToken) {
            ModalFactory.showPlayer(videoId);
        } else {
            alert("Please log in or register!");
        }
    };
});
anglApp.controller("menuController", function ($scope, $cookies, $location, $route, $rootScope, UserFactory) {
    $scope.userToken = $cookies.getObject('userToken');
    $rootScope.$on("login-done", function () {
        $scope.userToken = $cookies.getObject('userToken');
    });
    $rootScope.$on("logout-done", function () {
        $scope.userToken = $cookies.getObject('userToken');
    });
    $scope.logout = function () {
        UserFactory.logout($cookies.getObject('userToken').token).then(function (response) {
            $cookies.remove("userToken");
            $rootScope.$broadcast("logout-done");
            $location.path("/");
            $route.reload();
        });
    };
});
anglApp.controller("profileController", function ($cookies, $scope) {
    var userToken = $cookies.getObject('userToken');
    $scope.name = userToken.displayname;
    $scope.email = userToken.email;
});
anglApp.controller("signUpController", function ($scope, $location, UserFactory) {
    $scope.fasza = false;
    $scope.signUpAsyncAsJSON = function () {        
        $scope.fasza = true;
        var user = {
            loginname: $scope.loginname,
            password: $scope.password,
            displayname: $scope.displayname,
            email: $scope.email
        };
        UserFactory.signup(user).then(function (response) {
            if (response.data = "ok") {
                $scope.fasza = false;
                alert("Sucessfull registration. Check your mailbox!");
                $location.path("/login");                
            }
        }).catch(function (response) {
            $scope.fasza = false;
            alert(response.data.message);
            console.log(response);
        });
    };
});
anglApp.controller("loginController", function ($scope, $cookies, $location, $rootScope, UserFactory) {
    $scope.loginAsyncAsJSON = function () {
        var user = {
            loginname: $scope.loginname,
            password: $scope.password
        };
        UserFactory.login(user).then(function (response) {
            var expireDate = new Date(response.data.tokentime);
            $cookies.putObject('userToken', response.data, {'expires': expireDate});
            $rootScope.$broadcast("login-done");
            $location.path("/");
        }).catch(function (response) {
            alert("Wrong username or password! Please, try a again!");
            console.log(response);
        });
    };
});
function historyLoad($scope, RestFactory, userToken) {
    if (userToken) {
        RestFactory.history(userToken.id).then(function (response) {
            $scope.videos = response.data;
        });
    } else {
        RestFactory.getIp().then(function (response) {
            var ip = response.data.ip;//.replace(/\./g , "-");
            RestFactory.history(ip).then(function (vresponse) {
                $scope.videos = vresponse.data;
            });
        });
    }
}
function sendHistory(RestFactory, video, userToken, $rootScope, LogFactory) {
    var hr = {video: video, ipAddress: ""};
    RestFactory.getIp().then(function (ipresponse) {
        hr.ipAddress = ipresponse.data.ip;
        if (userToken) {
            hr.user = userToken;
        }
        RestFactory.addHistory(hr).then(function (hresponse) {
            if (hresponse.data.message == "Ok") {
                LogFactory.videoOpenLog(hr);
                LogFactory.startVideoLog(hr);
                $rootScope.$broadcast("add-history-done");
            }
        });
    });
}
