<?php
$get = filter_input(INPUT_GET, "menu",FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET, "id",FILTER_SANITIZE_STRING);
if($get == "list"){
  if(file_exists("listData.json")){
    echo file_get_contents("listData.json");
  }else{
    echo "{\"status\":\"fail\"}";
  }
}else if($id && $get == "video"){
  if(file_exists($id.".json")){
    echo file_get_contents($id.".json");
  }else{
    echo "{\"status\":\"fail\"}";
  }
}else{
  echo "{\"status\":\"fail\"}";
}