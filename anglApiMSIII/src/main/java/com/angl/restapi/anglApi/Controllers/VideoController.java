
package com.angl.restapi.anglApi.Controllers;

import com.angl.angldao.aggregation.AggregationObject;
import com.angl.angldao.aggregation.AggregationObjectFactory;
import com.angl.angldao.aggregation.PopularityAggregation;
import com.angl.angldao.aggregation.RecommendedAggregation;
import com.angl.angldao.aggregation.SpaceAggregation;
import com.angl.angldao.aggregation.SpaceResult;
import com.angl.angldao.dao.AggregationDAO;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.main.VideoStatistic;
import com.angl.angldao.models.Basicaggregation;
import com.angl.angldao.models.Comment;
import com.angl.angldao.models.Echo;
import com.angl.angldao.models.Userlog;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videohistory;
import com.angl.restapi.anglApi.Common.CommentRequest;
import com.angl.restapi.anglApi.Common.EchoRequest;
import com.angl.restapi.anglApi.Common.HistoryRequest;
import com.angl.restapi.anglApi.Common.InetAddressUtils;
import com.angl.restapi.anglApi.Common.ResponseMessage;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/video")
@CrossOrigin(origins = "*")
public class VideoController {   
    @RequestMapping(value="/add_history", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> addHistory(@RequestBody HistoryRequest hr){
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try{ 
            Videohistory videoHistory = null;
            if(hr.getUser() != null){
                videoHistory = new Videohistory(hr.getUser(), hr.getVideo(), new Date());
            }else{
                videoHistory = new Videohistory(hr.getVideo(), hr.getIpAddress(), new Date());
            }            
            dao.openSession();
            if(!((VideoDAO)dao).isExistsHistory(videoHistory)){
                dao.saveEntity(videoHistory);
            }
            return new ResponseEntity<>(new ResponseMessage("Ok"), HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }finally{
            dao.closeSession();
        }   
    }
    @RequestMapping(value="/add_comment", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> addComment(@RequestBody CommentRequest comment){
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        System.out.println(comment);
        try{
            Comment c = new Comment(comment.getUser(), comment.getVideo(), comment.getText(), comment.getTime());
            dao.openSession();
            dao.saveEntity(c);
            return new ResponseEntity<>(new ResponseMessage("Ok"), HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }finally{
            dao.closeSession();
        } 
    }
    @RequestMapping(value="/echo", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> addEcho(@RequestBody EchoRequest echo){
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        System.out.println(echo);
        try{
            dao.openSession();
            if(echo.getEchotype() != null && echo.getUser() != null && echo.getVideo() != null){
                Boolean isOk = ((VideoDAO)dao).saveEcho(new Echo(echo.getUser(), echo.getVideo(), echo.getEchotype()));
                if(!isOk){
                    return new ResponseEntity<>(new ResponseMessage("NotOk"), HttpStatus.OK);
                }
            }
            return new ResponseEntity<>(new ResponseMessage("Ok"), HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }finally{
            dao.closeSession();
        } 
    }
    @RequestMapping(value="/comment", method=RequestMethod.GET)
    public ResponseEntity<List<Comment>> getCommentByVideoId(@RequestParam("videoid") String videoId){
        List<Comment> comments = new ArrayList<>();
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try{
            dao.openSession();
            comments = ((VideoDAO)dao).getCommentsByVideoId(videoId);
            comments.forEach((comment)->{
                comment.setUserName(comment.getUser().getDisplayname());                
            });
            return new ResponseEntity(comments, HttpStatus.OK);
        }catch(Exception ex){
            ex.printStackTrace();
            return new ResponseEntity(new ArrayList<>(), HttpStatus.OK);
        }finally{
            dao.closeSession();
        }
    }
    @RequestMapping(value="/history", method=RequestMethod.GET)
    public ResponseEntity<List<Video>> getHistoryByUserId(@RequestParam("userid") String userIdOrIp){
        List<Video> result = new ArrayList<>();
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try{
            dao.openSession();
            String ipOrId = (InetAddressUtils.isInetAdderss(userIdOrIp)?VideoDAO.IP:VideoDAO.USER);
            result = ((VideoDAO)dao).getVideoHistoryByUser(userIdOrIp, ipOrId);
            result.forEach((v)->{
                v.setCommentCount(v.getComments().size());
                v.setEchoCount(v.getEchos().size());
            });
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            dao.closeSession();
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }    
    @RequestMapping(value="/details", method=RequestMethod.GET)
    public ResponseEntity<Video> getOneVideById(@RequestParam("videoId") String videoId){
        Video video = new Video();
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try{
            dao.openSession(); 
            video = ((VideoDAO)dao).getOneByVideoId(videoId);
            video.setLocalwatched((video.getLocalwatched()+1));
            dao.saveEntity(video);
            video.setCommentCount(video.getComments().size());
            video.setEchoCount(video.getEchos().size());
            if (video.getProvidertype().equals("periscope")) {
                video.setDefaultstreamurl("http://104.237.4.64:8090/proxy/" + video.getVideoid() + "/playlist.m3u8");
                video.setDefaultthumbnailurl("http://104.237.4.64:8090/proxy/" + video.getVideoid() + "/thumbnail");
            }   
        }catch(Exception e){
            e.printStackTrace();
        }finally{
           dao.closeSession();
        }
        return new ResponseEntity<>(video, HttpStatus.OK);
    }
    @RequestMapping(value="/closeVideo", method=RequestMethod.GET)
    public ResponseEntity<ResponseMessage> closeVideo(@RequestParam("videoId") String videoId){
        Video video = new Video();
        ResponseMessage rm = new ResponseMessage("Fail");
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try{
            dao.openSession(); 
            video = ((VideoDAO)dao).getOneByVideoId(videoId);
            video.setLocalwatched((video.getLocalwatched()-1));
            dao.saveEntity(video);
            rm = new ResponseMessage("Ok");
        }catch(Exception e){
            e.printStackTrace();
        }finally{
           dao.closeSession();
        }
        return new ResponseEntity<>(rm, HttpStatus.OK);
    }
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ResponseEntity<List<Video>> getVideoListByLimit(@RequestParam("from") String from){
        List<Video> videos = new ArrayList<>();
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try {            
            Long time = Long.parseLong(from);
            Date date = new Date(time);
            dao.openSession(); 
            videos = ((VideoDAO)dao).getVideoListByLimit(date, 15); 
            videos.forEach((v)->{
                v.setCommentCount(v.getComments().size());
                v.setEchoCount(v.getEchos().size());
                if (v.getProvidertype().equals("periscope")) {
                    v.setDefaultstreamurl("http://104.237.4.64:8090/proxy/" + v.getVideoid() + "/playlist.m3u8");
                    v.setDefaultthumbnailurl("http://104.237.4.64:8090/proxy/" + v.getVideoid() + "/thumbnail");
                } 
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally{
           dao.closeSession();
        }
        return new ResponseEntity<>(videos,  HttpStatus.OK);
    }
    @RequestMapping(value="/new_list", method=RequestMethod.GET)
    public ResponseEntity<List<Video>> getNewVideoListByLimit(@RequestParam("from") String from){
        List<Video> videos = new ArrayList<>();
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.mmm");
            Long time = Long.parseLong(from);
            Date date = new Date(time);
            dao.openSession(); 
            videos = ((VideoDAO)dao).getNewVideoListByLimit(date, 15);
            videos.forEach((v)->{
                v.setCommentCount(v.getComments().size());
                v.setEchoCount(v.getEchos().size());
                if (v.getProvidertype().equals("periscope")) {
                    v.setDefaultstreamurl("http://104.237.4.64:8090/proxy/" + v.getVideoid() + "/playlist.m3u8");
                    v.setDefaultthumbnailurl("http://104.237.4.64:8090/proxy/" + v.getVideoid() + "/thumbnail");
                } 
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally{
           dao.closeSession();
        }
        return new ResponseEntity<>(videos,  HttpStatus.OK);
    }
    @RequestMapping(value="/video", method=RequestMethod.GET)
    public ResponseEntity<List<Video>> getVideoList(){
        List<Video> videos = new ArrayList<>();
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try {
            dao.openSession(); 
            videos = ((VideoDAO)dao).getAllisAlived();
            videos.forEach((v)->{
                v.setCommentCount(v.getComments().size());
                v.setEchoCount(v.getEchos().size());
                if (v.getProvidertype().equals("periscope")) {
                    v.setDefaultstreamurl("http://104.237.4.64:8090/proxy/" + v.getVideoid() + "/playlist.m3u8");
                    v.setDefaultthumbnailurl("http://104.237.4.64:8090/proxy/" + v.getVideoid() + "/thumbnail");
                } 
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally{
           dao.closeSession();
        }
        return new ResponseEntity<>(videos,  HttpStatus.OK);
    }
    @RequestMapping(value="/popularity", method=RequestMethod.GET)
    public ResponseEntity<List<Video>> getPopularityVideoList(){
        List<Video> videos = new ArrayList<>();                
        try{
            AggregationObject ao = new AggregationObjectFactory(new Date()).getAggregationObject(PopularityAggregation.class);
            videos = ao.getAllVideo(100);        
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return new ResponseEntity<>(videos,  HttpStatus.OK);
    }
    @RequestMapping(value="/spaces", method=RequestMethod.GET)
    public ResponseEntity<List<SpaceResult>> getVideoSpaces(){
        List<SpaceResult> spaceList = new ArrayList<>();      
        try{
            AggregationObject ao = new AggregationObjectFactory().getAggregationObject(SpaceAggregation.class);
            spaceList = ao.getSpaces();            
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(spaceList, HttpStatus.OK);
    }
    @RequestMapping(value="/oneSpace", method=RequestMethod.GET)
    public ResponseEntity<SpaceResult> getOneVideoSpace(@RequestParam("spaceId") String spaceId){
             
        try{
            AggregationObject ao = new AggregationObjectFactory().getAggregationObject(SpaceAggregation.class);
           return new ResponseEntity<>(ao.getSpaceVideos(spaceId), HttpStatus.OK);
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }
    @RequestMapping(value="/recommended", method=RequestMethod.GET)
    public ResponseEntity<List<Video>> getRecommendedVideoList(@RequestParam("videoid") String videoid){
        List<Video> videos = new ArrayList<>();
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try{
            dao.openSession();
            Video video = ((VideoDAO)dao).getOneByVideoId(videoid);
            AggregationObject ao = new AggregationObjectFactory().getAggregationObject(RecommendedAggregation.class);
            videos = ao.getAllVideo(3,video);           
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            dao.closeSession();
        }
        return new ResponseEntity<>(videos,  HttpStatus.OK);
    }
    @RequestMapping(value="/statistic", method=RequestMethod.GET)
    public ResponseEntity<List<VideoStatistic>> getStatistic(){
        List<VideoStatistic> videos = new ArrayList<>();
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try{
            dao.openSession();
            videos = ((VideoDAO)dao).getStatisticByDb();
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            dao.closeSession();
        }
        return new ResponseEntity<>(videos,  HttpStatus.OK);
    }
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(){
        return "Hello";
    }
   
}
