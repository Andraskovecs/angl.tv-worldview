package com.angl.restapi.anglApi.Controllers;

import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.UserDAO;
import com.angl.angldao.models.Role;
import com.angl.angldao.models.User;
import com.angl.restapi.anglApi.Common.MailHandler;
import com.angl.restapi.anglApi.Common.ResponseMessage;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*")
public class UserController {

    private final MailHandler mailHandler;
    @Autowired
    JavaMailSender mailSender;

    @Autowired
    public UserController(TemplateEngine templateEngine) {
        mailHandler = new MailHandler(templateEngine);
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<User> login(@RequestBody User user) {
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(UserDAO.class);
        try {
            dao.openSession();
            String pwd = DigestUtils.sha1Hex(user.getPassword());
            User userlogin = ((UserDAO) dao).getUserByLogin(user.getLoginname(), pwd);
            if (userlogin == null) {
                throw new Exception("nincs a megadott adatokkal felhasználó");
            }
            String token = ((UserDAO) dao).getToken();
            userlogin.setToken(token);
            userlogin.setTokentime(new Date(System.currentTimeMillis() + 3600 * 1000));
            dao.saveEntity(userlogin);
            userlogin.setPassword("");
            userlogin.setLoginname("");
            ResponseEntity response = new ResponseEntity<>(userlogin, HttpStatus.OK);
            dao.closeSession();
            return response;
        } catch (Exception ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        } finally {
        }
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<ResponseMessage> signUp(@RequestBody User user) {
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(UserDAO.class);
        System.out.println(user);
        try {
            dao.openSession();
            Boolean userExist = ((UserDAO) dao).isExistsUser(user);
            System.out.println("Result: "+ userExist);
            if(!userExist){
                String token = ((UserDAO) dao).getToken();
                String pwd = DigestUtils.sha1Hex(user.getPassword());
                Role role = ((UserDAO) dao).getRoleByName("user");
                String guid = ((UserDAO) dao).getUniqueGuid();
                user.setPassword(pwd);
                user.setToken(token);
                user.setTokentime(new Date(System.currentTimeMillis() + 3600 * 1000));
                user.setRole(role);
                user.setGuid(guid);
                user.setEmailconfirmed(guid);
                this.sendMail(user.getEmail(), user.getEmailconfirmed());
                dao.saveEntity(user);
                return new ResponseEntity<>(new ResponseMessage("Ok"), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new ResponseMessage("This username or email is existing!"), HttpStatus.BAD_REQUEST);  
            }
        } catch (Exception ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(new ResponseMessage("Some error occured during registration, please try again!"), HttpStatus.FORBIDDEN);
        } finally {
            dao.closeSession();
        }
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public String emailConfirm(@RequestParam("token") String token) {
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(UserDAO.class);
        System.out.println("email token: " + token);
        try {
            dao.openSession();
            User user = ((UserDAO) dao).getOneByMailConfirm(token);
            if (user != null) {
                user.setEmailconfirmed("1");
                dao.saveEntity(user);
            } else {
                throw new Exception("Nem létezik a felhasználó ilyen tokennel!");
            }
            String response =String.format(UserController.redirectSring, redirectUrl);
            return response;
        } catch (Exception ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            String response =String.format(UserController.redirectSring, errorRedirect);
            return response;
        } finally {
            dao.closeSession();
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseEntity<ResponseMessage> logout(@RequestParam("token") String token) {
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(UserDAO.class);
        try {
            dao.openSession();
            User user = ((UserDAO) dao).getOneByToken(token);
            if (user != null) {
                user.setToken("0");
                user.setTokentime(new Date());
                dao.saveEntity(user);
            } else {
                throw new Exception("Nem létezik a felhasználó ilyen tokennel!");
            }
            return new ResponseEntity<>(new ResponseMessage("Ok"), HttpStatus.OK);
        } catch (Exception ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.OK);
        } finally {
            dao.closeSession();
        }
    }

    private Boolean sendMail(String email, String mailToken) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("info@angl.tv");
            messageHelper.setTo(email);
            messageHelper.setSubject("Emailcím megerősítése");
            String content = mailHandler.build("Emailcím megerősítése", serverUrl+"users/confirm/?token=" + mailToken);
            messageHelper.setText(content, true);
        };
        try {
            mailSender.send(messagePreparator);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    @Value("${angl.redirectUrl}")
    private String redirectUrl;
    @Value("${angl.errorRedirect}")
    private String errorRedirect;
    @Value("${angl.serverUrl}")
    private String serverUrl;
    private static final String redirectSring = "<html><head>\n" +
        "<script type=\"text/javascript\">\n" +
        "function redirect(){\n" +
        "	window.location.replace(\"%s\");\n" +
        "}\n" +
        "</script>\n" +
        "</head>\n" +
        "<body onload=\"redirect()\">\n" +
        "</body>\n" +
        "</html>";
}
