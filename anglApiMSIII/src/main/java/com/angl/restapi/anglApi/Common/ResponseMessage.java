package com.angl.restapi.anglApi.Common;

import lombok.Value;

@Value
public class ResponseMessage {
    private String message;
}
