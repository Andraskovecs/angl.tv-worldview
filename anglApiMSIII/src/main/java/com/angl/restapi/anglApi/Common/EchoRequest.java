/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.restapi.anglApi.Common;
import com.angl.angldao.models.User;
import com.angl.angldao.models.Video;
import lombok.Getter;
import lombok.Setter;

public class EchoRequest {
    private @Getter @Setter User user;
    private @Getter @Setter Video video;
    private @Getter @Setter String echotype;

    @Override
    public String toString() {
        return "EchoRequest{" + "user=" + user + ", video=" + video + ", echotype=" + echotype + '}';
    }
    
}