package com.angl.restapi.anglApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnglApplication {

    public static void main(String args[]) {
        SpringApplication.run(AnglApplication.class, args);
    }

}
