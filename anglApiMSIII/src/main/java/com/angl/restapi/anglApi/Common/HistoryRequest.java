/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.restapi.anglApi.Common;

import com.angl.angldao.models.User;
import com.angl.angldao.models.Video;
import lombok.Getter;
import lombok.Setter;

public class HistoryRequest {
    private @Getter @Setter User user;
    private @Getter @Setter String ipAddress, videoid;
    private @Getter @Setter Video video;

    public HistoryRequest() {
    }

    @Override
    public String toString() {
        return "HistoryRequest{" + "user=" + user + ", video=" + video + ", ipAddress=" + ipAddress + '}';
    }
    
}
