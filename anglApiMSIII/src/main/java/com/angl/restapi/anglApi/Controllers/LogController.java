
package com.angl.restapi.anglApi.Controllers;

import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.enums.Logtype;
import com.angl.angldao.models.Userlog;
import com.angl.angldao.models.Video;
import com.angl.restapi.anglApi.Common.HistoryRequest;
import com.angl.restapi.anglApi.Common.ResponseMessage;
import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/log")
@CrossOrigin(origins = "*")
public class LogController {
    
    @RequestMapping(value="/start_video", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> startVideo(@RequestBody HistoryRequest hr){
        ResponseMessage response = sendLog(hr, Logtype.videoStart);
        if(response == null){
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @RequestMapping(value="/stop_video", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> stopVideo(@RequestBody HistoryRequest hr){
        System.out.println(hr.getVideoid());
        System.out.println(hr);
        if(!hr.getVideoid().isEmpty()){
            DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
            dao.openSession(); 
            Video video = ((VideoDAO)dao).getOneByVideoId(hr.getVideoid());
            dao.closeSession();
            hr.setVideo(video);
        }
        ResponseMessage response = sendLog(hr, Logtype.videoStop);
        if(response == null){
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @RequestMapping(value="/recommended_open", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> recommendedOpen(@RequestBody HistoryRequest hr){
        ResponseMessage response = sendLog(hr, Logtype.recommendedOpen);
        if(response == null){
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @RequestMapping(value="/video_open", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> openVideo(@RequestBody HistoryRequest hr){
        ResponseMessage response = sendLog(hr, Logtype.videoOpen);
        if(response == null){
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @RequestMapping(value="/comment", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> commentVideo(@RequestBody HistoryRequest hr){
        ResponseMessage response = sendLog(hr, Logtype.comment);
        if(response == null){
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @RequestMapping(value="/echo", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> echoVideo(@RequestBody HistoryRequest hr){
        ResponseMessage response = sendLog(hr, Logtype.echo);
        if(response == null){
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @RequestMapping(value="/echo_remove", method=RequestMethod.POST)
    public ResponseEntity<ResponseMessage> disEcho(@RequestBody HistoryRequest hr){
        ResponseMessage response = sendLog(hr, Logtype.echoRemove);
        if(response == null){
            return new ResponseEntity<>(new ResponseMessage("Fail"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    private ResponseMessage sendLog(HistoryRequest hr, Logtype logtype){
        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
        try{             
            Userlog log = null;
            if(hr.getUser() != null){
                log = new Userlog(hr.getUser(), hr.getVideo(), logtype, new Date());
            }else{
                log = new Userlog(hr.getVideo(), hr.getIpAddress(), logtype, new Date());
            }            
            dao.openSession();
            dao.saveEntity(log);
            return new ResponseMessage("Ok");
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }finally{
            dao.closeSession();
        }   
    }
}
