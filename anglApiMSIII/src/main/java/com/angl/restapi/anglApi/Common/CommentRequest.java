/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.restapi.anglApi.Common;

import com.angl.angldao.models.User;
import com.angl.angldao.models.Video;
import java.util.Date;
import lombok.Value;

@Value
public class CommentRequest {
    private User user;
    private Video video;
    private String text;
    private Date time;
}
