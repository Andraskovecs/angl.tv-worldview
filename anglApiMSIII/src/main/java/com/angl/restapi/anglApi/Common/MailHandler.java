/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.restapi.anglApi.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author hans
 */
public class MailHandler {
    
    private TemplateEngine templateEngine;
   
    public MailHandler(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }
    public String build(String message, String link) {
        Context context = new Context();
        context.setVariable("message", message);
        context.setVariable("confirmLink", link);
        return templateEngine.process("mailTemplate", context);
    }
}
