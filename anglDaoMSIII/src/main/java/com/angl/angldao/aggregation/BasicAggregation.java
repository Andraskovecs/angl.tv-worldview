/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.aggregation;

import com.angl.angldao.models.Video;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Kanyó Krisztián
 */
public class BasicAggregation extends CommonAggregation{
    private List<Integer> selectedElements = new ArrayList<Integer>();
    
    public BasicAggregation() {
        super();
        this.similarityFileName = "basicSimilarityMatrix.csv";
    }
    
    public BasicAggregation(int k, int n) {
        super(k, n);
        this.similarityFileName = "basicSimilarityMatrix.csv";
    }
    
    @Override
    protected void fillAttributeList(){
        this.attributes.add(new AggregationAttribute(0.7, "vector", Arrays.asList("latitude", "longitude")));
        this.attributes.add(new AggregationAttribute(0.3, "boolean", Arrays.asList("username")));
    }
    
    @Override
    protected void structureResult() {
        this.searchForSelectedVideo();
        this.fillWithPopularVideo();
    }
    
    private void searchForSelectedVideo(){
        if(this.returnSize < 0 || this.selectedVideo == null) {
            System.out.println("Nem létező videót adott meg, vagy negatív listaszámmal kér le aggregáció eredményt");
            return;
        }
        List<Video> aggregationList = new ArrayList<>();
        outerloop:
        for (List<Video> list : this.result) {
            for(int i = 0; i <list.size(); i++){
                if(this.selectedVideo.getId().equals(list.get(i).getId())){
                    list.remove(i);
                    aggregationList = list;
                    System.out.println("Videó sikeresen megtalálva!");
                    break outerloop;
                }
            }
            System.out.println("Videót nem sikerült megtalálni ebben a listában. Lista elemszáma: "+list.size());
        }
        if(aggregationList.size() > 0 && aggregationList.size() < this.returnSize){
            this.structuredResult = aggregationList;
        }
        else if(aggregationList.size() > 0 && aggregationList.size() >= this.returnSize){
            Random rand = new Random();
            for(int i = 0; i < this.returnSize; i++){
                int index = rand.nextInt(aggregationList.size());
                this.structuredResult.add(aggregationList.remove(index));
            }
        }
    }
    
    private void fillWithPopularVideo(){
        if(this.structuredResult.size() >= this.returnSize) return;
        int freeSpaces = this.returnSize - this.structuredResult.size();
        System.out.println("Nincs megfelelő számú videó a listában, így a népszerű videókkal lesz feltöltve.");
        System.out.println("Jelenlegi videószám: "+this.structuredResult.size()+". Hiányzó elemszám: "+freeSpaces);
        AggregationObject ao = new AggregationObjectFactory().getAggregationObject(PopularityAggregation.class);
        this.structuredResult.addAll(ao.getAllVideo(freeSpaces));
        
    }
    
    @Override
    protected void kMedoid(){
        this.calculateSimilarity();
        this.getMostSimilarElements();
        this.getMostDifferentElements();
        this.aggregateRestVideos();
    }

    private void getMostSimilarElements() {
        int element1Index = 0, element2Index = 0;
        double maxSimilarity = 0.0;
        for (int i = 0; i < this.videoList.size(); i++) {
            for (int j = 0; j < this.videoList.size(); j++) {
                if(this.similarityMatrix[i][j] > maxSimilarity){
                    maxSimilarity = this.similarityMatrix[i][j];
                    element1Index = i;
                    element2Index = j;
                }
            }
        }
        
        this.result.add(this.result.size(), new ArrayList<Video>());
        this.result.get(this.result.size()-1).add(this.videoList.get(element1Index));
        this.result.get(this.result.size()-1).add(this.videoList.get(element2Index));
        this.resultMatrixIndex.add(this.resultMatrixIndex.size(), new ArrayList<Integer>());
        this.resultMatrixIndex.get(this.resultMatrixIndex.size()-1).add(element1Index);
        this.resultMatrixIndex.get(this.resultMatrixIndex.size()-1).add(element2Index);
        this.selectedElements.add(element1Index);
        this.selectedElements.add(element2Index);
    }

    private void getMostDifferentElements() {
        int elementIndex = 0;
        double minSimilarity = 1.0;
        int count = 0;
        double sumSimilarity = 0.0;
        
        while(this.resultMatrixIndex.size() < this.kMin){
            elementIndex = 0;
            minSimilarity = 1.0;
            
            for (int i = 0; i < this.videoList.size(); i++) {
                if(!this.selectedElements.contains(i)){
                    count = 0;
                    sumSimilarity = 0.0;
                    for (List<Integer> tmpList : this.resultMatrixIndex) {
                        for (Integer index : tmpList) {
                            count++;
                            sumSimilarity += this.similarityMatrix[i][index];
                        }
                    }
                    if(sumSimilarity / count < minSimilarity){
                        minSimilarity = sumSimilarity / count;
                        elementIndex = i;
                    }
                }
            }
            
            this.result.add(this.result.size(), new ArrayList<Video>());
            this.result.get(this.result.size()-1).add(this.videoList.get(elementIndex));
            this.resultMatrixIndex.add(this.resultMatrixIndex.size(), new ArrayList<Integer>());
            this.resultMatrixIndex.get(this.resultMatrixIndex.size()-1).add(elementIndex);
            this.selectedElements.add(elementIndex);
        }
    }

    private void aggregateRestVideos() {
        int elementIndex = 0;
        double maxSim = 0.0;
        int groupNumber = 0;
        int count = 0;
        double sumSimilarity = 0.0;
            
        while(this.selectedElements.size() < this.videoList.size()){
            elementIndex = 0;
            maxSim = 0.0;
            groupNumber = 0;
            
            for (int i = 0; i < this.videoList.size(); i++) {
                if(!this.selectedElements.contains(i)){
                    for (int j = 0; j < this.resultMatrixIndex.size(); j++) {
                        List<Integer> tmpList = this.resultMatrixIndex.get(j);
                        count = 0;
                        sumSimilarity = 0.0;
                        for (Integer index : tmpList) {
                            count++;
                            sumSimilarity += this.similarityMatrix[i][index];
                        }
                        
                        if(sumSimilarity / count > maxSim){
                            maxSim = sumSimilarity / count;
                            elementIndex = i;
                            groupNumber = j;
                        }
                    }
                }
            }
            
            this.result.get(groupNumber).add(this.videoList.get(elementIndex));
            this.resultMatrixIndex.get(groupNumber).add(elementIndex);
            this.selectedElements.add(elementIndex);
        }
    }
    
    
}
