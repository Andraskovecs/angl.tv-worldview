/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.aggregation;

import com.angl.angldao.main.OwnStatistic;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videopopularity;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kanyó Krisztián
 */
public class PopularityAggregation extends CommonAggregation{
    private List<OwnStatistic> statisticElements = new ArrayList<OwnStatistic>();
    private int newVIdeoLimit = 6;
    private int historyLimit = 10;
    private double minValue = 0.3; /* 0-1 közötti érték, a legutolsó elem minimum hány pontot kapjon, azaz a 0-1 skála hova szűküljön be */
    private HashMap<String, List<Double>> maxStatistics = new HashMap<String, List<Double>>();
    private HashMap<String, List<Double>> minStatistics = new HashMap<String, List<Double>>();
    private static final HashMap<String, Double> differentSkalars;
    static
    {
        differentSkalars = new HashMap<String, Double>();
        differentSkalars.put("avgView", 1.5);
        differentSkalars.put("viewSteppness", 1.5);
        differentSkalars.put("providerViewDifferences", 0.0);
    }
    
    
    public PopularityAggregation() {
        super();
        this.similarityFileName = "popularitySimilarityMatrix.csv";
    }
    
    public PopularityAggregation(Date date) {
        super();
        this.similarityFileName = "popularitySimilarityMatrix.csv";
        this.selectedDay = date;
    }
    
    @Override
    protected void fillAttributeList(){
        Field[] fields = OwnStatistic.class.getDeclaredFields();
        if(fields != null){
            for (Field field : fields) {
                String key = field.getName();
                Double skalar = this.differentSkalars.containsKey(key) ? this.differentSkalars.get(key) : 1.0;
                String type = key.contains("own") ? "own" : key.contains("provider") ? "provider" : null;
                if(type != null) this.attributes.add(new AggregationAttribute(skalar, type, Arrays.asList(key)));
            }
        }
    }

    @Override
    protected void checkStartValue() throws Exception {
        return;
    }
    
    
    
    @Override
    protected String getAdditionalAggregationName(){
        return "." + this.df.format(this.selectedDay);
    }
    
    @Override
    protected void structureResult() {
        int newVideoLimit = (int)(this.returnSize * 0.1);
        if(this.result.get(1).size() < newVideoLimit) newVideoLimit = this.result.get(1).size();
        int popularVideoLimit = this.returnSize - newVideoLimit;
        this.result.set(0, this.result.get(0).subList(0, this.result.get(0).size() > popularVideoLimit ? popularVideoLimit : this.result.get(0).size() ));
        if(this.result.get(0).size() != popularVideoLimit){ 
            popularVideoLimit = this.result.get(0).size();
            newVideoLimit = this.returnSize - popularVideoLimit;
        }
        this.result.set(1, this.result.get(1).subList(0, this.result.get(1).size() > newVideoLimit ? newVideoLimit : this.result.get(1).size() ));
        if(this.result.get(1).size() != newVideoLimit) newVideoLimit = this.result.get(1).size();
        Random rand = new Random();
        while(this.result.get(0).size() > 0 || this.result.get(1).size() > 0){
            int index = rand.nextInt(2);
            if(this.result.get(index).size() == 0) index = (index+1)%2;
            this.structuredResult.add(this.result.get(index).remove(0));
        }
        System.out.println("A videólisták összefűzésre kerültek. A listában "+popularVideoLimit+" népszerű videó és "+newVideoLimit+" új videó került be. A lista teljes mérete: "+this.structuredResult.size());
    }

    @Override
    public boolean doAggregation() {
        System.out.println("Popularity. Selected day: " + this.selectedDay);
        return super.doAggregation();
    }
    
    @Override
    protected void kMedoid(){
        System.out.println("Popularity kMedoid");
        this.calculateSimilarity();
    }
    
    @Override
    protected void calculateSimilarity(){    
        this.defineMyAttributes();
        this.calculateVideoStatistics();
        this.getMinMaxValues();
        this.calculateSimilarityValues();
        Collections.sort(this.statisticElements, STATISTIC_POPULARITY_ORDER);
        
        for (OwnStatistic statisticElement : this.statisticElements) {
            this.result.get(0).add(statisticElement.getVideo());
        }
        
        System.out.println("Elegendő statisztikával rendelkező videók: "+this.result.get(0).size());
        System.out.println("Új videók: "+this.result.get(1).size());
        this.writeMatrixToFile(this.similarityFileName, this.similarityMatrix); 
        System.out.println("Similarity matrix saved: " + this.similarityFileName);
    }
    
    private void defineMyAttributes(){
        this.similarityMatrix = new double[this.videoList.size()][1];
        this.statisticElements = new ArrayList<OwnStatistic>();
        this.result.add(0, new LinkedList<>());
        this.result.add(1, new LinkedList<>());
    }
    
    private void calculateVideoStatistics(){                
        for (Video video : this.videoList) {
            Set<Videopopularity> vpSet = video.getVideopopularityes();
            if(vpSet.size() <= this.newVIdeoLimit){
                this.result.get(1).add(video);
            }
            else {
                this.statisticElements.add(new OwnStatistic(video, vpSet, this.historyLimit));
            }
        }
    }
    
    private void getMinMaxValues(){
        Double value = 0.0;
        int index = 0;
        String key = "";
        
        try{
            for (OwnStatistic statisticElement : this.statisticElements) {
                for (AggregationAttribute attribute : this.attributes) {
                    key = attribute.attributeList.get(0);
                    index = 0;
                    value = (double)statisticElement.getOwnAvgView();
//                    key = "ownViewDifferences";
                    
                    if(key.contains("Steppness")){
                        this.mh = this.lookup.findVirtual(OwnStatistic.class, "get"+this.ucfFirst(key), MethodType.methodType(List.class));
                        List<Double> tmpList = (List<Double>)this.mh.invokeExact(statisticElement);
                        for(index = 0; index < tmpList.size(); index++){
                            value = tmpList.get(index);
                            this.uptadeHashmapValueValue(key, value, index);
                        }
                        
                    } else if(key.contains("Differences")){
                        this.mh = this.lookup.findVirtual(OwnStatistic.class, "get"+this.ucfFirst(key), MethodType.methodType(List.class));
                        List<Integer> tmpList = (List<Integer>)this.mh.invokeExact(statisticElement);
                        for(index = 0; index < tmpList.size(); index++){
                            value = (double)(tmpList.get(index));
                            this.uptadeHashmapValueValue(key, value, index);
                        }
                        
                    }
                    else {
                        this.mh = this.lookup.findVirtual(OwnStatistic.class, "get"+this.ucfFirst(key), MethodType.methodType(int.class));
                        value = (double)((int)this.mh.invokeExact(statisticElement));
                        this.uptadeHashmapValueValue(key, value, index);
                    }
                    
                }
            }
        } catch (Throwable ex) {
            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void calculateSimilarityValues(){
        OwnStatistic statisticElement = null;
        String key = "";
        List<Object> valueObject = null;
        double value = 0.0;
                
        try{
            for (int elementIndex = 0; elementIndex < this.statisticElements.size(); elementIndex++) {
                statisticElement = this.statisticElements.get(elementIndex);
                
                for (AggregationAttribute attribute : this.attributes) {
                    key = attribute.attributeList.get(0);
                    if(key.contains("Steppness") || key.contains("Differences")){
                        this.mh = this.lookup.findVirtual(OwnStatistic.class, "get"+this.ucfFirst(key), MethodType.methodType(List.class));
                        valueObject = (List<Object>)((List)this.mh.invokeExact(statisticElement));
                        for(int i = 0; i < valueObject.size();i++){
                            Double min = (double)this.minStatistics.get(key).get(i);
                            Double max = (double)this.maxStatistics.get(key).get(i);
                            Double attributeValue = new Double((valueObject.get(i)).toString());//null;
                            this.statisticElements.get(elementIndex).addSimilarityPoint(this.calculatePoint(attribute.skalar/(double)valueObject.size(), min, max, attributeValue));
                        }
                    }
                    else{
                        Double min = this.minStatistics.get(key).get(0);
                        Double max = this.maxStatistics.get(key).get(0);
                        this.mh = this.lookup.findVirtual(OwnStatistic.class, "get"+this.ucfFirst(key), MethodType.methodType(int.class));
                        value = (double)((int)this.mh.invokeExact(statisticElement));
                        this.statisticElements.get(elementIndex).addSimilarityPoint(this.calculatePoint(attribute.skalar, min, max, value));
                    }
                }
                
                this.similarityMatrix[elementIndex][0] = this.statisticElements.get(elementIndex).getSimilarityPoint();
            }
        } catch (Throwable ex) {
            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    private Double calculatePoint(Double skalar, Double min, Double max, Double attributeValue){
        Double range = max - min;
        Double normalizedAttributeValue = attributeValue - min;
        if(range == 0.0) return 0.0;
        return (this.minValue + (1-this.minValue) * (normalizedAttributeValue/range)) * skalar;
    }
    
    private void uptadeHashmapValueValue(String key, Double value, int index){
        this.uptadeHashmapMaxValue(key, value, index);
        this.uptadeHashmapMinValue(key, value, index);
    }
        
    private void uptadeHashmapMaxValue(String key, Double value, int index){
        List<Double> currList = this.maxStatistics.get(key);
        /* No hashMap key */
        if(currList == null){
            this.maxStatistics.put(key, new ArrayList<Double>());
            this.maxStatistics.get(key).add(value);
        }
        /* No arraylist element whit give index */
        else if(index >= currList.size()){
            this.maxStatistics.get(key).add(value);
        }
        /* Check previous value, if less, update it */
        else {
            if(currList.get(index) < value) this.maxStatistics.get(key).set(index, value);
        }
    }
        
    private void uptadeHashmapMinValue(String key, Double value, int index){
        List<Double> currList = this.minStatistics.get(key);
        /* No hashMap key */
        if(currList == null){
            this.minStatistics.put(key, new ArrayList<Double>());
            this.minStatistics.get(key).add(value);
        }
        /* No arraylist element whit give index */
        else if(index >= currList.size()){
            this.minStatistics.get(key).add(value);
        }
        /* Check previous value, if less, update it */
        else {
            if(currList.get(index) > value) this.minStatistics.get(key).set(index, value);
        }
    }
    
    static final Comparator<OwnStatistic> STATISTIC_POPULARITY_ORDER = 
                                        new Comparator<OwnStatistic>() {
            public int compare(OwnStatistic os1, OwnStatistic os2) {
                return os2.getSimilarityPoint().compareTo(os1.getSimilarityPoint());
            }
    };
    
    
}
