/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.aggregation;

import lombok.Value;

/**
 *
 * @author Kanyó Krisztián
 */
@Value
public class Point {
    private double longitude, latitude;
}
