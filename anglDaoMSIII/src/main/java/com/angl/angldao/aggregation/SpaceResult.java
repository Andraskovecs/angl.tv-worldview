/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.aggregation;

import com.angl.angldao.models.Video;
import java.util.ArrayList;
import java.util.List;
import lombok.Value;

/**
 *
 * @author Kanyó Krisztián
 */
@Value
public class SpaceResult {
    int id;
    double latitude;
    double longitude;
    String city;
    String aggregationtype;
    List<Video> videoliList;

    public SpaceResult(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.videoliList = new ArrayList<>();
        this.city = "";
        this.aggregationtype = "";
        this.id = 0;
    }

    public SpaceResult(int id, double latitude, double longitude, String city, List<Video> videoliList, String aggregationtype) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.videoliList = videoliList;
        this.aggregationtype = aggregationtype;
    }    
    
}
