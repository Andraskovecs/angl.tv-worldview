/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.aggregation;

import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;



/**
 *
 * @author Kanyó Krisztián
 */
public class AggregationObjectFactory {
    private int kMin, nMin;
    private Date date;
    private List<Point> points;
    private double eps;
    
    public AggregationObjectFactory(){
        this.kMin = 0;
        this.nMin = 0;
        this.date = new Date();
    }
    
    public AggregationObjectFactory(List<Point> points, double eps, int nMin){
        this.eps = eps;
        this.kMin = 0;
        this.nMin = nMin;
        this.points = points;
        this.date = new Date();
    }
    
    public AggregationObjectFactory(int k, int n){
        this.kMin = k;
        this.nMin = n;
    }
    
    public AggregationObjectFactory(Date date){
        this.kMin = 0;
        this.nMin = 0;
        this.date = date;
    }
    
    public AggregationObject getAggregationObject(Class aggregationClass){
        if(aggregationClass == null){return null;}
        if(aggregationClass.equals(BasicAggregation.class)){
            return new BasicAggregation(this.kMin, this.nMin);
        }else if(aggregationClass.equals(PopularityAggregation.class)){
            return new PopularityAggregation(date);
        }else if(aggregationClass.equals(RecommendedAggregation.class)){
            return new RecommendedAggregation(this.kMin, this.nMin);
        }else if(aggregationClass.equals(SpaceAggregation.class)){
            if(this.points != null && !this.points.isEmpty())
                return new SpaceAggregation(this.points, this.eps, this.nMin);
            else return new SpaceAggregation();
        }else{
            return new CommonAggregation(this.kMin, this.nMin);
        }
    }
}
