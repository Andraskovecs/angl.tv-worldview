/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.aggregation;

import com.angl.angldao.dao.AggregationDAO;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.models.Basicaggregation;
import com.angl.angldao.models.Video;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.ml.clustering.DoublePoint;



public class SpaceAggregation extends CommonAggregation{
    private static @Getter @Setter int minElementCountToCreate = 20;
    private List<Point> startCentroid;
    private double eps = 0.5; // maximum radius of the neighborhood to be considered
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-hh:mm:ss");

    public SpaceAggregation(){
        this.selectedDay = new Date();
    }
    
    public SpaceAggregation(List<Point> centers, double eps, int nMin) {
        this.nMin = nMin;
        this.eps = eps;
        this.startCentroid = centers;
        this.spaceResult = new ArrayList<>();
        this.selectedDay = new Date();
        for (int i = 0; i < this.startCentroid.size(); i++) {
            this.spaceResult.add(new SpaceResult(this.startCentroid.get(i).getLatitude(), this.startCentroid.get(i).getLongitude()));
        }
        
    }

    @Override
    public SpaceResult getSpaceVideos(String id) {
        int selectedId = Integer.parseInt(id);
        this.readResult();
        for (SpaceResult sr : this.spaceResult) {
            if(sr.getId() == selectedId) return sr;
        }
        if(this.spaceResult.size() > 0) this.spaceResult.get(0);
        
        return new SpaceResult(0, 0);
    }

    @Override
    public List<SpaceResult> getSpaces() {
        this.readResult();
        boolean firstRun = true;
        SpaceResult firstSpace = null;
        List<SpaceResult> returnResult = new ArrayList<>();
        for (SpaceResult spaceResult1 : this.spaceResult) {
            if(firstRun){
                firstSpace = spaceResult1;
                firstRun = false;
            }
            if(spaceResult1.getAggregationtype().equals(firstSpace.getAggregationtype()))
                returnResult.add(spaceResult1);
        }
        return returnResult;
    }

    @Override
    protected String getAdditionalAggregationName() {
        return "."+this.sdf.format(this.selectedDay);
    }
    
    
    
    
    @Override
    protected void kMedoid(){
        this.firstRoundClaster();
        this.dbScanClaster();
        this.filterResult();
    }
    
    
    private void firstRoundClaster(){
        double d, currMin;
        int index, currIndex;        
        
        for (Video video : this.videoList) {
            index = 0; currIndex = -1; currMin = 0.0;
            for (Point point : this.startCentroid) {
                d = this.twoPointDifference(video.getLatitude(), point.getLatitude(), video.getLongitude(), point.getLongitude());
                if(currIndex == -1 || d < currMin){
                    currMin = d;
                    currIndex = index;
                }
                index++;
            }
            this.spaceResult.get(currIndex).getVideoliList().add(video);
        }
    }

//    @Override
//    protected void saveResult() {
//        DataAccessObject dao = null;
//        try {
//            dao = new DataAccessObjectFactory().getDataAccessObject(AggregationDAO.class);
//            dao.openSession();
//            ((AggregationDAO)dao).saveSpace(this.spaceResult);
//        } catch (Exception ex) {
//            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        finally{
//            if(dao != null) dao.closeSession();
//        }
//    }

    private void dbScanClaster() {
        DBSCANClusterer dbScan = new DBSCANClusterer(this.eps, this.kMin);
        List<DoublePoint> points;
        List<Cluster<DoublePoint>> cluster;
//        List<Cluster<Point>> tmpResult;
        
        for (SpaceResult spaceResult : this.spaceResult) {
            points = new ArrayList<>();
            for (Video video : spaceResult.getVideoliList()) {
                points.add(new DoublePoint(new double[]{video.getLongitude(), video.getLatitude()}));
            }
            cluster = dbScan.cluster(points);
            
            for (Cluster<DoublePoint> clusterList : cluster) {
                this.result.add(new ArrayList<>());
                for (DoublePoint pointPair : clusterList.getPoints()) {
                    this.addVideoToResult(new Point(pointPair.getPoint()[0], pointPair.getPoint()[1]));
                }
            }
            System.out.println("The end");
        }
    }
    
    private void addVideoToResult(Point videoGpsPoint){
        for (Video video : this.videoList) {
            if(video.getLatitude().equals(videoGpsPoint.getLatitude()) && video.getLongitude().equals(videoGpsPoint.getLongitude())){
                this.result.get(this.result.size()-1).add(video);
            }
        }
    }
    
    private void filterResult(){
        int index = 0;
        while(index < this.result.size()){
            if(this.result.get(index).size() < this.minElementCountToCreate)
                this.result.remove(index);
            else index++;
        }
    }
    
}

