package com.angl.angldao.aggregation;

import com.angl.angldao.models.Video;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Kanyó Krisztián
 */
public class RecommendedAggregation extends CommonAggregation{
    private List<Integer> selectedElements = new ArrayList<Integer>();
    
    public RecommendedAggregation() {
        super();
        this.nMin = 5;
        this.similarityFileName = "recommendedSimilarityMatrix.csv";
    }
    
    public RecommendedAggregation(int k, int n) {
        super(k, n);
        this.similarityFileName = "recommendedSimilarityMatrix.csv";
    }    
    
    @Override
    protected void fillAttributeList(){
        this.attributes.add(new AggregationAttribute(0.7, "vector", Arrays.asList("latitude", "longitude")));
        this.attributes.add(new AggregationAttribute(0.3, "boolean", Arrays.asList("username")));
    }
    
    @Override
    protected void structureResult() {
        this.searchForSelectedVideo();
        this.fillWithPopularVideo();
    }
    
    private void searchForSelectedVideo(){
        if(this.returnSize < 0 || this.selectedVideo == null) {
            System.out.println("Nem létező videót adott meg, vagy negatív listaszámmal kér le aggregáció eredményt");
            return;
        }
        List<Video> aggregationList = new ArrayList<>();
        for (List<Video> list : this.result) {
            if(this.selectedVideo.getId().equals(list.get(1).getId())){
                list.remove(1);
                aggregationList = list;
                System.out.println("Videó sikeresen megtalálva!");
            }
        }
        if(aggregationList.size() > 0 && aggregationList.size() < this.returnSize){
            this.structuredResult = aggregationList;
        }
        else if(aggregationList.size() > 0 && aggregationList.size() >= this.returnSize){
            Random rand = new Random();
            for(int i = 0; i < this.returnSize; i++){
                int index = rand.nextInt(aggregationList.size());
                this.structuredResult.add(aggregationList.remove(index));
            }
        }
        else{
            System.out.println("Videót nem sikerült megtalálni. Videoid: "+this.selectedVideo.getVideoid());
        }
    }
    
    private void fillWithPopularVideo(){
        if(this.structuredResult.size() >= this.returnSize) return;
        int freeSpaces = this.returnSize - this.structuredResult.size();
        System.out.println("Nincs megfelelő számú videó a listában, így a népszerű videókkal lesz feltöltve.");
        System.out.println("Jelenlegi videószám: "+this.structuredResult.size()+". Hiányzó elemszám: "+freeSpaces);
        AggregationObject ao = new AggregationObjectFactory().getAggregationObject(PopularityAggregation.class);
        this.structuredResult.addAll(ao.getAllVideo(freeSpaces));
        
    }
    
    @Override
    protected void kMedoid(){
        System.out.println("Hasonló videók keresése...");
        this.calculateSimilarity();
        this.getMostSimilarElementsForAllVideo();
        System.out.println("Hasonló videók keresése befejeződött!");
    }    
    
    @Override
    protected void checkStartValue() throws Exception{
        if(this.kMin > 10) throw new Exception("Nem megfelelő kezdeti értékek. (k értéke nem lehet nagyobb 10-nél)");
    }

    private void getMostSimilarElementsForAllVideo() {
        int elementIndex = 0;
        double maxSimilarity = 0.0;
        
        for (int i = 0; i < this.videoList.size(); i++) {
            this.result.add(i, new ArrayList<Video>());
            this.result.get(i).add(this.videoList.get(i));
            this.selectedElements.clear();
            for (int j = 0; j < this.nMin; j++) {
                elementIndex = 0;
                maxSimilarity = 0.0;
                for (int k = 0; k < this.videoList.size(); k++) {
                    if(maxSimilarity < this.similarityMatrix[i][k] && i == k && !this.selectedElements.contains(k)){
                        maxSimilarity = this.similarityMatrix[i][k];
                        elementIndex = k;
                    }
                }
                this.selectedElements.add(elementIndex);
                this.result.get(i).add(this.videoList.get(elementIndex));
            }
        }
    }
}
