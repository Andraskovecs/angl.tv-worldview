/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.aggregation;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Kanyó Krisztián
 */
public class AggregationAttribute {
    public Double skalar;
    public String type;
    public List<String> attributeList;

    public AggregationAttribute(double skalar, String type, List<String> attributeList) {
        this.skalar = skalar;
        this.type = type;
        this.attributeList = attributeList;
    }
    
}
