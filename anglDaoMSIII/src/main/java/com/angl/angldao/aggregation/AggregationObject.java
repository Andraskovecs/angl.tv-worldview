/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.aggregation;

import com.angl.angldao.models.Video;
import com.angl.angldao.models.Basicaggregation;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kanyó Krisztián
 */
public interface AggregationObject {
    
    public boolean doAggregation();
    
    public List<Video> getAllVideo(int elementSize);
    public List<Video> getAllVideo(int elementSize, Date date);
    public List<Video> getAllVideo(int elementSize, Video video);
    public List<SpaceResult> getSpaces();
    public SpaceResult getSpaceVideos(String id);
}
