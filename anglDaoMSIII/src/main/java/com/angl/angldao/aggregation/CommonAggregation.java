package com.angl.angldao.aggregation;

import com.angl.angldao.dao.AggregationDAO;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Basicaggregation;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.logging.log4j.LogManager;


public class CommonAggregation implements AggregationObject{
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();
    
    protected int iteration = 0;
    /**
     * minimális elemszám egy klaszteren belül
     */
    protected int nMin;
    /**
     * minimális klaszterszám
     */
    protected int kMin;
    /**
     * aktuális klaszterszám
     */
    protected Integer kCurr;
    protected List<Video> videoList;                // lekért videók tárolására
    protected List<List<Video>> result;             // klaszterezés során előállított videó klaszterek
    protected List<Video> structuredResult;         // videólekéréshez előállított lista
    protected List<SpaceResult> spaceResult;        // dao.readResult visszatérése
    protected List<List<Integer>> resultMatrixIndex;
    protected double[][] similarityMatrix;          // hasonlósági mátrix
    protected Date selectedDay = new Date();        // aggregációhoz kiválasztott nap
    protected Video selectedVideo = new Video();    // aggregációhoz kiválasztott videó
    protected int returnSize = 0;                   // videólista maximális mérete visszatérés esetén
    protected DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    
    protected List<AggregationAttribute> attributes = new ArrayList<AggregationAttribute>();
    
    protected String similarityFileName = "commonSimilarityMatrix.csv";
    
    protected MethodHandles.Lookup lookup = MethodHandles.lookup();
    protected MethodHandle mh = null;
    
    public CommonAggregation(){
        this.kMin = 0;
        this.nMin = 0;
        this.resetAttribute();  
        this.fillAttributeList();      
    }
    
    public CommonAggregation(int k, int n){
        this.kMin = k;
        this.nMin = n;
        this.resetAttribute();
        this.fillAttributeList();
    }
    
    protected void fillAttributeList(){
        return;
    }
        
    @Override
    public List<Video> getAllVideo(int elementSize, Video video){
        this.selectedVideo = video;
        return this.getAllVideo(elementSize);
    }
        
    @Override
    public List<Video> getAllVideo(int elementSize, Date date){
        this.selectedDay = date;
        return this.getAllVideo(elementSize);
    }

    @Override
    public List<Video> getAllVideo(int elementSize){
        this.returnSize = elementSize;
        if(this.returnSize == 0) return this.structuredResult;
        if(this.getClass() == CommonAggregation.class)
            this.doAggregation();
        else
            this.readResult();
        this.structureResult();
        return this.structuredResult;
    }
    
    protected void structureResult(){
        if(this.videoList.size() > this.returnSize) this.structuredResult = this.videoList.subList(0, this.returnSize);
    }
    
    @Override
    public boolean doAggregation() {
        System.out.println("Aggregation is starting... kMin=" + this.kMin);
        this.resetAttribute();
        DataAccessObject dao = null;
        try {
            dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
            dao.openSession();
            if(this.getClass() == PopularityAggregation.class){
                this.videoList = ((VideoDAO)dao).getAllVideoByDay(this.selectedDay);
            } else if(this.getClass() == SpaceAggregation.class){
                this.videoList = ((VideoDAO)dao).getAllVideoForSpace();
            } else {
                this.videoList = ((VideoDAO)dao).getAllVideo();
            }
//            this.videoList = this.videoList.subList(0, videoList.size() > 200 ? 200 : videoList.size() );
            System.out.println("Klaszterezéshez lekért videók száma: "+this.videoList.size());
            this.checkStartValue();
            //this.makeClasters();
            this.kMedoid();
            
            if(this.getClass() != CommonAggregation.class){
                this.soutSimilarityMatrixIndex();
                this.saveResult();
            }
            
        } catch (Exception ex) {
            System.out.println("getAllVideo hibaüzenet: " + ex.getMessage());
            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        finally {
            if(dao != null) dao.closeSession();
        }
        return true;
    }
    
    protected void makeClasters() throws Exception{
        this.iteration++;
        System.out.println(this.iteration + ". iteráció");
        if(!this.calculateCurrentK()) return;
        this.kMedoid();
        if(this.clasterFiltering()) return;
        else this.makeClasters();
    }
    
    protected void resetAttribute(){
        this.selectedDay = new Date();
        this.kCurr = null;
        this.result = new LinkedList<List<Video>>();
        this.structuredResult = new LinkedList<Video>();
        this.resultMatrixIndex = new LinkedList<List<Integer>>();
    }
    
    protected void checkStartValue() throws Exception{
        if(!((double)this.kMin < (double)this.videoList.size() / this.nMin)) throw new Exception("Nem megfelelő kezdeti értékek.");
    }
    
    protected boolean calculateCurrentK() throws Exception{
        if(this.kCurr == null)
            this.kCurr = (int)(this.videoList.size() / this.kMin);
        else{
            this.kCurr--;
            if(this.kCurr < this.kMin) return false;
        }
        return true;
    }
    
    protected void kMedoid(){
        this.structuredResult = this.videoList;
    }
    
    protected boolean clasterFiltering(){
//        List<List<Video>> tmp = new ArrayList<List<Video>>();
//        for (List<Video> list : this.result) {
//            if(list.size() >= this.nMin) tmp.add(list);
//        }
//        if(tmp.size() >= this.kMin){
//            this.result = tmp;
//            return true;
//        }
//        return false;
        if(this.iteration > 1 ) return true;
        return false;
    }
    
    private void printMyAttribute(){
        System.out.println("kMin: "+this.kMin);
        System.out.println("videoList size: "+this.videoList.size());
        System.out.println("nMin: "+this.nMin);
    }
    
    protected void calculateSimilarity(){
        this.similarityMatrix = new double[this.videoList.size()][this.videoList.size()];
        for(AggregationAttribute attribute: this.attributes){
            if(attribute.type.equals("vector"))
                this.calculateVektorDistance(attribute.attributeList, attribute.skalar);
        }
        
        for (int i = 0; i < this.videoList.size(); i++) {
            for (int j = 0; j < this.videoList.size(); j++) {
                for(AggregationAttribute attribute: this.attributes){
                    if(attribute.type.equals("boolean"))
                        this.similarityMatrix[i][j] += this.calculateBooleanDistance(attribute.attributeList.get(0), i, j) * attribute.skalar;
                }
            }

        }
        
        this.writeMatrixToFile(this.similarityFileName, this.similarityMatrix); 
        System.out.println("Similarity matrix saved: " + this.similarityFileName);
    }
    
    private void calculateVektorDistance(List<String> attributeList, double skalar){

        double[][] tmpMatrix = new double[this.videoList.size()][this.videoList.size()];

        double maxDistance = 0.0;
        double d = 0.0;
        
        for (int i = 0; i < this.videoList.size(); i++) {
            for (int j = 0; j < this.videoList.size(); j++) {
                if(i == j){
                    tmpMatrix[i][j] = 0.0;
                }
                else {
                    try{
                        mh = lookup.findVirtual(Video.class, "get"+this.ucfFirst(attributeList.get(0)), MethodType.methodType(Double.class));
                        Double iValue1 = (Double)mh.invokeExact(this.videoList.get(i));
                        Double jValue1 = (Double)mh.invokeExact(this.videoList.get(j));

                        mh = lookup.findVirtual(Video.class, "get"+this.ucfFirst(attributeList.get(1)), MethodType.methodType(Double.class));
                        Double iValue2 = (Double)mh.invokeExact(this.videoList.get(i));
                        Double jValue2 = (Double)mh.invokeExact(this.videoList.get(j));

                        d = this.twoPointDifference(jValue1, iValue1, jValue2, iValue2);
                        
                        if(maxDistance < d) maxDistance = d;
                        tmpMatrix[i][j] = d;
                    } catch (Throwable ex) {
                        Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }   
        }
        
        for (int i = 0; i < this.videoList.size(); i++) {
            for (int j = 0; j < this.videoList.size(); j++) {
                if(i != j){
                    this.similarityMatrix[i][j] += (1.0 - (tmpMatrix[i][j] / maxDistance)) * skalar;
                }
                
            }
        }
        
    }
    
    protected double twoPointDifference(double jValue1, double iValue1, double jValue2, double iValue2){
        return Math.sqrt(Math.pow(jValue1-iValue1, 2) + Math.pow(jValue2-iValue2, 2));
    }
    
    private double calculateBooleanDistance(String attribute, int i, int j){
        if(i == j) return 0.0;
        try{
            mh = lookup.findVirtual(Video.class, "get"+this.ucfFirst(attribute), MethodType.methodType(String.class));
            String iValue = (String)mh.invokeExact(this.videoList.get(i));
            String jValue = (String)mh.invokeExact(this.videoList.get(j));
            return iValue.equals(jValue) ? 1.0 : 0.0;
        } catch (NoSuchFieldException ex) { 
            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Throwable ex) {
            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0.0;
    }
    
    protected void saveResult(){            
        DataAccessObject dao = null;
        try {
            dao = new DataAccessObjectFactory().getDataAccessObject(AggregationDAO.class);
            dao.openSession();
            ((AggregationDAO)dao).saveAggregation(this, this.result, this.getAdditionalAggregationName());
            System.out.println("Mentett aggregáció csoportok száma: "+this.result.size());
        } catch (Exception ex) {
            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(dao != null) dao.closeSession();
        }
    }
    
    protected String getAdditionalAggregationName(){
        return "";
    }
    
    protected void readResult(){
        DataAccessObject dao = null;
        try {
            dao = new DataAccessObjectFactory().getDataAccessObject(AggregationDAO.class);
            dao.openSession();
            System.out.println("class: "+this.getClass().getName());
            String aggregationType = this.getClass().getName();
            if(this.getClass() == PopularityAggregation.class) aggregationType += "." + this.df.format(this.selectedDay);
            if(this.getClass() == SpaceAggregation.class) aggregationType += "%";
            this.spaceResult = ((AggregationDAO)dao).getAggregation(aggregationType);
            if(!this.getClass().equals(SpaceAggregation.class)){
                for (SpaceResult sr : this.spaceResult) {
                    this.result.add(sr.getVideoliList());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CommonAggregation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(dao != null) dao.closeSession();
        }
    }
    
    protected void writeMatrixToFile(String filename, double[][] matrix) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(filename, Boolean.FALSE));

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    bw.write(matrix[i][j] + ((j == matrix[i].length-1) ? "" : ";"));
                }
                bw.newLine();
            }
            bw.flush();
        } catch (IOException e) {}
    }
    
    protected String ucfFirst(String s){
        return s.substring(0,1).toUpperCase() + s.substring(1);
    }
    
    private void soutSimilarityMatrixIndex(){
        for (List<Integer> list : this.resultMatrixIndex) {
            System.out.println("új indexlista.");
            for (Integer integer : list) {
                System.out.printf(integer + ", ");
            }
            System.out.println("end");
        }
    }   

    @Override
    public List<SpaceResult> getSpaces() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SpaceResult getSpaceVideos(String is) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
