
package com.angl.angldao.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "videohistory", schema = "public")
public class Videohistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private @Getter @Setter int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_user")
    private @Getter @Setter User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_video")
    private @Getter @Setter Video video;
    @Column(name = "ip", nullable = false)
    private @Getter @Setter String ip;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "watchdate", length = 29)
    private @Getter @Setter Date watchdate;

    public Videohistory(User user, Video video, Date watchdate) {
        this.user = user;
        this.video = video;
        this.watchdate = watchdate;
    }

    public Videohistory(Video video, String ip, Date watchdate) {
        this.video = video;
        this.ip = ip;
        this.watchdate = watchdate;
    }

    public Videohistory() {
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Videohistory other = (Videohistory) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Videohistory{" + "id=" + id + ", user=" + user + ", video=" + video + ", ip=" + ip + ", watchdate=" + watchdate + '}';
    }
}
