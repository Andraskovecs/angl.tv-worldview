/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.models;

import com.angl.angldao.enums.Logtype;
import com.angl.angldao.enums.PostgreSQLEnumType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@Entity
@Table(name = "userlog", schema = "public")
@TypeDef(
    name = "pgsql_enum",
    typeClass = PostgreSQLEnumType.class
)
public class Userlog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private @Getter @Setter int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_user")
    private @Getter @Setter User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_video")
    private @Getter @Setter Video video;
    @Column(name = "ip", nullable = false)
    private @Getter @Setter String ip;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "logtype")
    @Type( type = "pgsql_enum" )
    private @Getter @Setter Logtype logtype;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "logdate", length = 29)
    private @Getter @Setter Date logdate;

    public Userlog() {
    }

    public Userlog(User user, Video video, Logtype logtype, Date logdate) {
        this.user = user;
        this.video = video;
        this.logtype = logtype;
        this.logdate = logdate;
    }

    public Userlog(Video video, String ip, Logtype logtype, Date logdate) {
        this.video = video;
        this.ip = ip;
        this.logtype = logtype;
        this.logdate = logdate;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Userlog other = (Userlog) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Userlog{" + "id=" + id + ", ip=" + ip + ", logtype=" + logtype + ", logdate=" + logdate + '}';
    }
    
}

