/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.models;


import com.angl.angldao.enums.Statistictype;
import com.angl.angldao.enums.PostgreSQLEnumType;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

/**
 *
 * @author Kanyó Krisztián
 */

@Entity
@Table(name = "videopopularity",schema = "public")
@TypeDef(
    name = "pgsql_enum",
    typeClass = PostgreSQLEnumType.class
)
public class Videopopularity implements java.io.Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private @Getter @Setter int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_video", nullable = false)
    private @Getter @Setter Video video;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "statistictype")
    @Type( type = "pgsql_enum" )
    private @Getter @Setter Statistictype statistictype;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "statistictime", length = 29)
    private @Getter @Setter Date statistictime;
    @Column(name = "views", nullable = false)
    private @Getter @Setter int views;
    @Column(name = "comments", nullable = true)
    private @Getter @Setter int comments;
    @Column(name = "echo", nullable = true)
    private @Getter @Setter int echo;

    public Videopopularity() {
    }

    public Videopopularity(Video video, Statistictype statistictype, Date statistictime, int views) {
        this.video = video;
        this.statistictype = statistictype;
        this.statistictime = statistictime;
        this.views = views;
    }    
    
    public Videopopularity(Video video, Statistictype statistictype, Date statistictime, int views, int comments, int echo) {
        this.video = video;
        this.statistictype = statistictype;
        this.statistictime = statistictime;
        this.views = views;
        this.comments = comments;
        this.echo = echo;
    }

    public Videopopularity(int id, Video video, Statistictype statistictype, Date statistictime, int views, int comments, int echo) {
        this.id = id;
        this.video = video;
        this.statistictype = statistictype;
        this.statistictime = statistictime;
        this.views = views;
        this.comments = comments;
        this.echo = echo;
    }
    
}