package com.angl.angldao.models;
// Generated 2018.01.16. 10:53:40 by Hibernate Tools 4.3.1

import com.angl.angldao.enums.Sextype;
import com.angl.angldao.enums.PostgreSQLEnumType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

/**
 * Userdetails generated by hbm2java
 */
@Entity
@Table(name = "userdetails", schema = "public")
@TypeDef(
    name = "pgsql_enum",
    typeClass = PostgreSQLEnumType.class
)
public class Userdetails implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private @Getter @Setter int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_user", nullable = false)
    private @Getter @Setter User user;
    @Column(name = "firstname", nullable = false)
    private @Getter @Setter String firstname;
    @Column(name = "lastname", nullable = false)
    private @Getter @Setter String lastname;
    @Column(name = "age")
    private @Getter @Setter Short age;
    @Column(name = "profession")
    private @Getter @Setter String profession;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "sex")
    @Type( type = "pgsql_enum" )
    private @Getter @Setter Sextype sex;
    @Column(name = "highestqualification")
    private @Getter @Setter String highestqualification;
    @Column(name = "familystatus")
    private @Getter @Setter String familystatus;

    public Userdetails() {
    }

    public Userdetails(int id, User user, String firstname, String lastname) {
        this.id = id;
        this.user = user;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public Userdetails(User user, String firstname, String lastname, Short age, String profession, Sextype sex, String highestqualification, String familystatus) {
        this.user = user;
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.profession = profession;
        this.sex = sex;
        this.highestqualification = highestqualification;
        this.familystatus = familystatus;
    }

    public Userdetails(int id, User user, String firstname, String lastname, Short age, String profession, Sextype sex, String highestqualification, String familystatus) {
        this.id = id;
        this.user = user;
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.profession = profession;
        this.sex = sex;
        this.highestqualification = highestqualification;
        this.familystatus = familystatus;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Userdetails other = (Userdetails) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Userdetails{" + "id=" + id + ", user=" + user + ", firstname=" + firstname + ", lastname=" + lastname + ", age=" + age + ", profession=" + profession + ", sex=" + sex + ", highestqualification=" + highestqualification + ", familystatus=" + familystatus + '}';
    }
    
}
