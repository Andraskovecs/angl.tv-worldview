package com.angl.angldao.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
@Entity
@Table(name = "video", schema = "public")

@JsonIgnoreProperties({"provider","videodetailses", "echos","comments", "videokeywords", "videolabelses", "videopopularityes", "history", "handler", "hibernateLazyInitializer"})
public class Video implements java.io.Serializable {   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private @Getter @Setter Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_provider")
    private @Getter @Setter Provider provider;
    @Column(name = "videoid")
    private @Getter @Setter String videoid;
    @Column(name = "originalurl")
    private @Getter @Setter String originalurl;
    @Column(name = "defaultstreamurl")
    private @Getter @Setter String defaultstreamurl;
    @Column(name = "defaultthumbnailurl")
    private @Getter @Setter String defaultthumbnailurl;
    @Column(name = "title")
    private @Getter @Setter String title;
    @Column(name = "watched", nullable = false)
    private @Getter @Setter int watched;
    @Column(name = "localwatched", nullable = false)
    private @Getter @Setter int localwatched;
    @Column(name = "username")
    private @Getter @Setter String username;
    @Column(name = "location")
    private @Getter @Setter String location;
    @Column(name = "providertype", nullable = false)
    private @Getter @Setter String providertype;
    @Column(name = "live")
    private @Getter @Setter Boolean live;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "publishdate", length = 29)
    private @Getter @Setter Date publishdate;
    @Column(name = "latitude", precision = 17, scale = 17)
    public @Getter @Setter Double latitude;
    @Column(name = "longitude", precision = 17, scale = 17)
    public @Getter @Setter Double longitude;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "video")
    private @Getter @Setter Set<Videodetails> videodetailses = new HashSet<Videodetails>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "video")
    private @Getter @Setter Set<Comment> comments = new HashSet<Comment>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "video")
    private @Getter @Setter Set<Videokeyword> videokeywords = new HashSet<Videokeyword>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "video")
    private @Getter @Setter Set<Videolabels> videolabelses = new HashSet<Videolabels>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "video")
    private @Getter @Setter Set<Videopopularity> videopopularityes = new HashSet<Videopopularity>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "video")
    private @Getter @Setter Set<Echo> echos = new HashSet<Echo>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "video")
    private @Getter @Setter Set<Videohistory> history = new HashSet<Videohistory>(0);
    @Transient
    private String videoType;
    @Transient
    private @Getter @Setter Integer echoCount, commentCount;
    @Transient
    private static final SimpleDateFormat DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    @Transient
    private static @Getter @Setter Long borderTime = 48L;  
    @Transient
    private static @Getter @Setter Long plusPerEcho = 2L;
    @Transient
    private static final String patternM3u8 = ".*m3u8.*", patternMpd = ".*mpd.*",patternMp4 = ".*mp4.*";
    @Transient
    private static final String typeMp4 = "video/mp4",typeMpd = "application/dash+xml",typeM3u8 = "application/x-mpegURL"; 
    
    public Video() {
    }
        
    public Video(String videoid, String originalurl, String defaultstreamurl, String defaultthumbnailurl, String title, int watched, String username, String providertype, Boolean live, Date publishdate, Double latitude, Double longitude) {
        this.videoid = videoid;
        this.originalurl = originalurl;
        this.defaultstreamurl = defaultstreamurl;
        this.defaultthumbnailurl = defaultthumbnailurl;
        this.title = title;
        this.watched = watched;
        this.username = username;
        this.providertype = providertype;
        this.live = live;
        this.publishdate = publishdate;
        this.latitude = latitude;
        this.longitude = longitude;
    }    

    public Video(Long id, Provider provider, String videoid, String originalurl, String defaultstreamurl, String defaultthumbnailurl, String title, int watched, String username, String providertype, Boolean live, Date publishdate, Double latitude, Double longitude, Set<Videodetails> videodetailses, Set<Comment> comments, Set<Videokeyword> videokeywords, Set<Videolabels> videolabelses) {
        this.id = id;
        this.provider = provider;
        this.videoid = videoid;
        this.originalurl = originalurl;
        this.defaultstreamurl = defaultstreamurl;
        this.defaultthumbnailurl = defaultthumbnailurl;
        this.title = title;
        this.watched = watched;
        this.username = username;
        this.providertype = providertype;
        this.live = live;
        this.publishdate = publishdate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.videodetailses = videodetailses;
        this.comments = comments;
        this.videokeywords = videokeywords;
        this.videolabelses = videolabelses;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Video other = (Video) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

   
    @Override
    public String toString() {
        return "Video{" + "id=" + id + ", videoid=" + videoid + '}';
    }

    

    public String getVideoType() {
        videoType = "";
        if(this.getDefaultstreamurl().matches(Video.patternMpd)){videoType = Video.typeMpd; }
        if(this.getDefaultstreamurl().matches(Video.patternM3u8)){videoType = Video.typeM3u8; }
        if(this.getDefaultstreamurl().matches(Video.patternMp4)){videoType = Video.typeMp4; }
        return videoType;
    }    
    public Boolean isNeeded(){
        Long now = new Date().getTime();
        final long hoursInMillis = 60L * 60L * 1000L;
        final long minInMillis = 60L * 1000L;
        Long echoPlus = 0L;
//        if(this.echos != null && this.echos.size()>0){
//            echoPlus = (this.echos.size()*plusPerEcho*minInMillis);
//        }
        Long border = new Date(now - (borderTime * hoursInMillis)-echoPlus).getTime();
        Long publish = publishdate.getTime();        
        return !(border>publish);
    }

}
