package com.angl.angldao.models;
// Generated 2018.01.16. 10:53:40 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * Provider generated by hbm2java
 */
@Entity
@Table(name = "provider", schema = "public")
public class Provider implements java.io.Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private @Getter @Setter int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_administrator", nullable = false)
    private @Getter @Setter User user;
    @Column(name = "guid", nullable = false, length = 100)
    private @Getter @Setter String guid;
    @Column(name = "name", nullable = false)
    private @Getter @Setter String name;
    @Column(name = "logo", length = 21845)
    private @Getter @Setter String logo;
    @Column(name = "icon", nullable = false, length = 21845)
    private @Getter @Setter String icon;
    @Column(name = "settings")
    private @Getter @Setter String settings;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "provider")
    private @Getter @Setter Set<Provideruser> providerusers = new HashSet<Provideruser>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "provider")
    private @Getter @Setter Set<Video> videos = new HashSet<Video>(0);

    public Provider() {
    }

    public Provider(int id, User user, String guid, String name, String icon) {
        this.id = id;
        this.user = user;
        this.guid = guid;
        this.name = name;
        this.icon = icon;
    }

    public Provider(int id, User user, String guid, String name, String logo, String icon, String settings, Set<Provideruser> providerusers, Set<Video> videos) {
        this.id = id;
        this.user = user;
        this.guid = guid;
        this.name = name;
        this.logo = logo;
        this.icon = icon;
        this.settings = settings;
        this.providerusers = providerusers;
        this.videos = videos;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Provider other = (Provider) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Provider{" + "id=" + id + ", user=" + user + ", guid=" + guid + ", name=" + name + ", logo=" + logo + ", icon=" + icon + ", settings=" + settings + ", providerusers=" + providerusers + ", videos=" + videos + '}';
    }
    
}
