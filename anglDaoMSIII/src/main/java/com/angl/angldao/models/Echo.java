package com.angl.angldao.models;
// Generated 2018.01.16. 10:53:40 by Hibernate Tools 4.3.1

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

/**
 * Comment generated by hbm2java
 */
@Entity
@Table(name = "echo", schema = "public")
@JsonIgnoreProperties({"video","user"})
public class Echo implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private @Getter @Setter int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_user")
    private @Getter @Setter User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fid_video")
    private @Getter @Setter Video video;
    @Column(name = "echotype")
    private @Getter @Setter String echotype;
    @Column(name = "ip")
    private @Getter @Setter String ip;

    public Echo(User user, Video video, String echotype) {
        this.user = user;
        this.video = video;
        this.echotype = echotype;
    }

    public Echo() {
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Echo other = (Echo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Echo{" + "id=" + id + ", echotype=" + echotype + '}';
    }

}
