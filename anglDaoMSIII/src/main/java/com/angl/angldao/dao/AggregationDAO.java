/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.dao;

import com.angl.angldao.aggregation.PopularityAggregation;
import com.angl.angldao.aggregation.SpaceAggregation;
import com.angl.angldao.aggregation.SpaceResult;
import com.angl.angldao.main.LocationHelper;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Aggregationvideo;
import com.angl.angldao.models.Basicaggregation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.hibernate.HibernateException;
import org.hibernate.Query;

/**
 *
 * @author Kanyó Krisztián
 */
public class AggregationDAO extends CommonDAO{
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();
    
    public AggregationDAO() {
        super();
    }
    
    public Boolean saveAggregation(Object aggregatorClass, List<List<Video>> result, String date) throws HibernateException{
        try{ 
            this.openTransaction();
            Integer minId = 0;
            for (List<Video> videos : result) {
                Object entity = new Object();
                entity = new Basicaggregation(aggregatorClass.getClass().getName() + date);
//                if(aggregatorClass.getClass().getSimpleName().equals("BasicAggregation")) entity = new Basicaggregation(aggregatorClass.getClass().getName());
//                Integer newId = ((AggregationDAO)dao).saveAggregation(aggregationObject, list);
                Serializable ser = this.session.save(entity);
                if (ser == null) {
                    logger.error("ROLLBACK. Nem sikerült felvinni az új klasztert az adatbázisba.");
                    this.transaction.rollback();
                    return false;
                }
                Integer id = (Integer) ser;
                if(minId.equals(0) || id < minId) minId = id;
                Double sumLat = 0.0;
                Double sumLon = 0.0;
                Integer elements = videos.size();
                for (Video video : videos) {
                    Video isInDb = (Video) this.session.get(Video.class, video.getId());
                    if(isInDb!= null){
                        Aggregationvideo av = new Aggregationvideo((Basicaggregation)entity, video);
                        ser = this.session.save(av);
                        if (ser != null && video.getLatitude() != null && video.getLongitude() != null) {
                            sumLat += video.getLatitude();
                            sumLon += video.getLongitude();
                        }
                        else{
                            elements--;
                        }
                    }
                    else elements--;
                }

                ((Basicaggregation)entity).setLatitude(sumLat / elements);
                ((Basicaggregation)entity).setLongitude(sumLon / elements);
                if(aggregatorClass.getClass().getName().equals(SpaceAggregation.class.getName()))
                    ((Basicaggregation)entity).setCity(new LocationHelper(String.valueOf(((Basicaggregation)entity).getLatitude()), String.valueOf(((Basicaggregation)entity).getLongitude())).getFormattedAddress());
                this.session.update(entity);
            }
            
            /* Remove previous aggregation result */
            Query query = session.createQuery("delete Basicaggregation where id < :maxId and aggregationtype like :aggregationtype");
            if(aggregatorClass.getClass().getSimpleName().equals(SpaceAggregation.class.getSimpleName())){
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.HOUR, -1);
                Date oneHourBack = cal.getTime();
                query = session.createQuery("delete Basicaggregation where aggregationdate < :oneHourBack");
                query.setParameter("oneHourBack", oneHourBack);
            }
            else if(aggregatorClass.getClass().getSimpleName().equals(PopularityAggregation.class.getSimpleName())){
                query = session.createQuery("delete Basicaggregation where (id < :maxId and aggregationtype like :aggregationtype) or (aggregationdate < :oneHourBack)");
                query.setParameter("maxId", minId);
                query.setParameter("aggregationtype", aggregatorClass.getClass().getName() + date);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.HOUR, -1);
                Date oneHourBack = cal.getTime();
                query.setParameter("oneHourBack", oneHourBack);
            }
            else{
                query.setParameter("maxId", minId);
                query.setParameter("aggregationtype", aggregatorClass.getClass().getName() + date);
                System.out.println("maxId "+ minId + " at "+aggregatorClass.getClass().getName() + date);
            }
            int deletedRows = query.executeUpdate();
            System.out.println("Törölt aggregációk száma: "+deletedRows);

            /* Commit */
            this.transaction.commit();
        }catch(HibernateException ex){
            this.transaction.rollback();
            logger.error("ROLLBACK. HibarnateException: "+ex.getMessage());
            throw ex;
        }
        return true;
    }
    
    public Boolean saveSpace(List<SpaceResult> result) throws HibernateException{
        try{ 
            this.openTransaction();
            Integer minId = 0;
            for (SpaceResult spaceResult : result) {
                Object entity = new Object();
                entity = new Basicaggregation(spaceResult.getLatitude(), spaceResult.getLongitude(), SpaceAggregation.class.getName());
//                if(aggregatorClass.getClass().getSimpleName().equals("BasicAggregation")) entity = new Basicaggregation(aggregatorClass.getClass().getName());
//                Integer newId = ((AggregationDAO)dao).saveAggregation(aggregationObject, list);
                Serializable ser = this.session.save(entity);
                if (ser == null) {
                    logger.error("ROLLBACK. Nem sikerült felvinni az új klasztert az adatbázisba.");
                    this.transaction.rollback();
                    return false;
                }

                Integer id = (Integer) ser;
                if(minId.equals(0) || id < minId) minId = id;
                for (Video video : spaceResult.getVideoliList()) {
                    Video isInDb = (Video) this.session.get(Video.class, video.getId());
                    if(isInDb!= null){
                        Aggregationvideo av = new Aggregationvideo((Basicaggregation)entity, video);
                        ser = this.session.save(av);                        
                    }
                }
            }
            
            /* Remove previous aggregation result */
            Query query = session.createQuery("delete Basicaggregation where id < :maxId and aggregationtype like :aggregationtype");
            query.setParameter("maxId", minId);
            query.setParameter("aggregationtype", SpaceAggregation.class.getName());
            System.out.println("maxId "+ minId + " at "+SpaceAggregation.class.getName());
            int deletedRows = query.executeUpdate();
            System.out.println("Törölt aggregációk száma: "+deletedRows);

            /* Commit */
            this.transaction.commit();
        }catch(HibernateException ex){
            this.transaction.rollback();
            logger.error("ROLLBACK. HibarnateException: "+ex.getMessage());
            throw ex;
        }
        return true;
    }
    
    public List<SpaceResult> getAggregation(String className){
        List<SpaceResult> result = new ArrayList<SpaceResult>();
        List<Object[]> queryList = this.session.createQuery("select v, ba" +
                    " from Video v, Aggregationvideo av, Basicaggregation ba" +
                    " where v = av.video and ba = av.basicaggregation" +
                    " and ba.aggregationtype like '" + className + "'" +
                    " order by ba.aggregationtype desc, ba.id")
                .list();
        
        Basicaggregation prevBa = new Basicaggregation();
        Boolean firstRun = true;
        Video video;
        List<Video> videoList = new ArrayList<Video>();
        for (Object[] objects : queryList) {
            video = (Video)objects[0];
            video.setCommentCount(video.getComments().size());
            video.setEchoCount(video.getEchos().size());
            if (video.getProvidertype().equals("periscope")) {
                video.setDefaultstreamurl("http://104.237.4.64:8090/proxy/" + video.getVideoid() + "/playlist.m3u8");
                video.setDefaultthumbnailurl("http://104.237.4.64:8090/proxy/" + video.getVideoid() + "/thumbnail");
            }
            Basicaggregation ba = (Basicaggregation)objects[1];
            if(!prevBa.equals(ba) && !firstRun){
                result.add(new SpaceResult(ba.getId(), ba.getLatitude(), ba.getLongitude(), ba.getCity(), videoList, ba.getAggregationtype()));
                videoList = new ArrayList<Video>();
            }
            videoList.add(video);
            prevBa = ba;
            firstRun = false;
        }
        if(!queryList.isEmpty())
            result.add(new SpaceResult(prevBa.getId(), prevBa.getLatitude(), prevBa.getLongitude(), prevBa.getCity(), videoList, prevBa.getAggregationtype())); // Adding last aggregation group to result
        
        return result;
    }
    
}
