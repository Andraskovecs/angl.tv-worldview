package com.angl.angldao.dao;

import com.angl.angldao.main.VideoStatistic;
import com.angl.angldao.models.Comment;
import com.angl.angldao.models.Echo;
import com.angl.angldao.models.Interest;
import com.angl.angldao.enums.Statistictype;
import com.angl.angldao.models.User;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videohistory;
import com.angl.angldao.models.Videopopularity;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class VideoDAO extends CommonDAO{
    public static final String IP="ip", USER="user";
    public static int historyCount = 10;
    public VideoDAO() {
        super();
    }

    @Override
    public void updateEntity(Object entity) throws HibernateException {
        if(entity instanceof Video){
            Video v = (Video)entity;
            super.updateEntity(v);
            super.saveEntity(new Videopopularity(v, Statistictype.provider, new Date(), v.getWatched()));
        }else{
            super.updateEntity(entity);
        }
    }

    @Override
    public void saveEntity(Object entity) throws HibernateException {
        if(entity instanceof Video){
            Video v = (Video)entity;
            super.saveEntity(v);
            super.saveEntity(new Videopopularity(v, Statistictype.provider, new Date(), v.getWatched()));
        }else{
            super.saveEntity(entity);
        }
    }
    public Boolean saveEcho(Echo echo) throws HibernateException{
        Boolean isOk = false;
        try{
            if(this.getOneByVideoId(echo.getVideo().getVideoid()) != null && !this.isExistsEcho(echo)){
                this.openTransaction();
                this.session.save(echo);
                this.transaction.commit();
                isOk = true;
            }
        } catch(Exception e) {
            if (transaction != null) {this.transaction.rollback();}
            e.printStackTrace();
        }
        return isOk;
    }
    public Boolean isExistsEcho(Echo echo){
        List result = new ArrayList();
        if(echo.getUser() != null){
            result = session.createCriteria(Echo.class).add(Restrictions.eq("video", echo.getVideo())).add(Restrictions.eq("user", echo.getUser())).list();
        }else{
            result = session.createCriteria(Echo.class).add(Restrictions.eq("video", echo.getVideo())).add(Restrictions.eq("ip", echo.getIp())).list();
        }
        if(result.isEmpty()){return false;}
        else{
            try{
                this.openTransaction();
                System.out.println(result);
                Object v = session.load(Echo.class,((Echo)result.get(0)).getId());
                session.delete(v);
                this.transaction.commit();
            }catch (Exception e) {
                if (transaction != null && transaction.wasCommitted()) {transaction.rollback();}
            }
            return true;
        }
    }
    
    public List<Comment> getCommentsByVideoId(String videoId) {
        List<Comment> result = new ArrayList<>();
        Object video = session.createCriteria(Video.class).add(Restrictions.eq("videoid", videoId)).uniqueResult();
        if(video instanceof Video){
            result = session.createCriteria(Comment.class).add(Restrictions.eq("video", video)).addOrder(Order.asc("time")).list();
        }
        return result;
    }
    
    public Boolean isExistsHistory(Videohistory videoHistory){
        List result = new ArrayList();
        if(videoHistory.getUser() != null){
            result = session.createCriteria(Videohistory.class).add(Restrictions.eq("video", videoHistory.getVideo())).add(Restrictions.eq("user", videoHistory.getUser())).list();
        }else{
            result = session.createCriteria(Videohistory.class).add(Restrictions.eq("video", videoHistory.getVideo())).add(Restrictions.eq("ip", videoHistory.getIp())).list();
        }
        return (result.size()>0);
    }

    public List<Video> getVideoHistoryByUser(String userIdOrIp, String ipOrId){        
        List<Video> videoList = new ArrayList<>();
        Object data = userIdOrIp;
        if(ipOrId.equals(VideoDAO.USER)){
            data = session.createCriteria(User.class).add(Restrictions.eq("id", Integer.parseInt((String)data))).uniqueResult();
        }
        List<Videohistory> result = session.createCriteria(Videohistory.class)
                .add(Restrictions.eq(ipOrId, data)).addOrder(Order.desc("watchdate")).setMaxResults(VideoDAO.historyCount).list();
        result.forEach((videoHistory)->{
            if(videoHistory.getVideo() != null){
                videoList.add(videoHistory.getVideo());
            }
        });
        return videoList;
    }    
    

    public Video getOneByVideoId(String videoId){
        Object result = session.createCriteria(Video.class)
                .add(Restrictions.eq("videoid", videoId)).list();
        if(!((List)result).isEmpty()){
            return (((List)result).get(0) instanceof Video?(Video)((List)result).get(0):null);
        }
        return null;
    }
    public List<Video> getOneToProvider(String providertype){
        List<Video> result = session.createCriteria(Video.class)
                .add(Restrictions.eq("providertype", providertype)).addOrder(Order.desc("publishdate")).list();
        
        return result;
    }
    public List<Video> getVideosToFacebook(Double west, Double east, Double south, Double north){
        List<Video> result = session.createCriteria(Video.class)
                .add(Restrictions.eq("providertype", "facebook"))
                .add(Restrictions.gt("longitude", west))
                .add(Restrictions.lt("longitude", east))
                .add(Restrictions.gt("latitude", south))
                .add(Restrictions.lt("latitude", north))
                .addOrder(Order.desc("publishdate")).list();
        
        return result;
    }
    public List<Video> getAllisAlived(){
        List<Video> result = session.createCriteria(Video.class)
                .add(Restrictions.eq("live", true))
                .addOrder(Order.desc("publishdate")).list();        
        return result;
    }
	
    public List<Video> getAllVideoByDay(Date date){
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date minDate = formatter.parse(formatter.format(date));
            Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
            maxDate = formatter.parse(formatter.format(maxDate));
            
            List<Video> result = session.createCriteria(Video.class)
                    .add(Restrictions.gt("publishdate", minDate))
                    .add(Restrictions.lt("publishdate", maxDate))
                    .add(Restrictions.eq("live", true))
                    .addOrder(Order.desc("publishdate")).list();
            return result;
        } catch (ParseException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<Video>();
        }
    }
    
     public List<Video> getAllVideoForSpace(){
        List<Video> result = session.createCriteria(Video.class)
                .add(Restrictions.not(Restrictions.like("providertype", "instagram")))
                .addOrder(Order.desc("publishdate")).list();
        return result;
    }

    public List<Video> getVideosByIds(List<Integer> ids){
        Criteria c = session.createCriteria(Video.class);
        Disjunction d = Restrictions.disjunction();
        for(Integer id:ids){
            d.add(Restrictions.eq("id", id));
        }
        c.add(d);
        List<Video> result = c.list();
        return result;
    }
    
    public List<Video> getVideoListByLimit(Date from, int count){
        List<Video> result = this.session.createCriteria(Video.class).add(Restrictions.lt("publishdate", from)).addOrder(Order.desc("publishdate")).setMaxResults(count).list(); 
        return result;
    }
    public List<Video> getNewVideoListByLimit(Date from, int count){
        List<Video> result = this.session.createCriteria(Video.class).add(Restrictions.gt("publishdate", from)).addOrder(Order.asc("publishdate")).setMaxResults(count).list(); 
        return result;
    }
    
    public List<Video> getAllVideo() {
        List<Video> result = this.session.createCriteria(Video.class).addOrder(Order.desc("publishdate")).list(); 
        return result;
    }
    	
    public void saveStatistic(Video v, Videopopularity vp){
        try{
            this.openTransaction();
            if(this.getOneByVideoId(v.getVideoid()) != null){
                this.session.save(vp);
            }
            this.transaction.commit();
        } catch(Exception e) {
            if (transaction != null) {this.transaction.rollback();}
            e.printStackTrace();
        }
    }
        
    public Interest saveInterest(Interest interest){
        List<Interest> result = this.session.createCriteria(Interest.class).add(Restrictions.eq("name", interest.getName())).list();
        if(result.isEmpty()){
            this.saveEntity(interest);
            return interest;
        }
        return result.get(0);
    }
    
    public void deleteVideo(Video video){        
        try{
            Object v = session.load(Video.class, video.getId());
            if(v != null){
               this.openTransaction();
               session.delete(v);
               this.transaction.commit();
            }
        }catch (Exception e) {
            if(this.transaction != null){this.transaction.rollback();}
            System.out.println("Delete exception: "+e.getMessage());
        } 
    }  
    
    public List<VideoStatistic> getStatisticByDb(){
        ProjectionList projectionList = Projections.projectionList()
        .add(Projections.groupProperty("providertype"))
        .add(Projections.rowCount());
        List<Object[]> results = session.createCriteria(Video.class).setProjection(projectionList).list();
        List<VideoStatistic> response = new ArrayList<>();
        results.forEach(data -> {
            response.add(new VideoStatistic(String.valueOf(data[0]), String.valueOf(data[1])));
        });
        return response;
    }
}
