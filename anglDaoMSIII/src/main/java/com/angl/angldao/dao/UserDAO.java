package com.angl.angldao.dao;

import com.angl.angldao.models.Role;
import com.angl.angldao.models.User;
import java.util.List;
import java.util.UUID;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class UserDAO extends CommonDAO{

    public UserDAO() {
        super();
    }
    
    public Boolean isExistsUser(User user) throws HibernateException{
        System.out.println(user.getEmail());
        System.out.println(user.getLoginname());
        Criterion rest1 = Restrictions.eq("loginname", user.getLoginname());
        Criterion rest2 = Restrictions.eq("email", user.getEmail());
        List<User> result = session.createCriteria(User.class)
                .add(Restrictions.or(rest1, rest2)).list();
        System.out.println(result);
        return (!result.isEmpty());
    }
        
    public User getOneById(String userId){
        List result = session.createCriteria(User.class)
                .add(Restrictions.eq("id", userId)).list();
        if(!result.isEmpty()){
            return (result.get(0) instanceof User?(User)result.get(0):null);
        }
        return null;
    }
    
    public User getOneByToken(String token){
        List result = session.createCriteria(User.class)
                .add(Restrictions.eq("token", token)).list();
        if(!result.isEmpty()){
            return (result.get(0) instanceof User?(User)result.get(0):null);
        }
        return null;
    }
    public User getOneByMailConfirm(String emailToken){
        List result = session.createCriteria(User.class)
                .add(Restrictions.eq("emailconfirmed", emailToken)).list();
        if(!result.isEmpty()){
            return (result.get(0) instanceof User?(User)result.get(0):null);
        }
        return null;
    }
    public User getUserByLogin(String name, String password){
        List result = session.createCriteria(User.class)
                .add(Restrictions.eq("loginname", name))
                .add(Restrictions.eq("password", password)).list();
        if(!result.isEmpty()){
            return (result.get(0) instanceof User?(User)result.get(0):null);
        }
        return null;
    }
    public Role getRoleByName(String name){
         List result = session.createCriteria(Role.class)
                .add(Restrictions.eq("role", name)).list();
        if(!result.isEmpty()){
            return (result.get(0) instanceof Role?(Role)result.get(0):null);
        }
        return null;
    }

    public String getUniqueGuid() {
        List result;
        String guid;
        do{
            guid = UUID.randomUUID().toString().replaceAll("-", "");
            result = session.createCriteria(User.class).add(Restrictions.eq("guid", guid)).list();
        }while(!result.isEmpty());
        return guid;
    }

    public String getToken() {
        List result;
        String token;
        do{
            token = UUID.randomUUID().toString().replaceAll("-", "");
            result = session.createCriteria(User.class).add(Restrictions.eq("token", token)).list();
        }while(!result.isEmpty());
        return token;
    }
    
}
