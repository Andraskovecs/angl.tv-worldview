package com.angl.angldao.dao;

import java.util.List;
import org.hibernate.HibernateException;

public interface DataAccessObject {

    public void openSession() throws HibernateException;

    public void closeSession();

    public List<Object> getAll(Class clazz);

    public void saveEntity(Object entity) throws HibernateException;

    public void updateEntity(Object entity) throws HibernateException;
}
