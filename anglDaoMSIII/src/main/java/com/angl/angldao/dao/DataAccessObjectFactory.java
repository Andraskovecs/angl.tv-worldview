package com.angl.angldao.dao;

public class DataAccessObjectFactory {
    public DataAccessObject getDataAccessObject(Class daoClass){
        if(daoClass == null){return null;}
        if(daoClass.equals(VideoDAO.class)){
            return new VideoDAO();
        }else if(daoClass.equals(UserDAO.class)){
            return new UserDAO();
        }else if(daoClass.equals(AggregationDAO.class)){
            return new AggregationDAO();
        }else{
            return new CommonDAO();
        }
    }
}
