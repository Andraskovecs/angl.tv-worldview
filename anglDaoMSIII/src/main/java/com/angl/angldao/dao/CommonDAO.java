package com.angl.angldao.dao;

import java.util.List;
import java.util.logging.Level;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class CommonDAO implements DataAccessObject{
    private static SessionFactory sessionFactory = null;
    protected Session session = null;
    protected Transaction transaction = null;
    public CommonDAO(){}

    @Override
    public void openSession() throws HibernateException{
        //java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
        if(CommonDAO.sessionFactory == null){CommonDAO.buildSessionFactory();}
        this.session = CommonDAO.sessionFactory.openSession();
    }
    @Override
    public void closeSession(){
        if(this.session.isOpen()){
            this.session.close();
        }
    }
    protected void openTransaction(){   
        this.transaction = session.beginTransaction();        
    }
    @Override
    public List<Object> getAll(Class clazz) {        
        System.out.println("clazz: "+clazz);
        List<Object> result = this.session.createCriteria(clazz).list(); 
        return result;
    }
    @Override
    public void saveEntity(Object entity) throws HibernateException{
        try{ 
            this.openTransaction();
            this.session.save(entity);
            this.transaction.commit();
        }catch(HibernateException ex){
            if (transaction != null) {this.transaction.rollback();}
            ex.printStackTrace();
        }
    }
    @Override
    public void updateEntity(Object entity)throws HibernateException{
       try{
            this.openTransaction();
            this.session.update(entity);
            this.transaction.commit();
        }catch(HibernateException ex){
            if (transaction != null) {this.transaction.rollback();}
            ex.printStackTrace();
        }
    }

    private static void buildSessionFactory() throws HibernateException{
        Configuration configuration = new Configuration().configure();
        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
        CommonDAO.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }
}
