/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.enums;

/**
 *
 * @author Kanyó Krisztián
 */
public enum Logtype{
    echo {
        public String toString() {
            return "echo";
        }
    },
    echoRemove {
        public String toString() {
            return "echoRemove";
        }
    },
    comment {
        public String toString() {
            return "comment";
        }
    },
    videoOpen {
        public String toString() {
            return "videoOpen";
        }
    },
    videoStart {
        public String toString() {
            return "videoStart";
        }
    },
    videoStop {
        public String toString() {
            return "videoStop";
        }
    },
    recommendedOpen {
        public String toString() {
            return "recommendedOpen";
        }
    }
}
