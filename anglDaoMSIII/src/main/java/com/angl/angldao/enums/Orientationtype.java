/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.enums;

/**
 *
 * @author Kanyó Krisztián
 */
public enum Orientationtype {
    landscape {
        public String toString() {
            return "landscape";
        }
    },

    portrait {
        public String toString() {
            return "portrait";
        }
    },

    undefined {
        public String toString() {
            return "undefined";
        }
    }
}
