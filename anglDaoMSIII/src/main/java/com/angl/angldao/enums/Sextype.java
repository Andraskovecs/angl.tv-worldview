/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.enums;

/**
 *
 * @author Kanyó Krisztián
 */
public enum Sextype {
    male {
        public String toString() {
            return "male";
        }
    },

    female {
        public String toString() {
            return "female";
        }
    }
}
