package com.angl.angldao.main;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;

public class LocationHelper {
    private static @Getter @Setter String API_KEY = "AIzaSyBT9HFeqVUMt4exDI_mWvMMeeDUtCU7el4";
    private static final String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=false&key=%s";
    private static final String USER_AGENT = "Mozilla/5.0";
    private final String callable, lat, lon;    
    private final JsonParser parser = new JsonParser();
    private final Gson gson = new Gson();
    
    public LocationHelper(String lat, String lon) {
        this.lon = lon;
        this.lat = lat;
        this.callable = String.format(URL, lat,lon, API_KEY);
    }
    public String getFormattedAddress(){
        try {
            HttpURLConnection connection = null;
            URL urlObject = new URL(callable);
            connection = (HttpURLConnection) urlObject.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", USER_AGENT);
            connection.setRequestProperty("Accept-Charset", "UTF-8"); 

            int responseCode = connection.getResponseCode();
            if(responseCode != 200){throw new Exception("The connection response code is not OK. I got "+responseCode);}

            StringBuilder responseString;
            try (BufferedReader inputReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"))) {
                String inputLine;
                responseString = new StringBuilder();
                while ((inputLine = inputReader.readLine()) != null) {
                    System.out.println(inputLine);
                    responseString.append(inputLine);
                }
            }
            JsonObject jsonRespons= parser.parse(responseString.toString()).getAsJsonObject();
            String status = jsonRespons.get("status").getAsString();
            String location = lat+","+lon;
            if(status.equals("OK")){
                JsonElement map = jsonRespons.get("results");
                if(map.isJsonArray()){
                    int index = 0;
                    if(map.getAsJsonArray().size()>1){
                        index = 1;
                    }
                    JsonElement array =  map.getAsJsonArray().get(index);
                    if(array.getAsJsonObject().has("formatted_address")){
                        location = array.getAsJsonObject().get("formatted_address").getAsString();
                    }
                }
            }
            return location;
        }   catch (Exception ex) {
            Logger.getLogger(LocationHelper.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return "unknown";
    }    
}
