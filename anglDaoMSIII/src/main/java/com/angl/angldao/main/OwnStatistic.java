/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.angldao.main;

import com.angl.angldao.enums.Statistictype;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videopopularity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import sun.util.calendar.BaseCalendar;

/**
 *
 * @author Kanyó Krisztián
 */
public class OwnStatistic {
    private @Getter int ownAvgView;
    private @Getter int ownMaxView;
    private @Getter List<Integer> ownViewDifferences = new ArrayList<Integer>();
    private @Getter List<Double> ownViewSteppness = new ArrayList<Double>();    
    
    private @Getter int ownAvgComment;
    private @Getter int ownMaxComment;
    private @Getter List<Integer> ownCommentDifferences = new ArrayList<Integer>();
    private @Getter List<Double> ownCommentSteppness = new ArrayList<Double>();
    
    private @Getter int ownAvgEcho;
    private @Getter int ownMaxEcho;
    private @Getter List<Integer> ownEchoDifferences = new ArrayList<Integer>();
    private @Getter List<Double> ownEchoSteppness = new ArrayList<Double>();
    
    private @Getter int providerAvgView;
    private @Getter int providerMaxView;
    private @Getter List<Integer> providerViewDifferences = new ArrayList<Integer>();
    private @Getter List<Double> providerViewSteppness = new ArrayList<Double>();
    
    private @Getter Video video;
    private @Getter Set<Videopopularity> popularitySet;
    private int historyLimit;
    
    private @Getter Double similarityPoint = 0.0;
    
    
    public OwnStatistic(Video v, Set<Videopopularity> vpSet, int historyLimit){
        this.video = v;
        this.popularitySet = vpSet;
        this.historyLimit = historyLimit;
        this.calculateStepness();
    }
    
    public void addSimilarityPoint(Double value){
        this.similarityPoint += value;
    }
    
    
    
    private void calculateStepness(){
        List<Videopopularity> ownVp = new ArrayList<Videopopularity>();
        List<Videopopularity> providerVp = new ArrayList<Videopopularity>();
        for (Videopopularity vp : this.popularitySet) {
            if(vp.getStatistictype().equals(Statistictype.own)) ownVp.add(vp);
            else if(vp.getStatistictype().equals(Statistictype.provider)) providerVp.add(vp);
        }
        Collections.sort(ownVp, STATISTIC_ORDER);
        Collections.sort(providerVp, STATISTIC_ORDER);
        
        Statistictype[] statistictypeEnums = Statistictype.values();
        for (Statistictype statistictype : statistictypeEnums) {
            List<Videopopularity> tmpVp = 
                    statistictype.equals(Statistictype.own) ? new ArrayList<Videopopularity>(ownVp) : 
                    statistictype.equals(Statistictype.provider) ? new ArrayList<Videopopularity>(providerVp) : 
                    new ArrayList<Videopopularity>();
            int max = tmpVp.size() > this.historyLimit ? this.historyLimit : tmpVp.size();
            int prevComment = 0, prevView = 0;
            long prevTime = 0;
            for(int i = 0; i < max; i++){
                int curComment = tmpVp.get(i).getComments(), curView = tmpVp.get(i).getViews();
                long curTime = tmpVp.get(i).getStatistictime().getTime();
                if(statistictype.equals(Statistictype.own)){
                    if(this.ownMaxComment < curComment) this.ownMaxComment = curComment;
                    if(this.ownMaxView < curView) this.ownMaxView = curView;
                    
                    this.ownAvgView = (this.ownAvgView * i + curView) / (i+1);
                    this.ownAvgComment = (this.ownAvgComment * i + curView) / (i+1);
                    
                    if(i > 0){
                        this.ownViewSteppness.add(this.calculateSteppness(prevTime, curTime, prevView, curView));
                        this.ownViewDifferences.add(prevView - curView);
                        this.ownCommentSteppness.add(this.calculateSteppness(prevTime, curTime, prevComment, curComment));
                        this.ownCommentDifferences.add(prevComment - curComment);
                    }
                }
                else if(statistictype.equals(Statistictype.provider)){
                    if(this.providerMaxView < curView) this.providerMaxView = curView;
                    this.providerAvgView = (this.providerAvgView * i + curView) / (i+1);
                    
                    if(i > 0){
                        this.providerViewSteppness.add(this.calculateSteppness(prevTime, curTime, prevView, curView));
                        this.providerViewDifferences.add(prevView - curView);
                    }
                }
                prevComment = curComment; prevView = curView; prevTime = curTime;
            }
        }
    }
    
    private double calculateSteppness(long x1, long x2, int y1, int y2){
        Double diffX = ((Long)(x1-x2)).doubleValue();
        Double diffY = (double)(y1-y2);
        return diffY / diffX;
    }
    
    static final Comparator<Videopopularity> STATISTIC_ORDER = 
                                        new Comparator<Videopopularity>() {
            public int compare(Videopopularity vp1, Videopopularity vp2) {
                return vp2.getStatistictime().compareTo(vp1.getStatistictime());
            }
    };
}
