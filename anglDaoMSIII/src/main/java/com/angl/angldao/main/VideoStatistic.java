package com.angl.angldao.main;

import lombok.Getter;
import lombok.Setter;

public class VideoStatistic {
    private @Setter @Getter String key, value;

    public VideoStatistic(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public VideoStatistic() {
    }
    
}
