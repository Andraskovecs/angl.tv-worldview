import com.angl.angldao.aggregation.*;
import com.angl.angldao.dao.CommonDAO;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.UserDAO;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.main.LocationHelper;
import com.angl.angldao.models.Advertiser;
import com.angl.angldao.models.Advertiseruser;
import com.angl.angldao.enums.Orientationtype;
import com.angl.angldao.models.Role;
import com.angl.angldao.enums.Sextype;
import com.angl.angldao.models.User;
import com.angl.angldao.models.Userdetails;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videodetails;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.internal.ir.annotations.Ignore;
import junit.framework.TestCase;
import org.hibernate.HibernateException;

public class DaoTest extends TestCase{
    
    public void testVideo() {
//        DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
//        dao.openSession();
//        Video v = ((VideoDAO)dao).getOneByVideoId("17910826720095362");
//        ((VideoDAO)dao).deleteVideo(v);
//        dao.closeSession();
    }
//    public void testAdvertiser(){
////        try {
////            DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(Advertiser.class);
////            dao.openSession();            
////            List list = dao.getAll(Advertiseruser.class);
////            for(Object a : list){
////                System.out.println(a.toString());
////            }            
////            dao.closeSession();
////            dao = new DataAccessObjectFactory().getDataAccessObject(Role.class);
////            Role r = new Role("user");
////            dao.openSession();
////            dao.saveEntity(r);
////            list = dao.getAll(Role.class);
////            for(Object a : list){
////                System.out.println(a.toString());
////            }
////            dao.closeSession();
//////            
//////            Integer userId = new UserDAO().openSessionFactory().openTransaction().saveEntity(u);
//////            Advertiser advertiser = new Advertiser("Teszt feltöltés", "sajt.jpg", new Date());
//////            Integer advId = new AdvertiserDAO().openSessionFactory().openTransaction().saveEntity(advertiser);
//////            Advertiseruser advU = new Advertiseruser(advertiser, u);
//////            Integer aduUId = new AdvertiseruserDAO().openSessionFactory().openTransaction().saveEntity(advU);
//////            List list = new AdvertiseruserDAO().openSessionFactory().openTransaction().getAll();
//////            
////        } catch (Exception ex) {
////            Logger.getLogger(DaoTest.class.getName()).log(Level.SEVERE, null, ex);
////        }
//    }
    

//    public void testCommonAggregationTest(){
//        AggregationObject ao = new AggregationObjectFactory(3,2).getAggregationObject(CommonAggregation.class);
//        List<List<Video>> videoList = ao.getAllVideo();
//        this.soutVideoNames(videoList);
//    }
    
//    public void testVideoDetailsTest(){
//        DataAccessObject dao = null;
//        DataAccessObject commonDao = null;
//        try {
//            System.out.println("videoDetailsTest");
//            dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);   
//            commonDao = new DataAccessObjectFactory().getDataAccessObject(CommonDAO.class);  
//            dao.openSession();
//            commonDao.openSession();
//            List<Video> videos = ((VideoDAO)dao).getAllVideo();
//            Video v = videos.get(0);
//            Videodetails vd = new Videodetails(v, (short)18, Sextype.female, null, null, null, null, null, (short)400, (short)600, Orientationtype.portrait);
//            commonDao.saveEntity(vd);
//            System.out.println("VideoDetails saved");
//            
//            List<Object> userList = commonDao.getAll(User.class);
//            User u = (User)userList.get(0);
//            Userdetails ud = new Userdetails(u, "Teszt", "Elek", (short)21, null, Sextype.male, null, null);
//            commonDao.saveEntity(ud);
//            System.out.println("UserDetails saved");
//            
//        } catch (Exception ex) {
//            Logger.getLogger(DaoTest.class.getName()).log(Level.SEVERE, null, ex);
//        } finally{
//            dao.closeSession();
//            commonDao.closeSession();
//        }
//    }
    
//    public void testBasicAggregationTest() {
//        AggregationObject ao = new AggregationObjectFactory(10,2).getAggregationObject(BasicAggregation.class);
//
////        boolean result = ao.doAggregation();
////        if(result) System.out.println("OK");
////        else System.out.println("FAIL");
//        
//        List<List<Video>> videoList = ao.getAllVideo();
//        System.out.println("Videó klaszterek száma: " + videoList.size());
//        this.soutVideoNames(videoList);
//    }
    
//    public  void testGetSetMatrix() throws IOException {
//        long start = System.nanoTime();
//        final long used0 = usedMemory();
//        LargeDoubleMatrix matrix = new LargeDoubleMatrix("tmp/ldm.test", 48*1000, 50*1000);
//        System.out.println("Matrix width: "+ matrix.width() + " height: "+matrix.height());
////        for(int i=0;i<matrix.width();i++)
////            matrix.set(i,i,i);
////        for(int i=0;i<matrix.width();i++)
////            assertEquals(i, matrix.get(i,i), 0.0);
//        int width = matrix.width()-1000;
//        int height = matrix.height()-1000;
//        System.out.println("W:"+width+" - H:"+height+ " - Val:"+matrix.get(width, height));
//        for(int i = 0; i < matrix.width(); i++){
//            for(int j = 0; j < matrix.height(); j++){
//                double value = 0.0;
//                value = ((double)i / matrix.width() + (double)j / matrix.height()) / 2;
//                System.out.println("W:"+i+" - H:"+j+ " - Val:"+value);
//                matrix.set(i, j, value);
//            } 
//        }
//        System.out.println("W:"+width+" - H:"+height+ " - Val:"+matrix.get(width, height));
//        System.out.println("True value: "+((double)width / matrix.width() + (double)height / matrix.height()) / 2);
//        long time = System.nanoTime() - start;
//        final long used = usedMemory() - used0;
//        if (used==0)
//            System.err.println("You need to use -XX:-UsedTLAB to see small changes in memory usage.");
//        System.out.printf("Setting the diagonal took %,d ms, Heap used is %,d KB%n", time/1000/1000, used/1024);
//        matrix.close();
//    }

    private long usedMemory() {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }
    
    public void testPopularityAggregationTest() {
//       DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(Advertiser.class);
//            dao.openSession();            
//            List list = dao.getAll(User.class);
//            dao.closeSession();
//            System.out.println(list);
//        LocationHelper lh = new LocationHelper("47.4729369", "19.0555075");
//        System.out.println(lh.getFormattedAddress());
//        try {
//            AggregationObject ao = new AggregationObjectFactory().getAggregationObject(SpaceAggregation.class);
            
//            boolean result = ao.doAggregation();
//            if(result) System.out.println("OK");
//            else System.out.println("FAIL");

//            DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
//            Date date = format.parse("2018/03/06");
//            List<Video> videoList = ao.getAllVideo(300, date);
//List<SpaceResult> videoList = ao.getSpaces();
//            System.out.println("Videók száma: " + videoList.size());
//            for (SpaceResult spaceResult : videoList) {
//                System.out.println(spaceResult.getAggregationtype());
//            }
//            
//        } catch (Exception ex) {
//            Logger.getLogger(DaoTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
    
        
    public void testSpaceAggregationTest() {
//            DataAccessObject dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);
//            dao.openSession();
//                Video v = ((VideoDAO)dao).getOneByVideoId("938928676274215");
//                ((VideoDAO)dao).deleteVideo(v);
//                v = ((VideoDAO)dao).getOneByVideoId("938928676274215");
//                System.out.println(v);
//            dao.closeSession();
    }
    
    public void testBasicAggregationTest() {
//        LocationHelper lh = new LocationHelper("-0.2416796", "51.5285582");
//        System.out.println(lh.getFormattedAddress());
    }
    
    
    private void soutVideoNames(List<Video> videoList, int limit){
        if(videoList.isEmpty()) return;
        if(videoList.size() < limit ) limit = videoList.size();
        for (int i = 0; i < limit; i++) {
            System.out.println("Videó title: " + videoList.get(i).getTitle());
        }      
    }
}
