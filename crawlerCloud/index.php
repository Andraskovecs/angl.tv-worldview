<?php
session_start();
?>
<html>
    <head>
        <title>Crawler Cloude</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
        if(array_key_exists('menu', $_GET)&& $_GET['menu'] == 'logout'){
          session_destroy();
          header('Location: /');
        }else{          
          include 'lib/User.php';          
          if(array_key_exists('user', $_SESSION) && $_SESSION['user'] != NULL){
            include 'pages/servers.php';
          }else{
            if(array_key_exists('submit', $_POST) && $_POST['submit'] == 'login'){
              $username = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
              $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
              $user = User::login($username, $password);
              if($user != null){
                $_SESSION['user'] = serialize($user);
              }
              header('Location: /');
            }
            include 'pages/login.php';
          }
        }
        ?>
    </body>
    
</html>
