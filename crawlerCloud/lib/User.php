<?php
class User {
  public $username, $password;
  
  function __construct($username) {
    $this->username = $username;
  }
  function getUsername() {
    return $this->username;
  }
  
  public static $users = array(
      array('username'=>'mpalfy', 'password'=>'1q2w3e4r'),
      array('username'=>'kanyo', 'password'=>'1q2w3e4r'),
      array('username'=>'hans', 'password'=>'1q2w3e4r'),
      array('username'=>'andras', 'password'=>'1q2w3e4r'));
  
  public static function login($username, $password){
    foreach (self::$users as $user) {
      if($user['username'] == $username && $user['password']== $password)return new User($username);
    }
    return NULL;
  }
}
