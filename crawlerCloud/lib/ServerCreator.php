<?php
/**
 * Description of ServerCreator
 *
 * @author hans
 */
class ServerCreator {
  const SERVERDIR = "servers/";
  private $serverDir = "servers/", $templateDir = "servers/template/";
  private $checkURL = "/service/";
  private $serverName, $serverIp, $serverPort;
  private $error = null;
  private $mkDir = null;
  function __construct($serverName = null, $serverIp = null, $port = null) {
    $this->serverName = $serverName;
    $this->serverIp = $serverIp;
    $this->serverPort = $port;
    if($serverIp != null){$this->validation();}
  }
  
  public function deleteServer($pathToDelete){
    $rmdir= $this->serverDir.$pathToDelete;
    if(!file_exists($rmdir))return "Nem létezik a megadott elérés";
    $this->rrmdir($rmdir);
    return "OK";
  }
  
  public function createServer(){
    if($this->error != null)return $this->error;
    mkdir($this->mkDir, 0777);
    $this->xcopy($this->templateDir, $this->mkDir);
    $indexHtml = str_replace("[[serverName]]", $this->serverName, Template::$indexHTML);
    $myfile = fopen($this->mkDir."/index.html", "w") or die("Unable to open file!");
    fwrite($myfile, $indexHtml);
    fclose($myfile);
    $rest = str_replace("[[URL]]", ('http://'.$this->serverIp.':'.$this->serverPort.'/'), Template::$restJS);    
    $myfile = fopen($this->mkDir."/js/RestFactory.js", "w") or die("Unable to open file!");
    fwrite($myfile, $rest);
    fclose($myfile);
    return "OK";
  }
  
  public function validation(){
    if (!filter_var($this->serverIp, FILTER_VALIDATE_IP)) {
      $this->error = "Not valid ip address: ".$this->serverIp;
    }
    $this->mkDir = $this->serverDir.$this->slugCode($this->serverName).$this->slugCode($this->serverIp);
    if(is_dir($this->mkDir)) {  
      $this->error = "Server is existing!";
    }
    $response = json_decode(file_get_contents('http://'.$this->serverIp.':'.$this->serverPort.$this->checkURL)) or die("<h1>Server is not running</h1>");
    if($response->message != "Run"){
      $this->error = "Server is not running";
    }
  }
  private function xcopy($src, $dest) {
    foreach (scandir($src) as $file) {
      if(($file != '.') && ($file != '..')){
        if (!is_readable($src . '/' . $file)) continue;
        if (is_dir($src .'/' . $file) && ($file != '.') && ($file != '..') ) {
            mkdir($dest . '/' . $file);
            $this->xcopy($src . '/' . $file, $dest . '/' . $file);
        } else {
            copy($src . '/' . $file, $dest . '/' . $file);
        }
      }
    }
  }
  private function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir."/".$object))
           $this->rrmdir($dir."/".$object);
         else
           unlink($dir."/".$object); 
       } 
     }
     rmdir($dir); 
   } 
 }
  private function slugCode($str){
     $ekezet = array("Ö", "ö", "Ü", "ü", "Ó", "ó", "O", "o", "Ú", "ú", "Á", "á", "U", "u", "É", "é", "Í", "í", 
         "/", "ő", "Ű", "ű", "Ő", "Ä", 
         " ", ",", "(", ")","{", "}",
        ".", "@", "?", "#", '"', "'");
    $nekezet = array("O", "o", "U", "u", "O", "o", "O", "o", "U", "u", "A", "a", "U", "u", "E", "e", "I", "i", 
        "", "o", "U", "u", "O", "A", 
        "-", "-", "", "","", "",
        "_", "", "", "", "", "");
    return(str_replace($ekezet, $nekezet, $str));
  }
}
