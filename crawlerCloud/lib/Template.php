<?php
/**
 * Description of Template
 *
 * @author hans
 */
class Template {
  public static $indexHTML = '<!DOCTYPE html>
<html ng-app="crawlerApp">
    <head>
        <title>Server - [[serverName]]</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-route.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-cookies.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/ng-stomp.standalone.min.js"></script>
        <script src="js/core.js"></script>
        <script src="js/RestFactory.js"></script>
    </head>
    <body>
        <div  class="container-fluid" style="margin: 0px !important; padding: 0px !important"> 
            <div ng-controller="headerController">
                <nav ng-if="userToken" class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#!/">Crawler service maintance</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Back to Servers</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#!/running_service">Running services</a></li>
                                        <li><a href="#!/new_service">New service</a></li>
                                        <li><a href="#!/killed_service">Killed service</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Aggregations <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#!/aggregation">Aggregation</a></li>
                                        <li><a href="#!/new_aggregation">New aggregation</a></li>
                                    </ul>
                                </li>
                                <li><a href="#!/settings">Settings</a></li>
                            </ul> 
                            <p  class="navbar-text navbar-right"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{userToken.email}}</p>
                            <p ng-repeat="s in stat" class="navbar-text navbar-right"><img style="height: 20px;" src="img/{{s.key}}_neg.png" alt="{{s.key}}"/>
                                    {{s.key}} video: {{s.value}}
                            </p>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
            <div ng-view>
            </div>
        </div>
    </body>
</html>';
  public static $restJS = "crawlerApp.factory('RestService', function(\$http, \$stomp) {
 var urlBase = '[[URL]]';
    var dataFactory = {};

    dataFactory.login = function (user) {
        return \$http.post(urlBase+'users/login',user);
    };
    dataFactory.getServices = function () {
        return \$http.get(urlBase + 'service/list');
    };
    dataFactory.getKilledServices = function () {
        return \$http.get(urlBase + 'service/killed_list');
    };
    dataFactory.startService = function (serviceData) {
        return \$http.post(urlBase+'service/start', serviceData);
    };
    dataFactory.restartService = function (serviceInfo) {
        return \$http.post(urlBase+'service/restart', serviceInfo);
    };
    dataFactory.subscribeing = function () {
        return \$stomp.connect(urlBase+'websocket', {});            
    };
    dataFactory.stopService = function (serviceInfo) {
        return \$http.post(urlBase + 'service/stop',serviceInfo);
    };
    dataFactory.pauseService = function (serviceInfo) {
        return \$http.post(urlBase + 'service/pause',serviceInfo);
    };
    dataFactory.startAggregationService = function (aggregationData) {
        return \$http.post(urlBase+'aggregation/start', aggregationData);
    };
    dataFactory.stopAggregation = function (aggregationData) {
        return \$http.post(urlBase + 'aggregation/stop',aggregationData);
    };
    dataFactory.pauseAggregation = function (aggregationData) {
        return \$http.post(urlBase + 'aggregation/pause',aggregationData);
    };
    dataFactory.settings = function (settingsData) {
        return \$http.post(urlBase + 'service/settings',settingsData);
    };
    dataFactory.getSettings = function () {
        return \$http.get(urlBase + 'service/settings');
    };
    dataFactory.getStats = function () {
        return \$http.get(urlBase + 'service/stats');
    };
    return dataFactory;
});";
}
