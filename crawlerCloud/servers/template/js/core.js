var crawlerApp = angular.module('crawlerApp', ["ngRoute", 'ngCookies', 'ngStomp']);

crawlerApp.config(function($routeProvider) {
    $routeProvider.when("/", {
        templateUrl : "sites/start.html",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }else{
                    $location.path('/running_service');
                }
            }
        }  
    }).when("/running_service", {
        templateUrl : "sites/serviceList.html",
        controller : "listController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/new_service", {
        templateUrl : "sites/newService.html",
        controller : "startController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/killed_service", {
        templateUrl : "sites/killed.html",
        controller : "killedListController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/aggregation", {
        templateUrl : "sites/aggregation.html",
        controller : "aggregationListController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/new_aggregation", {
        templateUrl : "sites/newAggregation.html",
        controller : "aggregationStartController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/settings", {
        templateUrl : "sites/staticConfig.html",
        controller : "settingsController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/login",{
        templateUrl : "sites/login.html",
        controller : "loginController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(userToken){ 
                    $location.path('/');
                    alert("U are loged in!");
                }
            }
        }  
    }).otherwise({redirectTo: '/'});
});
crawlerApp.controller('settingsController', function ($scope, RestService) {
    $scope.settings = {
         borderOfLifetime:666,
         twitterLong:666,
         minElementCountToCreate:666,
         periscopePlusHours:666,
         apiKey:666,
         plusPerEcho:666
    };
    RestService.getSettings().then(function(response){
        $scope.settings = response.data;
        console.log(response.data);
        $scope.$apply;
    });
    $scope.setStaticDataAsyncAsJSON = function(){
         RestService.settings($scope.settings).then(function (response){
             $scope.settings = response.data;
             alert("Sikeres beállítás!")
             $scope.$apply;
         });
     }
});
crawlerApp.controller('headerController', function ($scope, $cookies, RestService, $rootScope) {
     $scope.userToken = $cookies.getObject('userToken');
     $scope.stat = [{
         key:"Donald",
         value:"Kacsa"
     }];
    $rootScope.$on("login-done", function () {
        $scope.userToken = $cookies.getObject('userToken');
        RestService.getStats().then(function (response){
         $scope.stat = response.data;
        });
    });
    RestService.getStats().then(function (response){
         $scope.stat = response.data;
    });
});
crawlerApp.controller("listController", function ($scope, $cookies, $stomp, $rootScope, RestService) {    
    $scope.userToken = $cookies.getObject('userToken');
    $scope.crawlServices = {};
    $scope.crawlServices.logList = [];
    $scope.crawlServices.serviceList = [];
    $scope.labels = ["Update", "Insert", "Delete"];
    $scope.getExp = function(str) {
        var today = new Date();
        today.setHours(today.getHours() - 1);
        var startDate = new Date(str);
        var exp = today - startDate;  
        var timeDiff = Math.abs(today.getTime() - startDate.getTime());
        var diffDays = Math.round(timeDiff / (1000 * 3600 * 24));
        var time = new Date(exp);
        if(diffDays>2) return (diffDays-1)+" day and"+ time.getHours()+":"+time.getMinutes()+":"+time.getSeconds();  
        return time.getHours()+":"+time.getMinutes()+":"+time.getSeconds();  
    };
    $scope.stopService = function(crawlservice){
        RestService.stopService(crawlservice).then(function (response) {
                $scope.crawlServices.serviceList = response.data;
                console.log(response.data);
            }).catch (function(data){
                console.log(data);
            }); 
    };
    $scope.pauseService = function(crawlservice){
        RestService.pauseService(crawlservice).then(function (response) {
                $scope.crawlServices.serviceList = response.data;
                console.log(response.data);
            }).catch (function(data){
                console.log(data);
            }); 
    };
    RestService.subscribeing().then(function (frame) {
        $stomp.subscribe('/topic/log', function (response) {
            console.log(response);
            $scope.crawlServices.serviceList = response.services;
            $scope.crawlServices.logList.unshift(response.logMap);
            $rootScope.$apply();
        });
    });
});
crawlerApp.controller("killedListController", function ($scope, $cookies, $location, RestService) {    
    $scope.userToken = $cookies.getObject('userToken');
    $scope.crawlServices = {};
    $scope.crawlServices.serviceList = [];
    RestService.getKilledServices().then(function (response) {
        console.log(response);
        $scope.crawlServices.serviceList = response.data;
    });
    $scope.restartService = function(crawlservice){
        RestService.restartService(crawlservice).then(function (response) {            
            console.log(response.data);
            if(response.data.message == "Ok"){
                $location.path("/running_service");  
            }else{
                alert("Ups! Something went wrong!");
            }
        }).catch (function(data){
            alert("Ups! Something went wrong!");
        }); 
    };
});
crawlerApp.controller("loginController", function($scope, $cookies, $location, RestService, $rootScope){
    $scope.loginAsyncAsJSON = function(){		
            var dataObj = {
                email: $scope.loginname,
                password: $scope.password
            };
            console.log(dataObj);
            RestService.login(dataObj).then(function(data){                
                console.log(data);
                var today = new Date();
                var expiresValue = new Date(today);
                expiresValue.setSeconds(today.getSeconds() + 3600); 
                $cookies.putObject('userToken', data.data, {'expires': expiresValue});
                $rootScope.$broadcast("login-done");
                $location.path("/running_service");                
            }).catch (function(data){
                alert("Ups! Something went wrong!");
                console.log(data);
            });
	};
});
crawlerApp.controller("startController", function($scope, $cookies, $location, RestService, $route){
    $scope.userToken = $cookies.getObject('userToken');
    $scope.modeList = ["insert","update"];
    $scope.typeList = ["facebook","twitter", "instagram","custom", "periscope"];
    $scope.fbCoordinate = {east:"23.432491", west:"15.896219", north:"48.423464", south:"45.725798"};
    $scope.twitterCoordinate = {longitude:-77.0846157, latitude:38.8937091, radius:100};
    $scope.customData = {listUrl:'http://www.fakeprovider.lh/?menu=list', detailsUrl:'http://www.fakeprovider.lh/?menu=video&id=%s', providerName:'FAKE'};
    $scope.serviceType = "facebook";
    $scope.type = "facebook";
    $scope.startAsyncAsJSON = function(){	
        var dataObj = {
                name: (!$scope.name?$scope.type+"_"+$scope.mode:$scope.name),
                type: $scope.type,
                mode: $scope.mode,
                maxViewer: $scope.maxViewer,
                videoCount: $scope.videoCount
            };
        if($scope.type == "facebook"){
            dataObj.east = $scope.fbCoordinate.east;
            dataObj.west = $scope.fbCoordinate.west;
            dataObj.south = $scope.fbCoordinate.south;
            dataObj.north = $scope.fbCoordinate.north;
        }else if($scope.type == "twitter"){
            dataObj.latitude = $scope.twitterCoordinate.latitude;
            dataObj.longitude = $scope.twitterCoordinate.longitude;
            dataObj.radius = $scope.twitterCoordinate.radius;
        }else if($scope.type == "custom"){
            dataObj.listUrl = $scope.customData.listUrl;
            dataObj.detailsUrl = $scope.customData.detailsUrl;
            dataObj.providerName = $scope.customData.providerName;
        }
            console.log(dataObj);
            RestService.startService(dataObj).then(function(data){
                console.log(data);
                if(data.data.message == "Ok"){
                     $location.path("/running_service");   
                }
            }).catch (function(data){
                alert("Ups! Something went wrong!");
                console.log(data);
            });
	};
});
crawlerApp.controller("aggregationStartController", function($scope, $cookies, $location, RestService, $route){
    $scope.userToken = $cookies.getObject('userToken');
    $scope.dateToPopularity = new Date();
    $scope.aggregationTypeList = ["BasicAggregation","PopularityAggregation", "SpaceAggregation", "StatisticGenerator"];
    $scope.spacedata = {eps:0.5, minItemSpace:2};
    $scope.basicdata = {clusterNumber:10, minItem:2};
    $scope.onFileChange = function (fileEl) {
        console.log("sajt");
        var files = fileEl.files;
        var file = files[0];
        var reader = new FileReader();

        reader.onloadend = function(evt) {
          if (evt.target.readyState === FileReader.DONE) {
            $scope.$apply(function () {
              $scope.pointList = JSON.parse(evt.target.result);
            });
          }
        };

        reader.readAsText(file);
    };
    $scope.startAggregationAsyncAsJSON = function(){	
        
        var dataObj = {
            aggregationType: $scope.aggregationType,
        };
        if($scope.aggregationType == "BasicAggregation"){
            dataObj.clusterNumber= $scope.basicdata.clusterNumber;
            dataObj.minItem= $scope.basicdata.minItem;
        }else if($scope.aggregationType == "SpaceAggregation"){            
            if(!$scope.pointList){
                alert("U did not choose file!");
                return;
            }
            dataObj.pointList = $scope.pointList;
            dataObj.eps = parseFloat($scope.spacedata.eps);
            dataObj.minItem= $scope.spacedata.minItemSpace;
        }
        console.log(dataObj);
        RestService.startAggregationService(dataObj).then(function(response){
            console.log(response);
            if(response.data.message == "Ok"){
                $location.path("/aggregation");   
            }
        }).catch (function(data){
            alert("Ups! Something went wrong!");
            console.log(data);
        });
    };
});
crawlerApp.controller("aggregationListController", function ($scope, $cookies, $stomp, $rootScope, RestService) {    
    $scope.userToken = $cookies.getObject('userToken');
    $scope.aggreagtions = {};
    $scope.aggreagtions.logList = [];
    $scope.aggreagtions.aggregationList = [];
    
    $scope.stopAggr = function(aggregation){
        RestService.stopAggregation(aggregation).then(function (response) {
                $scope.aggreagtions.aggregationList = response.data;
                console.log(response.data);
            }).catch (function(data){
                console.log(data);
            }); 
    };
    $scope.pauseAggr = function(aggregation){
        RestService.pauseAggregation(aggregation).then(function (response) {
                $scope.aggreagtions.aggregationList = response.data;
                console.log(response.data);
            }).catch (function(data){
                console.log(data);
            }); 
    };
    RestService.subscribeing().then(function (frame) {
        $stomp.subscribe('/topic/aggregation', function (response) {
            console.log(response);
            $scope.aggreagtions.aggregationList = response.aggregationStarter;
            $scope.aggreagtions.logList.unshift(response.logMap);
            $rootScope.$apply();
        });
    });
});