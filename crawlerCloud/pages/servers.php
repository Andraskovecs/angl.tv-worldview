<?php
include 'lib/ServerCreator.php';
include 'lib/Template.php';
$post = filter_input(INPUT_POST, "submit", FILTER_SANITIZE_STRING);
if ($post == "submit") {
  $ip = filter_input(INPUT_POST, "ipAddress", FILTER_SANITIZE_STRING);
  $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
  $port = filter_input(INPUT_POST, "port", FILTER_SANITIZE_STRING);
  $creator = new ServerCreator($name, $ip, $port);
  $data = $creator->createServer();
  if ($data != "OK")echo $data;
  $_POST = null;
}else if ($post == "delete") {
  $name = filter_input(INPUT_POST, "deletePath", FILTER_SANITIZE_STRING);
  $creator = new ServerCreator();
  $data = $creator->deleteServer($name);
  if ($data != "OK")echo $data;
  $_POST = null;
}
?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Crawler Cloud</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Servers <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php
            foreach (scandir(ServerCreator::SERVERDIR) as $file) {
              if (($file != '.') && ($file != '..') && ($file != "template") && ($file != "index.php")) {
                echo '<li><a href="servers/' . $file . '/index.html">' . $file . '</a></li>';
              }
            }
            ?>
          </ul>
        </li>
        <li><a href="?menu=logout">Logout</a></li>        
      </ul> 
        <p class="navbar-text navbar-right">Signed in as <?php echo unserialize($_SESSION['user'])->username; ?></p>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form method="POST" action="index.php">
                <div class="form-group">
                  <input type="text" class="form-control" name="name" required="" placeholder="Server name">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="ipAddress" required="" placeholder="Ip address">
                </div>
                <div class="form-group">
                  <input type="number" class="form-control" min='8000' max="10000" name="port" required="" placeholder="Port number">
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-primary btn-sm" name="submit" value="submit" >
                </div>
            </form>
            <form method="POST" action="index.php">
                <?php
                foreach (scandir(ServerCreator::SERVERDIR) as $file) {
                  if (($file != '.') && ($file != '..') && ($file != "template") && ($file != "index.php")) {
                    echo '<div class="radio">
                          <label>
                            <input type="radio" name="deletePath" value="'.$file.'">
                            '.$file.'
                          </label>
                        </div>';

                  }
                }
                ?>                
                <div class="form-group">
                  <input type="submit" class="btn btn-primary btn-sm" name="submit" value="delete" >
                </div>
            </form>
        </div>
    </div>
</div>

