<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-3">
            <div style="margin-bottom: 20px;"><h2>Login</h2></div>
            <form method="POST" action="index.php">
                <div class="form-group">
                  <input class="form-control" type="text" name="name" required="" placeholder="User name">
                </div>
                <div class="form-group">
                  <input class="form-control" type="password" name="password" required="" placeholder="Password">
                </div>
                <div class="form-group">
                  <input class="btn btn-primary btn-sm" type="submit" name="submit" value="login" >
                </div>
            </form>
        </div>
    </div>
</div>
<?php
if ($_POST && $_SESSION['user'] == null) {
  echo '<p>Nem helyes adatok!</p>';
}
  