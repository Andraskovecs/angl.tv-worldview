var crawlerApp = angular.module('crawlerApp', ["ngRoute", 'ngCookies', 'ngStomp', "chart.js"]);
crawlerApp.factory('RestService', function($http, $stomp) {
 var urlBase = 'http://152.66.254.131:8899/';
    var dataFactory = {};

    dataFactory.login = function (user) {
        return $http.post(urlBase+'users/login',user);
    };
    dataFactory.getServices = function () {
        return $http.get(urlBase + 'service/list');
    };
    dataFactory.getKilledServices = function () {
        return $http.get(urlBase + 'service/killed_list');
    };
    dataFactory.startService = function (serviceData) {
        return $http.post(urlBase+'service/start', serviceData);
    };
    dataFactory.restartService = function (serviceInfo) {
        return $http.post(urlBase+'service/restart', serviceInfo);
    };
    dataFactory.subscribeing = function () {
        return $stomp.connect(urlBase+'websocket', {});            
    };
    dataFactory.stopService = function (serviceInfo) {
        return $http.post(urlBase + 'service/stop',serviceInfo);
    };
    dataFactory.pauseService = function (serviceInfo) {
        return $http.post(urlBase + 'service/pause',serviceInfo);
    };
    dataFactory.startAggregationService = function (aggregationData) {
        return $http.post(urlBase+'aggregation/start', aggregationData);
    };
    dataFactory.stopAggregation = function (aggregationData) {
        return $http.post(urlBase + 'aggregation/stop',aggregationData);
    };
    dataFactory.pauseAggregation = function (aggregationData) {
        return $http.post(urlBase + 'aggregation/pause',aggregationData);
    };
    dataFactory.settings = function (settingsData) {
        return $http.post(urlBase + 'service/settings',settingsData);
    };
    dataFactory.getSettings = function () {
        return $http.get(urlBase + 'service/settings');
    };

    return dataFactory;
});
crawlerApp.config(function($routeProvider) {
    $routeProvider.when("/", {
        templateUrl : "sites/start.html",
        controller : "listController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/new_service", {
        templateUrl : "sites/newService.html",
        controller : "startController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/killed_service", {
        templateUrl : "sites/killed.html",
        controller : "killedListController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/aggregation", {
        templateUrl : "sites/aggregation.html",
        controller : "aggregationListController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/new_aggregation", {
        templateUrl : "sites/new_aggregation.html",
        controller : "aggregationStartController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/settings", {
        templateUrl : "sites/staticConfig.html",
        controller : "settingsController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(!userToken){ 
                    $location.path('/login');
                }
            }
        }  
    }).when("/login",{
        templateUrl : "sites/login.html",
        controller : "loginController",
        resolve:{
            "check":function($cookies, $location){  
                var userToken = $cookies.get('userToken');
                if(userToken){ 
                    $location.path('/');
                    alert("U are loged in!");
                }
            }
        }  
    }).otherwise({redirectTo: '/'});
});
crawlerApp.controller('settingsController', function ($scope, RestService) {
    $scope.settings = {
         borderOfLifetime:333,
         twitterLong:333
    };
    RestService.getSettings().then(function(response){
        $scope.settings = response.data;
        $scope.$apply;
    });
    $scope.setStaticDataAsyncAsJSON = function(){
         RestService.settings($scope.settings).then(function (response){
             $scope.settings = response.data;
             alert("Sikeres beállítás!")
             $scope.$apply;
         });
     }
});
crawlerApp.controller('headerController', function ($scope, $cookies) {
     $scope.userToken = $cookies.getObject('userToken');
});
crawlerApp.controller("listController", function ($scope, $cookies, $stomp, $rootScope, RestService) {    
    $scope.userToken = $cookies.getObject('userToken');
    $scope.crawlServices = {};
    $scope.crawlServices.logList = [];
    $scope.crawlServices.serviceList = [];
    $scope.labels = ["Update", "Insert", "Delete"];
    $scope.getExp = function(str) {
        var today = new Date();
        today.setHours(today.getHours() - 1);
        var startDate = new Date(str);
        var exp = today - startDate;  
        var timeDiff = Math.abs(today.getTime() - startDate.getTime());
        var diffDays = Math.round(timeDiff / (1000 * 3600 * 24));
        var time = new Date(exp);
        return diffDays+" day and "+ time.getHours()+":"+time.getMinutes()+":"+time.getSeconds();     
    };
    $scope.stopService = function(crawlservice){
        RestService.stopService(crawlservice).then(function (response) {
                $scope.crawlServices.serviceList = response.data;
                console.log(response.data);
            }).catch (function(data){
                console.log(data);
            }); 
    };
    $scope.pauseService = function(crawlservice){
        RestService.pauseService(crawlservice).then(function (response) {
                $scope.crawlServices.serviceList = response.data;
                console.log(response.data);
            }).catch (function(data){
                console.log(data);
            }); 
    };
    RestService.subscribeing().then(function (frame) {
        $stomp.subscribe('/topic/log', function (response) {
            console.log(response);
            $scope.crawlServices.serviceList = response.services;
            $scope.crawlServices.logList.unshift(response.logMap);
            $rootScope.$apply();
        });
    });
});
crawlerApp.controller("killedListController", function ($scope, $cookies, $location, RestService) {    
    $scope.userToken = $cookies.getObject('userToken');
    $scope.crawlServices = {};
    $scope.crawlServices.serviceList = [];
    RestService.getKilledServices().then(function (response) {
        console.log(response);
        $scope.crawlServices.serviceList = response.data;
    });
    $scope.restartService = function(crawlservice){
        RestService.restartService(crawlservice).then(function (response) {            
            console.log(response.data);
            if(response.data.message == "Ok"){
                $location.path("/");  
            }else{
                alert("Ups! Something went wrong!");
            }
        }).catch (function(data){
            alert("Ups! Something went wrong!");
        }); 
    };
});
crawlerApp.controller("loginController", function($scope, $cookies, $location, RestService){
    $scope.loginAsyncAsJSON = function(){		
            var dataObj = {
                email: $scope.loginname,
                password: $scope.password
            };
            console.log(dataObj);
            RestService.login(dataObj).then(function(data){                
                console.log(data);
                var today = new Date();
                var expiresValue = new Date(today);
                expiresValue.setSeconds(today.getSeconds() + 3600); 

                $cookies.putObject('userToken', data.data, {'expires': expiresValue});
                $location.path("/");                
            }).catch (function(data){
                alert("Ups! Something went wrong!");
                console.log(data);
            });
	};
});
crawlerApp.controller("startController", function($scope, $cookies, $location, RestService, $route){
    $scope.userToken = $cookies.getObject('userToken');
    $scope.modeList = ["insert","update"];
    $scope.typeList = ["facebook","twitter", "instagram","custom", "periscope"];
    $scope.serviceType = "facebook";
    $scope.startAsyncAsJSON = function(){	
        var dataObj = {
                name: ($scope.name == ""?$scope.type+" _ "+$scope.mode:$scope.name),
                type: $scope.type,
                mode: $scope.mode,
                maxViewer: $scope.maxViewer,
                videoCount: $scope.videoCount
            };
        if($scope.type == "facebook"){
            dataObj.east = $scope.east;
            dataObj.west = $scope.west;
            dataObj.south = $scope.south;
            dataObj.north = $scope.north;
        }else if($scope.type == "twitter"){
            dataObj.latitude = $scope.latitude;
            dataObj.longitude = $scope.longitude;
            dataObj.radius = $scope.radius;
        }else if($scope.type == "custom"){
            dataObj.listUrl = $scope.listUrl;
            dataObj.detailsUrl = $scope.detailsUrl;
            dataObj.providerName = $scope.providerName;
        }
            console.log(dataObj);
            RestService.startService(dataObj).then(function(data){
                console.log(data);
                if(data.data.message == "Ok"){
                     $location.path("/");   
                }
            }).catch (function(data){
                alert("Ups! Something went wrong!");
                console.log(data);
            });
	};
});
crawlerApp.controller("aggregationStartController", function($scope, $cookies, $location, RestService, $route){
    $scope.userToken = $cookies.getObject('userToken');
    $scope.dateToPopularity = new Date();
    $scope.aggregationTypeList = ["BasicAggregation","PopularityAggregation"];
    $scope.startAggregationAsyncAsJSON = function(){	
        var dataObj = {
            aggregationType: $scope.aggregationType,
        };
        if($scope.aggregationType == "BasicAggregation"){
            dataObj.clusterNumber= $scope.clusterNumber,
            dataObj.minItem= $scope.minItem;
        }else{
            dataObj.dateToPopularity = $scope.dateToPopularity;
        }
        console.log(dataObj);
        RestService.startAggregationService(dataObj).then(function(response){
            console.log(response);
            if(response.data.message == "Ok"){
                $location.path("/aggregation");   
            }
        }).catch (function(data){
            alert("Ups! Something went wrong!");
            console.log(data);
        });
    };
});
crawlerApp.controller("aggregationListController", function ($scope, $cookies, $stomp, $rootScope, RestService) {    
    $scope.userToken = $cookies.getObject('userToken');
    $scope.aggreagtions = {};
    $scope.aggreagtions.logList = [];
    $scope.aggreagtions.aggregationList = [];
    
    $scope.stopAggr = function(aggregation){
        RestService.stopAggregation(aggregation).then(function (response) {
                $scope.aggreagtions.aggregationList = response.data;
                console.log(response.data);
            }).catch (function(data){
                console.log(data);
            }); 
    };
    $scope.pauseAggr = function(aggregation){
        RestService.pauseAggregation(aggregation).then(function (response) {
                $scope.aggreagtions.aggregationList = response.data;
                console.log(response.data);
            }).catch (function(data){
                console.log(data);
            }); 
    };
    RestService.subscribeing().then(function (frame) {
        $stomp.subscribe('/topic/aggregation', function (response) {
            console.log(response);
            $scope.aggreagtions.aggregationList = response.aggregationStarter;
            $scope.aggreagtions.logList.unshift(response.logMap);
            $rootScope.$apply();
        });
    });
});