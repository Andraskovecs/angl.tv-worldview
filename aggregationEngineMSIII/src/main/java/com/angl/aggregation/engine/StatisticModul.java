/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.aggregation.engine;


import com.angl.angldao.dao.CommonDAO;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.enums.Statistictype;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videopopularity;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kanyó Krisztián
 */
public class StatisticModul implements Runnable{
    private static final Logger logger = LogManager.getLogger();
    private static DataAccessObject dao = null;
    
    private int sleepTime = 1000 * 60 * 30; // 30perc
    private int maxVIdeo = 500; // maximum ennyi videóhoz generál statisztikai adatot (0 esetén az összeshez)
    
    private List<Video> videoList;
    
    @Override
    public void run(){
        try {
            this.work();
        } catch (InterruptedException ex) {
//            CrawlLogger.getInstance().sendAnalytic(info, "Stoped by interrupted", "Producer", "run", 39);
            return;
        } catch (Exception ex) {
//            CrawlLogger.getInstance().sendError(ex);
        } finally {
            if(this.dao != null) this.dao.closeSession();
        }
        

    }
    
    private void work() throws InterruptedException {
        
    }
}
