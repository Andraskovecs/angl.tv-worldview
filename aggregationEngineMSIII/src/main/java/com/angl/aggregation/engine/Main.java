/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angl.aggregation.engine;

/**
 *
 * @author Kanyó Krisztián
 */
public class Main {
    /**
     * 
     * @param args - Klaszter típusa, minimum klaszterszám, minimum elemszám egy klaszteren belül
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
        args[0] = "ownstatistic";
//        args[0] = "PopularityAggregation";
        args[1] = "2018/02/28";
        for(String s:args){
            System.out.println(s+"\n");
        }
        /* Statisztika generálása */
        if(args.length == 0 || args[0].toLowerCase().equals("ownstatistic")){
            System.out.println("Saját nézettségi adatok mentésének indítása...");
            Thread t = new Thread(new StatisticModul());
            t.start();
        }
        /* Aggregáció indítása (aggregáció típusát az args[0] határozza meg.) */
        else if(args.length == 3){
            System.out.println("Aggregációs szál indítása...");
            //Thread t = new Thread(new AggregationModul(args));
            //t.start();
            
        }else{
            System.out.println("Pontosan három paramétert kell megadni (Klaszter típusa, minimum klaszterszám, minimum elemszám egy klaszteren belül).");
        }
    }
}
