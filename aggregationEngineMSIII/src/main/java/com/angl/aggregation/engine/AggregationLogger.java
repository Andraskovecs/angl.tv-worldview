package com.angl.aggregation.engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AggregationLogger {
    private static final Logger logger = LogManager.getLogger("aggregation-log");
    private static final Logger errorLogger = LogManager.getLogger("error-log");

    private static class SingletonHelper {
        private static final AggregationLogger INSTANCE = new AggregationLogger();
    }

    public static AggregationLogger getInstance() {
        return SingletonHelper.INSTANCE;
    }
    public void sendError(Exception ex){
        errorLogger.error("Exception class: {}",ex.getClass().toGenericString());
        errorLogger.error("Exception message: {}",ex.getMessage());
        int i = 1;
        String trace = "Exception trace";
        for(StackTraceElement ste: ex.getStackTrace()){
            trace += "\r\n\t"+i+". "+ste.getFileName()+", Class:  "+ste.getClassName()+", Method: "+ste.getMethodName()+"(), Line: "+ste.getLineNumber();
            i++;
        }
        errorLogger.error("{}",trace);
        errorLogger.error("{}","-------------------------------------------------------------------------------------------------------------------------");
    }
    public void sendInfo(AggregationStarter starter, String message){       
        String infoLine = message+"\r\n\t"+starter.toString();
        logger.info("Aggregation message {}", infoLine);
         try {
            starter.putMessage(message);
        } catch (InterruptedException ex) {
            
        }
    }
}
