package com.angl.aggregation.engine;

import com.angl.angldao.aggregation.Point;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import lombok.Getter;
import lombok.Setter;

public class AggregationStarter {
    public static final String POPULARITY = "PopularityAggregation", BASIC = "BasicAggregation", SPACE = "SpaceAggregation", STATISTIC="StatisticGenerator";
    public static final String STARTING = "Starting",RUNNING="Running",PAUSED="Paused";
    private static AtomicLong  idCounter = new AtomicLong(); 
    
    private @Getter @Setter String id, aggregationType, status;
    private @Getter @Setter Integer clusterNumber, minItem;
    private @Getter @Setter Date dateToPopularity;
    private @Getter @Setter Boolean isStopable;
    private @Getter @Setter List<Point> pointList;
    private @Getter @Setter Double eps;
    
    private BlockingQueue<String> messageQueue = new ArrayBlockingQueue<>(30);

    public AggregationStarter() {
        this.id = AggregationStarter.createID();// minden kapcsolat jobra balra iterálja 
        this.status = AggregationStarter.STARTING;
        this.isStopable = true;
    }
    private static String createID(){
        return String.valueOf(idCounter.incrementAndGet());
    }
    public void putMessage(String message) throws InterruptedException {
        this.messageQueue.put(message);
    }
    public String takeMessage() throws InterruptedException{
        if(this.messageQueue.size()>0){
        return this.messageQueue.take();
        }
        return "";
    }

    public String getName() {
        return this.id+"-"+this.aggregationType;
    }

    @Override
    public String toString() {
        return "AggregationStarter{" + "id=" + id + ", aggregationType=" + aggregationType + ", status=" + status + ", clusterNumber=" + clusterNumber + ", minItem=" + minItem + ", dateToPopularity=" + dateToPopularity + ", pointList=" + pointList + ", eps=" + eps + '}';
    }

    
    
}
