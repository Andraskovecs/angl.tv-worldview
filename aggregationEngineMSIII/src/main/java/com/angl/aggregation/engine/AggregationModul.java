package com.angl.aggregation.engine;

import com.angl.angldao.aggregation.AggregationObject;
import com.angl.angldao.aggregation.AggregationObjectFactory;
import com.angl.angldao.aggregation.BasicAggregation;
import com.angl.angldao.aggregation.CommonAggregation;
import com.angl.angldao.aggregation.Point;
import com.angl.angldao.aggregation.PopularityAggregation;
import com.angl.angldao.aggregation.RecommendedAggregation;
import com.angl.angldao.aggregation.SpaceAggregation;
import com.angl.angldao.dao.DataAccessObject;
import com.angl.angldao.dao.DataAccessObjectFactory;
import com.angl.angldao.dao.VideoDAO;
import com.angl.angldao.enums.Statistictype;
import com.angl.angldao.models.Video;
import com.angl.angldao.models.Videopopularity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class AggregationModul implements Runnable {

    private int sleepTime = 1000 * 60;// * 10; // 10min
    private AggregationStarter starter;

    private volatile Boolean run = true;
    private DataAccessObject dao;
    private List<Video> videoList;
    private int maxVIdeo = 0; // maximum ennyi videóhoz generál statisztikai adatot (0 esetén az összeshez);

    public AggregationModul(AggregationStarter starter) {
        this.starter = starter;
    }

    @Override
    public void run() {
        try {
            if(starter.getAggregationType().equals(AggregationStarter.STATISTIC)){
                this.statisticWork();
            }else{
                this.work();
            }
        } catch (InterruptedException ex) {
            AggregationLogger.getInstance().sendError(ex);
        } catch (Exception ex) {
            AggregationLogger.getInstance().sendError(ex);
        }
    }

    private void work() throws InterruptedException {
        this.starter.setStatus(AggregationStarter.RUNNING);
        long t1, t2, d;
        Object aggregationType = new CommonAggregation();
        if (this.starter.getAggregationType().equals(AggregationStarter.BASIC)) {
            aggregationType = new RecommendedAggregation();
        }
        if (this.starter.getAggregationType().equals(AggregationStarter.POPULARITY)) {
            aggregationType = new PopularityAggregation();
        }
        if (this.starter.getAggregationType().equals(AggregationStarter.SPACE)) {
            aggregationType = new SpaceAggregation();
        }

        AggregationLogger.getInstance().sendInfo(starter, "Kiválasztott aggregáció: " + aggregationType.getClass().getName());

        AggregationObject ao = null;
        if (this.starter.getAggregationType().equals(AggregationStarter.POPULARITY)) {
            ao = new AggregationObjectFactory(this.starter.getDateToPopularity()).getAggregationObject(aggregationType.getClass());
        }else if(this.starter.getAggregationType().equals(AggregationStarter.SPACE)){
            ao = new AggregationObjectFactory(this.starter.getPointList(), this.starter.getEps(),this.starter.getMinItem()).getAggregationObject(aggregationType.getClass());
        }else {
            ao = new AggregationObjectFactory(this.starter.getClusterNumber(), this.starter.getMinItem()).getAggregationObject(aggregationType.getClass());
        }

        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                System.out.println(starter.getName() + " - Interrupted Producer:= " + Thread.currentThread().isInterrupted());
                throw new InterruptedException();
            } else {
                this.hasToPause();

                t1 = System.currentTimeMillis();

                if (ao != null) {
                    boolean result = ao.doAggregation();
                    if (result) {
                        AggregationLogger.getInstance().sendInfo(starter, "Az aggregáció sikeresen lefutott!");
                    } else {
                        AggregationLogger.getInstance().sendInfo(starter, "Az aggregáció során hiba lépett fel!");
                    }
                } else {
                    AggregationLogger.getInstance().sendInfo(starter, "---> FAIL!! Nem sikerült létrehozni aggregációs osztályt!<----");
                    throw new InterruptedException("---> FAIL!! Nem sikerült létrehozni aggregációs osztályt!<----");
                }

                t2 = System.currentTimeMillis();
                d = t2 - t1; //eltelt ido
                AggregationLogger.getInstance().sendInfo(starter, "Az iteráció " + d / (60 * 1000) + " percig futott.");
                AggregationLogger.getInstance().sendInfo(starter, "Következő aggregációs ciklus " + this.sleepTime / 1000 + " másodperc múlva indul");
                Thread.sleep(this.sleepTime);
            }
        }
    }

    private void statisticWork() throws InterruptedException{
        
        this.starter.setStatus(AggregationStarter.RUNNING);
        this.dao = new DataAccessObjectFactory().getDataAccessObject(VideoDAO.class);

        int iteration = 1;
        int statisticCount = 0;
        long t1,t2,d;
        
        while(true){
            if (Thread.currentThread().isInterrupted()) {
                System.out.println(starter.getName() + " - Interrupted Producer:= " + Thread.currentThread().isInterrupted());
                throw new InterruptedException();
            } else {
                this.hasToPause();
                t1 = System.currentTimeMillis();
                AggregationLogger.getInstance().sendInfo(starter,iteration + ". iteráció");
                this.dao.openSession();
                this.videoList = ((VideoDAO)dao).getAllVideo();
                if(this.maxVIdeo != 0) this.videoList = this.videoList.subList(0, videoList.size() > this.maxVIdeo ? this.maxVIdeo : videoList.size() );
                AggregationLogger.getInstance().sendInfo(starter,"Lekért videók száma: "+this.videoList.size());
                
                for (Video video : videoList) {
                    if(statisticCount % 100 == 0) AggregationLogger.getInstance().sendInfo(starter,statisticCount + " videóstatisztika feltöltve");
                    int view = video.getLocalwatched();
                    int comment = video.getComments().size();
                    int echo = video.getEchos().size();
                    Videopopularity vp = new Videopopularity(video, Statistictype.own, new Date(), view, comment, echo);
                    ((VideoDAO)dao).saveStatistic(video, vp);
                    statisticCount++;
                }
                
                
                t2 = System.currentTimeMillis();
                d = t2 - t1; //eltelt ido
                AggregationLogger.getInstance().sendInfo(starter,"Összesen "+this.videoList.size()+" videóstatisztika lett feltöltve.");
                AggregationLogger.getInstance().sendInfo(starter,"Az iteráció "+d/(60*1000)+" percig futott.");
                AggregationLogger.getInstance().sendInfo(starter,"Futás szüneteltetése " + this.sleepTime/1000 + " másodpercig");
                iteration++;
                statisticCount = 0;
                this.dao.closeSession();
                Thread.sleep(this.sleepTime);
            }
        }
    }
    public void pause() {
        if (starter.getStatus().equals(AggregationStarter.PAUSED)) {
            starter.setStatus(AggregationStarter.RUNNING);
        } else {
            starter.setStatus(AggregationStarter.PAUSED);
        }
        System.out.println(starter.getName() + " - Aggregation called pause");
        this.run = !this.run;
        if (this.run == true) {
            starter.setStatus(AggregationStarter.RUNNING);
            System.out.println(starter.getName() + " - Aggregation restart");
            synchronized (this) {
                this.notify();
            }
        }
    }

    private void hasToPause() throws InterruptedException {
        if (this.run == false) {
            System.out.println(starter.getName() + " - Aggregation wait");
            synchronized (this) {
                this.wait();
            }
        }
    }
}
